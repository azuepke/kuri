#!/bin/bash
# SPDX-License-Identifier: MIT
# azuepke, 2021-01-25: initial
echo "Building for all targets ..."

function usage {
	echo "error: must specify an operation"
	echo "  $0 all      -- build all targets"
	echo "  $0 list     -- list all targets"
	echo "  $0 clean    -- clean all targets"
}

if [ -z "$1" ]; then
	usage
	exit 1
fi

if [ "$1" = "clean" ]; then
	rm tmp-buildall-*-*-*.log
	exit 0
fi

TARGETS=BUILDENV.sh-*

if [ "$1" = "list" ]; then
	echo $TARGETS
	exit 0
fi

if [ "$1" != "all" ]; then
	usage
	exit 1
fi

# warn when using uninitialised variables
set -u
# stop on first error
set -e

# build(env-file, DEBUG, SMP)
function build {
	echo -e "\n-----------------------------------------------------------\n"
	echo "Building $1 DEBUG=$2 SMP=$3"
	if [ -f tmp-buildall-$1-$2-$3.log ]; then
		echo "-- exists, skipped"
		return
	fi
	bash -c "source $1 ; make DEBUG=$2 SMP=$3 all"
	bash -c "source $1 ; make DEBUG=$2 SMP=$3 distclean"
	echo "-- done"
	touch tmp-buildall-$1-$2-$3.log
}

# build all targets with both DEBUG and SMP set to yes or no
for t in $TARGETS; do
	build $t yes no
	build $t no  no
	build $t yes yes
	build $t no  yes
done
