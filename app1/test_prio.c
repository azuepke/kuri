/* SPDX-License-Identifier: MIT */
/*
 * test_prio.c
 *
 * Testcase for fast priority switching
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <stdio.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"

/* expected priority of partition*/
#define MAX_PRIO 200
#define TEST_PRIO (MAX_PRIO-1)


void test_prio(void)
{
	unsigned int prio;

	print_header("Fast priority switching");

	/* starting at MAX_PRIO */
	prio = sys_prio_max();
	assert(prio == MAX_PRIO);
	prio = sys_prio_get();
	assert(prio == MAX_PRIO);
	prio = sys_prio_get_syscall();
	assert(prio == MAX_PRIO);

	/* lower priority to TEST_PRIO */
	prio = sys_prio_set(TEST_PRIO);
	assert(prio == MAX_PRIO);
	prio = sys_prio_get();
	assert(prio == TEST_PRIO);
	prio = sys_prio_get_syscall();
	assert(prio == TEST_PRIO);

	/* testing upper bound and beyond */
	prio = sys_prio_set(255);
	assert(prio == TEST_PRIO);
	prio = sys_prio_set(1000);
	assert(prio == MAX_PRIO);

	prio = sys_prio_set(TEST_PRIO-1);
	assert(prio == MAX_PRIO);
	prio = sys_prio_get();
	assert(prio == TEST_PRIO-1);
	prio = sys_prio_set(0xffffffff);
	assert(prio == TEST_PRIO-1);
	prio = sys_prio_get();
	assert(prio == MAX_PRIO);

	prio = sys_prio_set_syscall(TEST_PRIO-1);
	assert(prio == MAX_PRIO);
	prio = sys_prio_get_syscall();
	assert(prio == TEST_PRIO-1);
	prio = sys_prio_set_syscall(0xffffffff);
	assert(prio == TEST_PRIO-1);
	prio = sys_prio_get_syscall();
	assert(prio == MAX_PRIO);

	/* done */
	(void)sys_prio_set_syscall(MAX_PRIO);
	prio = sys_prio_get();
	assert(prio == MAX_PRIO);

	printf("OK\n");
}
