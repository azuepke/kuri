/* SPDX-License-Identifier: MIT */
/*
 * test_mutex_cond.c
 *
 * Mutex and condition variable testcases
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"

/* sync objects 0 and 1 are used by the API benchmark */
#define MUTEX1 2
#define MUTEX2 3
#define COND1 4
#define COND2 5
#define UNUSED 6
#define INVALID -1


static volatile unsigned int test_step;

static void mutex_thr1(void *arg __unused)
{
	err_t err;

	assert(test_step == 101);
	test_step = 102;

	err = sys_mutex_lock(MUTEX1, 0, TIMEOUT_INFINITE);
	assert(err == EOK);

	test_step = 102;

	err = sys_thread_suspend();
	assert(err == EOK);

	assert(test_step == 103);
	test_step = 104;

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);

	assert(test_step == 105);

	test_step = 999;
	sys_thread_exit();
}

static void cond_thr1(void *arg __unused)
{
	err_t err;

	assert(test_step == 201);
	test_step = 202;

	err = sys_thread_suspend();
	assert(err == EOK);

	assert(test_step == 203);
	test_step = 204;

	/* naked wakeup */
	err = sys_cond_wake(COND1, false);
	assert(err == EOK);

	assert(test_step == 204);
	test_step = 205;

	err = sys_thread_suspend();
	assert(err == EOK);

	assert(test_step == 207);
	test_step = 208;

	/* wakeup with lock */
	err = sys_mutex_lock(MUTEX1, 0, 0);
	assert(err == EOK);

	err = sys_cond_wake(COND1, false);
	assert(err == EOK);

	assert(test_step == 208);
	test_step = 209;

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);

	assert(test_step == 210);
	test_step = 211;

	err = sys_thread_suspend();
	assert(err == EOK);

	test_step = 999;
	sys_thread_exit();
}

void test_mutex_cond(void)
{
	static int first_run = 1;
	err_t err;

	print_header("Mutex Implementation");

	printf("init sync objects: ");
	if (first_run) {
		first_run = 0;

		/* init objects */
		err = sys_mutex_init(MUTEX1, 0);
		assert(err == EOK);
		err = sys_mutex_init(MUTEX2, 0);
		assert(err == EOK);
		err = sys_cond_init(COND1, 0);
		assert(err == EOK);
		err = sys_cond_init(COND2, 0);
		assert(err == EOK);
		printf("OK (first run)\n");
	} else {
		printf("skipped (second run)\n");
	}

	////////////////////////////////////////////////////////////////////////////

	printf("API limits: ");

	/* bad flags */
	err = sys_mutex_init(MUTEX1, 1);
	assert(err == EINVAL);

	/* ID out of limits */
	err = sys_mutex_init(INVALID, 0);
	assert(err == ELIMIT);

	/* already a mutex */
	err = sys_mutex_init(MUTEX1, 0);
	assert(err == ESTATE);
	err = sys_cond_init(MUTEX1, 0);
	assert(err == ESTATE);

	/* bad flags */
	err = sys_mutex_lock(MUTEX1, 1, 0);
	assert(err == EINVAL);

	/* ID out of limits */
	err = sys_mutex_lock(INVALID, 0, 0);
	assert(err == ELIMIT);

	/* bad type */
	err = sys_mutex_lock(COND1, 0, 0);
	assert(err == ETYPE);
	err = sys_mutex_lock(UNUSED, 0, 0);
	assert(err == ETYPE);

	/* ID out of limits */
	err = sys_mutex_unlock(INVALID);
	assert(err == ELIMIT);

	/* bad type */
	err = sys_mutex_unlock(COND1);
	assert(err == ETYPE);
	err = sys_mutex_unlock(UNUSED);
	assert(err == ETYPE);

	/* bad flags */
	err = sys_cond_init(COND1, 1);
	assert(err == EINVAL);

	/* ID out of limits */
	err = sys_cond_init(INVALID, 0);
	assert(err == ELIMIT);

	/* already a cond */
	err = sys_cond_init(COND1, 0);
	assert(err == ESTATE);
	err = sys_mutex_init(MUTEX1, 0);
	assert(err == ESTATE);

	/* ID out of limits */
	err = sys_cond_wait(INVALID, INVALID, 0);
	assert(err == ELIMIT);
	err = sys_cond_wait(COND1, INVALID, 0);
	assert(err == ELIMIT);
	err = sys_cond_wait(INVALID, MUTEX1, 0);
	assert(err == ELIMIT);

	/* bad type */
	err = sys_cond_wait(UNUSED, UNUSED, 0);
	assert(err == ETYPE);
	err = sys_cond_wait(COND1, UNUSED, 0);
	assert(err == ETYPE);
	err = sys_cond_wait(COND1, COND2, 0);
	assert(err == ETYPE);
	err = sys_cond_wait(MUTEX1, MUTEX2, 0);
	assert(err == ETYPE);
	err = sys_cond_wait(UNUSED, MUTEX2, 0);
	assert(err == ETYPE);

	/* ID out of limits */
	err = sys_cond_wake(INVALID, false);
	assert(err == ELIMIT);

	/* bad type */
	err = sys_cond_wake(MUTEX1, false);
	assert(err == ETYPE);
	err = sys_cond_wake(UNUSED, false);
	assert(err == ETYPE);
	printf("OK\n");

	////////////////////////////////////////////////////////////////////////////

	printf("mutex API: ");

	err = sys_mutex_lock(MUTEX1, 0, TIMEOUT_INFINITE);
	assert(err == EOK);
	err = sys_mutex_lock(MUTEX1, 0, TIMEOUT_INFINITE);
	assert(err == EBUSY);

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);
	err = sys_mutex_unlock(MUTEX1);
	assert(err == ESTATE);

	err = sys_mutex_trylock(MUTEX1, 0);
	assert(err == EOK);
	err = sys_mutex_trylock(MUTEX1, 0);
	assert(err == EBUSY);

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);
	err = sys_mutex_unlock(MUTEX1);
	assert(err == ESTATE);

	/* create dummy thread for contention */
	test_step = 101;
	create_thread(1, mutex_thr1, NULL, sys_prio_get()-1);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(10000000));

	assert(test_step == 102);

	err = sys_mutex_lock(MUTEX1, 0, timeout_rel(10000000));
	assert(err == ETIMEOUT);

	assert(test_step == 102);
	test_step = 103;

	err = sys_thread_resume(1);
	assert(err == EOK);

	err = sys_mutex_lock(MUTEX1, 0, timeout_rel(10000000));
	assert(err == EOK);

	assert(test_step == 104);
	test_step = 105;

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	assert(test_step == 999);

	printf("\n");

	////////////////////////////////////////////////////////////////////////////

	printf("condition variable API: ");

	err = sys_cond_wait(COND1, MUTEX2, 0);
	assert(err == ESTATE);

	err = sys_cond_wait(COND1, MUTEX1, timeout_rel(10000000));
	assert(err == ETIMEOUT);

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);

	/* create dummy thread for contention */
	test_step = 201;
	create_thread(1, cond_thr1, NULL, sys_prio_get()-1);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(10000000));

	assert(test_step == 202);
	test_step = 203;

	err = sys_mutex_lock(MUTEX1, 0, 0);
	assert(err == EOK);

	err = sys_thread_resume(1);
	assert(err == EOK);

	err = sys_cond_wait(COND1, MUTEX1, TIMEOUT_INFINITE);
	assert(err == EOK);

	assert(test_step == 204);

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(10000000));

	assert(test_step == 205);
	test_step = 206;

	err = sys_mutex_lock(MUTEX1, 0, 0);
	assert(err == EOK);

	err = sys_thread_resume(1);
	assert(err == EOK);

	test_step = 207;
	err = sys_cond_wait(COND1, MUTEX1, TIMEOUT_INFINITE);
	assert(err == EOK);

	assert(test_step == 209);
	test_step = 210;

	err = sys_mutex_unlock(MUTEX1);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(10000000));

	assert(test_step == 211);

	err = sys_thread_resume(1);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(10000000));

	assert(test_step == 999);

	printf("\n");
}
