/* SPDX-License-Identifier: MIT */
/*
 * app.h
 *
 * Application main include file
 *
 * azuepke, 2018-03-19: initial
 */

#ifndef __APP_H__
#define __APP_H__

#include <marron/types.h>
#include <stdint.h>

/* buildid.c */
extern const char __buildid[];

/* main.c */
void create_thread(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio);
void create_thread_cpu(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio, unsigned int cpu);
void join_thread(unsigned int thr_id);
sys_timeout_t timeout_rel(sys_timeout_t rel);
void print_line(void);
void print_header(const char *s);

/* various */
void test_prio(void);
void test_sched(void);
void test_suspend_resume(void);
void test_suspend_resume_smp(void);
void test_timer_irq(void);
void test_signals(void);
void test_futex(void);
void test_lwsync(void);
void test_mutex_cond(void);
void test_event(void);
void test_partition(volatile uint32_t *shm1);

/* bench_* */
void bench_cpu(void);
void bench_api(void);
void bench_ospert2014(void);
void bench_ecrts2020(void);
void bench_ecrts2020_distrib(void);

#endif
