/* SPDX-License-Identifier: MIT */
/*
 * main.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 */

#include <marron/api.h>
#include <stack.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include <tls.h>
#include "app.h"
#include "my_tls.h"

/* stacks */
#define STACK_SIZE 8192
SYS_STACK_DEFINE(main_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread1_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread2_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread3_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread4_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread5_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread6_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread7_stack, STACK_SIZE);
SYS_STACK_DEFINE(thread8_stack, STACK_SIZE);

/* TLS areas */
MY_TLS_DEFINE(main_tls);
struct my_tls thread1_tls;
struct my_tls thread2_tls;
struct my_tls thread3_tls;
struct my_tls thread4_tls;
struct my_tls thread5_tls;
struct my_tls thread6_tls;
struct my_tls thread7_tls;
struct my_tls thread8_tls;

void create_thread_cpu(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio, unsigned int cpu)
{
	void *stack;
	void *tls;
	err_t err;

	switch (thr_id) {
	case 1:
		stack = SYS_STACK_PTR(thread1_stack);
		tls = &thread1_tls;
		break;

	case 2:
		stack = SYS_STACK_PTR(thread2_stack);
		tls = &thread2_tls;
		break;

	case 3:
		stack = SYS_STACK_PTR(thread3_stack);
		tls = &thread3_tls;
		break;

	case 4:
		stack = SYS_STACK_PTR(thread4_stack);
		tls = &thread4_tls;
		break;

	case 5:
		stack = SYS_STACK_PTR(thread5_stack);
		tls = &thread5_tls;
		break;

	case 6:
		stack = SYS_STACK_PTR(thread6_stack);
		tls = &thread6_tls;
		break;

	case 7:
		stack = SYS_STACK_PTR(thread7_stack);
		tls = &thread7_tls;
		break;

	case 8:
		stack = SYS_STACK_PTR(thread8_stack);
		tls = &thread8_tls;
		break;

	default:
		sys_abort();
	}

	err = sys_thread_create(thr_id, func, arg, stack, tls, prio, cpu);
	assert(err == EOK);
}

void create_thread(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio)
{
	create_thread_cpu(thr_id, func, arg, prio, 0);
}

void join_thread(unsigned int thr_id)
{
	/* empty on Marron */
	(void)thr_id;
}

sys_timeout_t timeout_rel(sys_timeout_t rel)
{
	return sys_time_get() + rel;
}

void print_line(void)
{
	printf("************************************************************************\n");
}

void print_header(const char *s)
{
	printf("\n");
	print_line();
	printf("*** Testcase: %s\n", s);
	print_line();
}

unsigned long long var64 = 0x0123456789abcdef;

extern uint32_t _shm1_start[];

void main(void *arg __unused)
{
	volatile uint32_t *shm1;
	char bsp_name[32];
	err_t err;

	/* minimal TLS setup */
	SYS_TLS_FIXUP();
	main_tls.tid = 0;
	thread1_tls.tid = 1;
	thread2_tls.tid = 2;
	thread3_tls.tid = 3;
	thread4_tls.tid = 4;

	// FIXME: HACK
	//bench_ecrts2020();
	//bench_ecrts2020_distrib();

	printf("\n");
	print_line();
	printf("*** TESTSUITE\n");
	print_line();

	/*
	 * shm1[0] == Magic Word (0x42424242)
	 * shm1[1] == Test Counter
	 * shm1[2] == Spinlock for Part 2
	 * shm1[3] == Test Select for Part 2
	 * shm1[4] == Part 2 Test State Variable
	 */
	shm1 = (volatile uint32_t *) _shm1_start;
	printf("# SHM1 addess: %p\n", shm1);
	/* check shm is init */
	if (shm1[0] != 0x42424242) {
		/* init shm */
		shm1[0] = 0x42424242;
		shm1[1] = 0;
		shm1[2] = 0;
		shm1[3] = 0;
		shm1[4] = 0;
	}

	printf("# Hello World from user space\n");
	/* test crt0, i.e. if the data segment is set up properly */
	printf("# Variable var64 is 0x%llx\n", var64);
	assert(var64 == 0x0123456789abcdef);

	printf("# TLS: 0x%p, 0x%p, 0x%p, 0x%p\n",
	       &main_tls, main_tls.sys.tls, (void*)__tls_base(),
	       tls_get_p(offsetof(struct my_tls, sys.tls)));
	assert(&main_tls == main_tls.sys.tls);
	assert(&main_tls == (void*)__tls_base());
	assert(&main_tls == tls_get_p(offsetof(struct my_tls, sys.tls)));

	printf("# priority: %d %d\n", sys_prio_get(), main_tls.sys.user_prio);
	assert(sys_prio_get() == main_tls.sys.user_prio);

	printf("# partition ID %d state %d\n", sys_part_self(), sys_part_state());
	assert(sys_part_state() == PART_STATE_RUNNING);

	err = sys_bsp_name(bsp_name, sizeof(bsp_name));
	assert(err == EOK);
	printf("# bsp name: %s\n", bsp_name);
	printf("\n");

	/* benchmarks */
	bench_cpu();
	bench_api();
	bench_ospert2014();
	bench_ecrts2020();
	//FIXME: takes ages to run ...
	//bench_ecrts2020_distrib();

	/* run tests */
	test_prio();
	test_sched();
	test_suspend_resume();
	test_suspend_resume_smp();
	test_signals();
	test_timer_irq();
	test_futex();
	test_lwsync();
	test_mutex_cond();
	test_event();
	test_partition(shm1);

	printf("### all tests done! ###\n");
	printf("Test Count: %u \n", shm1[1] + 1);
#ifndef AUTOBUILD
	shm1[1]++;
	printf("# last Test restart partion self\n");
	sys_part_restart();
#else
	if (shm1[1]++ < 1) {
		printf("# last Test restart partion self\n");
		sys_part_restart();
	} else {
		printf("# Shutdown\n");
		/* poweroff */
		sys_bsp_shutdown(2);
	}
#endif
}
