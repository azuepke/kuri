/* SPDX-License-Identifier: MIT */
/*
 * test_futex.c
 *
 * Futex testcases
 *
 * azuepke, 2018-03-19: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"

static volatile unsigned int test_step;

static void futex_thr1(void *arg)
{
	int *futex_p = arg;
	err_t err;

	assert(test_step == 1);
	test_step = 2;

	/* thread 0 not woken up */
	err = sys_futex_wake(futex_p, 0);
	assert(err == EOK);

	test_step = 3;

	/* thread 0 woken up */
	err = sys_futex_wake(futex_p, 1);
	assert(err == EOK);

	test_step = 4;

	/* thread 1 now waiting */
	err = sys_futex_wait(futex_p, 10, timeout_rel(1000*1000*1000));
	assert(err == EOK);

	test_step = 5;

	/* thread 1 waiting again */
	err = sys_futex_wait(futex_p, 10, timeout_rel(1000*1000*1000));
	assert(err == EOK);

	test_step = 999;

	sys_thread_exit();
}

void test_futex(void)
{
	int futex;
	int futex2;
	err_t err;

	print_header("Futex Implementation");

	test_step = 0;

	/* NULL pointer */
	err = sys_futex_wait((void*)0, 0, timeout_rel(1000000));
	assert(err == EINVAL);

	/* invalid alignment */
	err = sys_futex_wait((void*)1, 0, timeout_rel(1000000));
	assert(err == EINVAL);

	/* invalid address */
	err = sys_futex_wait((void*)4, 0, timeout_rel(1000000));
	assert(err == EFAULT);

	/* compare fails */
	futex = 10;
	err = sys_futex_wait(&futex, 20, timeout_rel(1000000));
	assert(err == EAGAIN);

	/* timeout triggers */
	err = sys_futex_wait(&futex, 10, timeout_rel(1000000));
	assert(err == ETIMEOUT);

	/* timeout triggers again */
	err = sys_futex_wait(&futex, 10, timeout_rel(1000000));
	assert(err == ETIMEOUT);

	/* create dummy thread which will wake us */
	create_thread(1, futex_thr1, &futex, sys_prio_get()-1);

	/* now wait */
	test_step = 1;
	err = sys_futex_wait(&futex, 10, TIMEOUT_INFINITE);
	assert(err == EOK);

	assert(test_step == 3);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	assert(test_step == 4);

	/* compare fails */
	err = sys_futex_requeue(&futex, 1, 11, NULL, 0);
	assert(err == EAGAIN);

	/* wakeup succeeds, no requeue */
	err = sys_futex_requeue(&futex, 1, 10, NULL, 0);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	assert(test_step == 5);

	/* requeue to 2nd futex */
	err = sys_futex_requeue(&futex, 0, 10, &futex2, 1);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	/* other thread is still blocked */
	assert(test_step == 5);

	/* try wakeup on first futex (no thread is waiting there now) */
	err = sys_futex_wake(&futex, 999);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	/* other thread is still blocked */
	assert(test_step == 5);

	/* wakeup on other futex */
	err = sys_futex_wake(&futex2, 999);
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	assert(test_step == 999);

	/* try wakeup on first futex again (no thread is waiting there now) */
	err = sys_futex_wake(&futex, 999);
	assert(err == EOK);

	printf("futex tests: OK\n");
	printf("\n");
}
