/* SPDX-License-Identifier: MIT */
/*
 * test_lwsync.c
 *
 * Light-weight synchronization testcases
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"

static volatile unsigned int test_step;
int thread0_user_state;
int thread1_user_state;

static void lwsync_thr1(void *arg __unused)
{
	err_t err;

	assert(test_step == 1);
	test_step = 2;

	/* thread 0 not woken up */
	err = sys_lwsync_wake(0, &thread0_user_state, 0, sys_prio_get());
	assert(err == EAGAIN);

	test_step = 3;

	/* thread 0 woken up */
	err = sys_lwsync_wake(0, &thread0_user_state, 10, sys_prio_get());
	assert(err == EOK);

	test_step = 4;

	/* thread 1 now waiting */
	err = sys_lwsync_wait(&thread1_user_state, 10, timeout_rel(1000*1000*1000), 5);
	assert(err == EOK);

	test_step = 5;

	/* thread 1 waiting again */
	err = sys_lwsync_wait(&thread1_user_state, 10, timeout_rel(1000*1000*1000), 5);
	assert(err == EOK);

	test_step = 999;

	sys_thread_exit();
}

void test_lwsync(void)
{
	err_t err;

	print_header("Light-weight sync implementation");

	thread0_user_state = 0;
	thread1_user_state = 10;
	test_step = 0;


	/* NULL pointer */
	err = sys_lwsync_wait((void*)0, 0, timeout_rel(1000000), 0);
	assert(err == EINVAL);

	/* invalid alignment */
	err = sys_lwsync_wait((void*)1, 0, timeout_rel(1000000), 0);
	assert(err == EINVAL);

	/* invalid address */
	err = sys_lwsync_wait((void*)4, 0, timeout_rel(1000000), 0);
	assert(err == EFAULT);

	/* compare fails */
	thread0_user_state = 10;
	err = sys_lwsync_wait(&thread0_user_state, 20, timeout_rel(1000000), 0);
	assert(err == EAGAIN);

	/* timeout triggers */
	err = sys_lwsync_wait(&thread0_user_state, 10, timeout_rel(1000000), 0);
	assert(err == ETIMEOUT);

	/* timeout triggers again */
	err = sys_lwsync_wait(&thread0_user_state, 10, timeout_rel(1000000), 0);
	assert(err == ETIMEOUT);

	/* create dummy thread which will wake us */
	create_thread(1, lwsync_thr1, NULL, sys_prio_get()-1);

	/* now wait */
	test_step = 1;
	err = sys_lwsync_wait(&thread0_user_state, 10, TIMEOUT_INFINITE, 0);
	assert(err == EOK);

	assert(test_step == 3);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	assert(test_step == 4);

	/* try wakeup on first lwsync (no thread is waiting there now) */
	err = sys_lwsync_wake(1, &thread1_user_state, 10, sys_prio_get());
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	/* other thread is still blocked */
	assert(test_step == 5);

	/* wakeup on other lwsync */
	err = sys_lwsync_wake(1, &thread1_user_state, 10, sys_prio_get());
	assert(err == EOK);

	/* wait for other threads to react */
	sys_sleep(timeout_rel(1000000));

	assert(test_step == 999);

	/* try wakeup on thread again (thread is dead by now) */
	err = sys_lwsync_wake(1, &thread1_user_state, 10, sys_prio_get());
	assert(err == ESTATE);

	printf("lwsync tests: OK\n");
	printf("\n");
}
