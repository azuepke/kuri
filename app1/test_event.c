/* SPDX-License-Identifier: MIT */
/*
 * test_event.c
 *
 * Event tests
 *
 * azuepke, 2018-04-10: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"
#include "config.h"

void test_event(void)
{
	err_t err;

	print_header("Event Implementation");

	/* test invalid flags */
	printf("test invalid corner cases: ");
	err = sys_event_wait(CFG_EVENT_WAIT_one, 1234, 0);
	assert(err == EINVAL);

	/* test invalid wait channel (we only have #0) */
	err = sys_event_wait(CFG_EVENT_WAITS, 0, 0);
	assert(err == ELIMIT);

	/* test invalid send channel (we only have #0 and #1) */
	err = sys_event_send(CFG_EVENT_SENDS);
	assert(err == ELIMIT);
	printf("OK\n");


	/* test timeout */
	printf("wait for event with timeout (no pending): ");
	err = sys_event_wait(CFG_EVENT_WAIT_one, 0, sys_time_get() + 5000000);
	assert(err == ETIMEOUT);
	printf("OK\n");

	/* send an event */
	err = sys_event_send(CFG_EVENT_SEND_one);
	assert(err == EOK);

	/* test OK, there is an event pending */
	printf("wait for event with timeout (ev pending): ");
	err = sys_event_wait(CFG_EVENT_WAIT_one, 0, sys_time_get() + 5000000);
	assert(err == EOK);
	printf("OK\n");

	/* test timeout 2, no event pending */
	printf("wait for event with timeout (no pending): ");
	err = sys_event_wait(CFG_EVENT_WAIT_one, 0, sys_time_get() + 5000000);
	assert(err == ETIMEOUT);
	printf("OK\n");
}
