/* SPDX-License-Identifier: MIT */
/*
 * bench_ecrts2020.c
 *
 * Benchmark for ECRTS 2020 paper
 *
 * azuepke, 2020-02-02: initial
 * azuepke, 2020-02-06: updated benchmarks
 * azuepke, 2020-02-07: final benchmarks for paper
 * azuepke, 2020-04-10: adding trylocks and SMP tests
 * azuepke, 2020-04-12: refine tests
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <compiler.h>
#include <assert.h>
#include "app.h"
#include "bench.h"
#include <arch_tls.h>
#include <arch_spin.h>
#include <atomic.h>
#include "my_tls.h"

#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"
#pragma GCC diagnostic ignored "-Wmissing-prototypes"
#pragma GCC diagnostic ignored "-Wunused-function"
//#pragma GCC diagnostic ignored "-Wunused-variable"

//#define RUN_ONCE

////////////////////////////////////////////////////////////////////////////////

// Note that this file includes the implementation of both the low-level
// building blocks and the actual library functions for mutexes and condvars.
// For realistic setting comparable to other operating systems,
// we allow the compiler to inline the former (as GLIBC does, for example),
// but prevent inlining of the latter exported API functions.

////////////////////////////////////////////////////////////////////////////////

/* base types */
typedef unsigned int tid_t;
typedef unsigned int prio_t;
typedef sys_timeout_t timeout_t;
#define INFINITE TIMEOUT_INFINITE

typedef struct my_tls thread_t;
#define SELF ((thread_t*)__tls_base())

#define FALSE 0
#define TRUE 1

#define CAS_RELAXED atomic_cas_relaxed
#define ACQUIRE_BARRIER atomic_acquire_barrier
#define RELEASE_BARRIER atomic_release_barrier

#define sys_wait_at_prio sys_lwsync_wait
#define sys_wake_set_prio sys_lwsync_wake

typedef struct {
	uint32_t lu;	// STATISTICS: lock uncontended
	uint32_t lc;	// STATISTICS: lock contended
	uint32_t lf;	// STATISTICS: lock fastpath (futex only)
	uint32_t uu;	// STATISTICS: unlock uncontended
	uint32_t uc;	// STATISTICS: unlock contended
	uint32_t uf;	// STATISTICS: unlock fastpath (futex only)
} lock_stat_t;

////////////////////////////////////////////////////////////////////////////////

/* global state */
static prio_t max_prio;

/* number of CPUs for SMP tests */
static unsigned int num_cpus;

/* priority level of benchmark thread */
#define BENCH_THREAD_PRIO 100

/* priority level of high priority waiter thread */
#define WAITER_THREAD_PRIO 101

/* priority level of lower priority worker thread */
#define WORKER_THREAD_PRIO 99

/* global ID of sync objects */
#define SYNC_OBJECT_MUTEX 0
#define SYNC_OBJECT_COND 1

/* delay in ns between tests (time to restart a thread with the same ID) */
#define BENCH_INTERTEST_DELAY 1000000

////////////////////////////////////////////////////////////////////////////////

/* spinlock API */
typedef arch_spin_t spin_t;

static inline void spin_init(spin_t *lock)
{
	arch_spin_init(lock);
}

static inline void spin_lock(spin_t *lock)
{
	arch_spin_lock(lock);
}

static inline void spin_unlock(spin_t *lock)
{
	arch_spin_unlock(lock);
}

////////////////////////////////////////////////////////////////////////////////

/* priority change API */
static inline prio_t prio_raise(prio_t new_prio)
{
	struct my_tls *tls = (struct my_tls *)__tls_base();
	prio_t old_prio = tls->sys.user_prio;
	assert(new_prio >= old_prio);
	tls->sys.user_prio = new_prio;
	return old_prio;
}

/* put syscall into a dedicated function */
static __noinline void prio_restore_syscall(void)
{
	sys_preempt();
}

static inline void prio_restore(unsigned int old_prio)
{
	struct my_tls *tls = (struct my_tls *)__tls_base();
	tls->sys.user_prio = old_prio;
	barrier();
	if (unlikely(old_prio < tls->sys.next_prio)) {
		prio_restore_syscall();
	}
}

////////////////////////////////////////////////////////////////////////////////

/* wait queue API */
typedef list_t waitq_t;

static inline void waitq_init(waitq_t *wq)
{
	list_head_init(wq);
}

static inline bool_t waitq_is_empty(waitq_t *wq)
{
	return list_is_empty(wq);
}

static inline bool_t waitq_is_not_empty(waitq_t *wq)
{
	return !list_is_empty(wq);
}

#define THR_FROM_WAITQ_NODE(t) list_entry((t), struct my_tls, waitq_node)

static inline void waitq_add_ordered(
	waitq_t *wq,
	thread_t *thr,
	prio_t prio)
{
	list_node_init(&thr->waitq_node);
	thr->waitq_prio = prio;

	list_add_sorted(wq, &thr->waitq_node,
	                THR_FROM_WAITQ_NODE(__ITER__)->waitq_prio < thr->waitq_prio);
}

static inline void waitq_remove(
	waitq_t *wq,
	thread_t *thr)
{
	(void) wq;
	list_del(&thr->waitq_node);
}

static inline void waitq_try_remove(
	waitq_t *wq,
	thread_t *thr)
{
	(void) wq;
	list_del(&thr->waitq_node);
}

static inline thread_t *waitq_remove_highest(
	waitq_t *wq)
{
	thread_t *thr;
	list_t *node;

	node = list_first(wq);
	if (node != NULL) {
		thr = THR_FROM_WAITQ_NODE(node);
		assert(thr != NULL);

		/* remove thread from wait queue */
		list_del(&thr->waitq_node);
	} else {
		thr = NULL;
	}
	return thr;
}

////////////////////////////////////////////////////////////////////////////////

/* mutex API */
#define LWSYNC_UNLOCKED (0xffffffff)

typedef struct {
	spin_t lock;        // internal spinlock
	tid_t owner;        // current mutex owner or LWSYNC_UNLOCKED
	waitq_t waitq;      // priority-ordered mutex wait queue
	uint32_t stat_lu;	// STATISTICS: lock uncontended
	uint32_t stat_lc;	// STATISTICS: lock contended
	uint32_t stat_lf;	// STATISTICS: lock fast-path in kernel
	uint32_t stat_uu;	// STATISTICS: unlock uncontended
	uint32_t stat_uc;	// STATISTICS: unlock contended
	uint32_t stat_uf;	// STATISTICS: unlock fast-path in kernel
} lwsync_mutex_t __aligned(64);

__noinline void lwsync_mutex_init(lwsync_mutex_t *m)
{
	spin_init(&m->lock);
	m->owner = LWSYNC_UNLOCKED;
	waitq_init(&m->waitq);
	m->stat_lu = 0;	// STATISTICS
	m->stat_lc = 0;	// STATISTICS
	m->stat_lf = 0;	// STATISTICS
	m->stat_uu = 0;	// STATISTICS
	m->stat_uc = 0;	// STATISTICS
	m->stat_uf = 0;	// STATISTICS
}

__noinline bool_t lwsync_mutex_timedlock(lwsync_mutex_t *m, timeout_t timeout)
{
	err_t err;

	prio_t old_prio = prio_raise(max_prio);
	spin_lock(&m->lock);

	bool_t success = (m->owner == LWSYNC_UNLOCKED);
	if (likely(success == TRUE)) {
		m->owner = SELF->tid;
		m->stat_lu++;	// STATISTICS
		goto out;
	}

	m->stat_lc++;	// STATISTICS
	waitq_add_ordered(&m->waitq, SELF, old_prio);
	uint32_t seq = ++SELF->ustate;
	spin_unlock(&m->lock);
	err = sys_wait_at_prio(&SELF->ustate, seq, timeout, old_prio);
	spin_lock(&m->lock);

	if (err == EAGAIN) {
		m->stat_lf++;		// STATISTICS
	}

	success = (SELF->ustate != seq);
	if (unlikely(success == FALSE)) {
		waitq_remove(&m->waitq, SELF);
	}

out:
	spin_unlock(&m->lock);
	prio_restore(old_prio);
	return success;
}

__noinline bool_t lwsync_mutex_trylock(lwsync_mutex_t *m)
{
	prio_t old_prio = prio_raise(max_prio);
	spin_lock(&m->lock);

	bool_t success = (m->owner == LWSYNC_UNLOCKED);
	if (likely(success == TRUE)) {
		m->owner = SELF->tid;
		m->stat_lu++;	// STATISTICS
	}

	spin_unlock(&m->lock);
	prio_restore(old_prio);
	return success;
}

__noinline void lwsync_mutex_unlock(lwsync_mutex_t *m)
{
	err_t err;

	assert(m->owner == SELF->tid);
	prio_t old_prio = prio_raise(max_prio);
	spin_lock(&m->lock);

	thread_t *next = waitq_remove_highest(&m->waitq);
	if (likely(next == NULL)) {
		m->owner = LWSYNC_UNLOCKED;
		m->stat_uu++;	// STATISTICS
		spin_unlock(&m->lock);
		prio_restore(old_prio);
		return;
	}

	m->stat_uc++;	// STATISTICS
	m->owner = next->tid;
	uint32_t seq = ++next->ustate;
	spin_unlock(&m->lock);
	err = sys_wake_set_prio(next->tid, &next->ustate, seq, old_prio);

	if (err == EAGAIN) {
		atomic_add_relaxed(&m->stat_uf, 1);		// STATISTICS
	}
}

__noinline void lwsync_mutex_stat(lwsync_mutex_t *m, lock_stat_t *s)
{
	s->lu = m->stat_lu;	// STATISTICS
	s->lc = m->stat_lc;	// STATISTICS
	s->lf = m->stat_lf;	// STATISTICS
	s->uu = m->stat_uu;	// STATISTICS
	s->uc = m->stat_uc;	// STATISTICS
	s->uf = m->stat_uf;	// STATISTICS
}

////////////////////////////////////////////////////////////////////////////////

/* condition variable API */
typedef waitq_t lwsync_cond_t; // wait queue for condition variable

__noinline void lwsync_cond_init(lwsync_cond_t *c)
{
	waitq_init(c);
}

__noinline bool_t lwsync_cond_timedwait(lwsync_cond_t *c, lwsync_mutex_t *m, timeout_t timeout)
{
	prio_t old_prio = prio_raise(max_prio);
	spin_lock(&m->lock);

	waitq_add_ordered(c, SELF, old_prio);
	uint32_t self_seq = ++SELF->ustate;

	uint32_t next_seq; // handover mutex
	thread_t *next = waitq_remove_highest(&m->waitq);
	if (next != NULL) {
		m->stat_uc++;	// STATISTICS
		m->owner = next->tid;
		next_seq = ++next->ustate;
	} else {
		m->stat_uu++;	// STATISTICS
		m->owner = LWSYNC_UNLOCKED;
	}

	spin_unlock(&m->lock);
	if (next != NULL) { // wake next waiter on the support mutex
		sys_wake_set_prio(next->tid, &next->ustate, next_seq, max_prio);
	}
	sys_wait_at_prio(&SELF->ustate, self_seq, timeout, old_prio);
	spin_lock(&m->lock);

	bool_t notified = (SELF->ustate != self_seq);
	if (notified) {
		// properly notified, no longer on c's wait queue
		if (m->owner == SELF->tid) {
			spin_unlock(&m->lock); // already the mutex owner, leave
			goto out;
		}
		self_seq = SELF->ustate; // on m's wait queue, wait for mutex
	} else {
		// not notified, remove from c's wait queue
		waitq_remove(c, SELF);
		if (m->owner == LWSYNC_UNLOCKED) { // re-apply for mutex
			m->stat_lu++;	// STATISTICS
			m->owner = SELF->tid;
			spin_unlock(&m->lock);
			goto out;
		}
		m->stat_lc++;	// STATISTICS
		waitq_add_ordered(&m->waitq, SELF, old_prio); // wait for mutex
		self_seq = ++SELF->ustate;
	}
	spin_unlock(&m->lock);
	sys_wait_at_prio(&SELF->ustate, self_seq, INFINITE, old_prio);
	assert(m->owner == SELF->tid);
out:
	prio_restore(old_prio);
	return notified;
}

// notify one, assumes the support mutex is currently locked
__noinline void lwsync_cond_notify_locked(lwsync_cond_t *c, lwsync_mutex_t *m)
{
	assert(m->owner == SELF->tid);

	prio_t old_prio = prio_raise(max_prio);
    spin_lock(&m->lock);
	thread_t *thr;

	// move threads from condition variable wait queue to mutex wait queue
	if ((thr = waitq_remove_highest(c)) != NULL) {
		++thr->ustate;
		waitq_add_ordered(&m->waitq, thr, thr->waitq_prio);
	}

    spin_unlock(&m->lock);
	prio_restore(old_prio);
}

// notify all, assumes the support mutex is currently locked
__noinline void lwsync_cond_notify_all_locked(lwsync_cond_t *c, lwsync_mutex_t *m)
{
	assert(m->owner == SELF->tid);

	prio_t old_prio = prio_raise(max_prio);
    spin_lock(&m->lock);
	thread_t *thr;

	// move threads from condition variable wait queue to mutex wait queue
	while ((thr = waitq_remove_highest(c)) != NULL) {
		++thr->ustate;
		waitq_add_ordered(&m->waitq, thr, thr->waitq_prio);
	}

    spin_unlock(&m->lock);
	prio_restore(old_prio);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/* futex mutex API */
#define WAITERS SYS_FUTEX_WAITERS

/* since TID=0 is a valid TID, we set another bit to indicate a locked mutex */
#define FUTEX_LOCKED SYS_FUTEX_LOCKED

typedef struct {
	uint32_t lock;
	uint32_t stat_lu;	// STATISTICS: lock uncontended
	uint32_t stat_lc;	// STATISTICS: lock contended
	uint32_t stat_lf;	// STATISTICS: lock fastpath in the kernel
	uint32_t stat_uu;	// STATISTICS: unlock uncontended
	uint32_t stat_uc;	// STATISTICS: unlock contended
	uint32_t stat_uf;	// STATISTICS: unlock fastpath in the kernel
} futex_mutex_t __aligned(64);

__noinline void futex_mutex_init(futex_mutex_t *m)
{
	m->lock = 0;
	m->stat_lu = 0;	// STATISTICS
	m->stat_lc = 0;	// STATISTICS
	m->stat_lf = 0;	// STATISTICS
	m->stat_uu = 0;	// STATISTICS
	m->stat_uc = 0;	// STATISTICS
	m->stat_uf = 0;	// STATISTICS
}

__noinline bool_t futex_mutex_timedlock(futex_mutex_t *m, timeout_t timeout)
{
	uint32_t val;

retry:
	val = access_once(m->lock);

	/* fast path -- unlocked mutex */
	if (likely(val == 0)) {
		if (CAS_RELAXED(&m->lock, val, SELF->tid | FUTEX_LOCKED) == 0) {
			goto retry;
		}
		ACQUIRE_BARRIER();
		m->stat_lu++;	// STATISTICS
		return TRUE;
	}

	/* slow path -- system call */
	/* enable extra statistics mode (set bit 0 in flags) */
	err_t err = sys_futex_lock(&m->lock, 0x1, timeout);
	assert((err == EOK) || (err == 0x100) || (err == ETIMEOUT));
	if (likely((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS))) {
		assert(err == EOK || err == 0x100);
		ACQUIRE_BARRIER();
		m->stat_lc++;	// STATISTICS
		if (err == 0x100) {
			m->stat_lf++;
		}
		return TRUE;
	}
	return FALSE;
}

__noinline bool_t futex_mutex_trylock(futex_mutex_t *m)
{
	uint32_t val;

retry:
	val = access_once(m->lock);

	/* fast path -- unlocked mutex */
	if (likely(val == 0)) {
		if (CAS_RELAXED(&m->lock, val, SELF->tid | FUTEX_LOCKED) == 0) {
			goto retry;
		}
		ACQUIRE_BARRIER();
		m->stat_lu++;	// STATISTICS
		return TRUE;
	}

	return FALSE;
}

__noinline void futex_mutex_unlock(futex_mutex_t *m)
{
	uint32_t val;
	err_t err;

	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));

	m->stat_uu++;	// STATISTICS
	RELEASE_BARRIER();

retry:
	val = access_once(m->lock);

	/* fast path -- uncontended mutex */
	if (likely((val & WAITERS) == 0)) {
		if (CAS_RELAXED(&m->lock, val, 0) == 0) {
			goto retry;
		}
		return;
	}

	/* slow path -- system call */
	m->stat_uu--;	// STATISTICS
	m->stat_uc++;	// STATISTICS
	err = sys_futex_unlock(&m->lock, 0x1);
	assert((err == EOK) || (err == 0x100));
	if (err == 0x100) {
		atomic_add_relaxed(&m->stat_uf, 1);	// STATISTICS
	}
	(void)err;
}

__noinline void futex_mutex_stat(futex_mutex_t *m, lock_stat_t *s)
{
	s->lu = m->stat_lu;	// STATISTICS
	s->lc = m->stat_lc;	// STATISTICS
	s->lf = m->stat_lf;	// STATISTICS
	s->uu = m->stat_uu;	// STATISTICS
	s->uc = m->stat_uc;	// STATISTICS
	s->uf = m->stat_uf;	// STATISTICS
}

////////////////////////////////////////////////////////////////////////////////

typedef struct {
	uint32_t round;
} futex_cond_t;

__noinline void futex_cond_init(futex_cond_t *c)
{
	c->round = 0;
}

__noinline bool_t futex_cond_timedwait(futex_cond_t *c, futex_mutex_t *m, timeout_t timeout)
{
	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));
	uint32_t val;
	err_t err;

	val = atomic_load_relaxed(&c->round);

	futex_mutex_unlock(m);
	err = sys_futex_wait(&c->round, val, timeout);
	assert(err == EOK); // FIXME: handle ESTATE and ETIMEOUT (not used by benchmark)
	(void)err;

	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));
	return true;
}

__noinline void futex_cond_notify_locked(futex_cond_t *c, futex_mutex_t *m)
{
	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));
	uint32_t val;
	err_t err;

	/* increment round */
	val = atomic_add_relaxed(&c->round, 1);

	err = sys_futex_requeue(&c->round, 0, val, &m->lock, 1);
	assert(err == EOK); // FIXME: handle ESTATE
	(void)err;

	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));
}

__noinline void futex_cond_notify_all_locked(futex_cond_t *c, futex_mutex_t *m)
{
	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));
	uint32_t val;
	err_t err;

	/* increment round */
	val = atomic_add_relaxed(&c->round, 1);

	err = sys_futex_requeue(&c->round, 0, val, &m->lock, -1);
	assert(err == EOK); // FIXME: handle ESTATE
	(void)err;

	assert((m->lock | WAITERS) == (SELF->tid | FUTEX_LOCKED | WAITERS));
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef struct {
    spin_t lock;        // internal spinlock
    prio_t old_prio;    // previous priority of thread in monitor
    waitq_t waitq;      // condition variable wait queue
    thread_t *wakeup;   // thread to wake up when leaving the monitor
} mon_t;

__noinline void mon_init(mon_t *m)
{
	spin_init(&m->lock);
	waitq_init(&m->waitq);
	m->wakeup = NULL;
}

__noinline void mon_enter(mon_t *m) {
    prio_t old_prio = prio_raise(max_prio);
    spin_lock(&m->lock);
    m->old_prio = old_prio;
}

// wait in monitor; returns TRUE if thread was woken up properly
__noinline bool_t mon_timedwait(mon_t *m, timeout_t timeout) {
    waitq_add_ordered(&m->waitq, SELF, m->old_prio);
    uint32_t seq = ++SELF->ustate;

    uint32_t old_prio = m->old_prio;
    spin_unlock(&m->lock);
    (void)sys_wait_at_prio(&SELF->ustate, seq, timeout, old_prio);
    spin_lock(&m->lock);
    m->old_prio = old_prio;

    bool_t notified = (SELF->ustate != seq);
    if (unlikely(notified == FALSE)) {
        waitq_remove(&m->waitq, SELF);
    }
    return notified;
}

__noinline void mon_notify(mon_t *m) {
    if (waitq_is_empty(&m->waitq) == FALSE) {
        thread_t *thr = m->wakeup;
        if (unlikely(thr != NULL)) {
            uint32_t seq = ++thr->ustate;
            sys_wake_set_prio(thr->tid, &thr->ustate, seq, max_prio);
        }
        m->wakeup = waitq_remove_highest(&m->waitq);
    }
}

__noinline void mon_leave(mon_t *m) {
    prio_t old_prio = m->old_prio;
    thread_t *thr = m->wakeup;
    if (thr != NULL) {
        uint32_t seq = ++thr->ustate;
        spin_unlock(&m->lock);
        sys_wake_set_prio(thr->tid, &thr->ustate, seq, old_prio);
    } else {
        spin_unlock(&m->lock);
        prio_restore(old_prio);
    }
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/* syscall mutex API */
typedef struct {
	uint32_t mutex_id;
} syscall_mutex_t;

__noinline void syscall_mutex_init(syscall_mutex_t *m)
{
	err_t err;

	/* we can use sync object 0 exclusively */
	m->mutex_id = SYNC_OBJECT_MUTEX;
	err = sys_mutex_init(m->mutex_id, 0);
	assert((err == EOK) || (err == ESTATE));
	(void)err;
}

__noinline void syscall_mutex_init_explicit(syscall_mutex_t *m, uint32_t mutex_id)
{
	err_t err;

	/* we can use sync object 0 exclusively */
	m->mutex_id = mutex_id;
	err = sys_mutex_init(m->mutex_id, 0);
	assert((err == EOK) || (err == ESTATE));
	(void)err;
}

__noinline bool_t syscall_mutex_timedlock(syscall_mutex_t *m, timeout_t timeout)
{
	err_t err;

	err = sys_mutex_lock(m->mutex_id, 0, timeout);
	assert((err == EOK) || (err == ETIMEOUT));
	if (likely(err == EOK)) {
		// ACQUIRE_BARRIER(); // barriers are in the kernel
		return TRUE;
	}
	return FALSE;
}

__noinline bool_t syscall_mutex_trylock(syscall_mutex_t *m)
{
	err_t err;

	err = sys_mutex_trylock(m->mutex_id, 0);
	assert((err == EOK) || (err == EBUSY));
	if (likely(err == EOK)) {
		// ACQUIRE_BARRIER(); // barriers are in the kernel
		return TRUE;
	}
	return FALSE;
}

__noinline void syscall_mutex_unlock(syscall_mutex_t *m)
{
	err_t err;

	// RELEASE_BARRIER(); // barriers are in the kernel
	err = sys_mutex_unlock(m->mutex_id);
	assert(err == EOK);
	(void)err;
}

__noinline void syscall_mutex_stat(syscall_mutex_t *m, lock_stat_t *s)
{
	err_t err;

	s->lf = 0;	// STATISTICS
	s->uf = 0;	// STATISTICS
	err = sys_mutex_stat(m->mutex_id, &s->lu, &s->lc, &s->uu, &s->uc);
	assert(err == EOK);
	(void)err;
}

////////////////////////////////////////////////////////////////////////////////

/* syscall cond API */
typedef struct {
	uint32_t cond_id;
} syscall_cond_t;

__noinline void syscall_cond_init(syscall_cond_t *c)
{
	err_t err;

	/* we can use sync object 1 exclusively */
	c->cond_id = SYNC_OBJECT_COND;
	err = sys_cond_init(c->cond_id, 0);
	assert((err == EOK) || (err == ESTATE));
	(void)err;
}

__noinline void syscall_cond_timedwait(syscall_cond_t *c, syscall_mutex_t *m, timeout_t timeout)
{
	err_t err;

	/* we can use sync object 1 exclusively */
	err = sys_cond_wait(c->cond_id, m->mutex_id, timeout);
	assert((err == EOK) || (err == ESTATE));
	(void)err;
}

__noinline void syscall_cond_notify_locked(syscall_cond_t *c, syscall_mutex_t *m __unused)
{
	err_t err;

	err = sys_cond_wake(c->cond_id, false);
	assert(err == EOK);
	(void)err;
}

__noinline void syscall_cond_notify_all_locked(syscall_cond_t *c, syscall_mutex_t *m __unused)
{
	err_t err;

	err = sys_cond_wake(c->cond_id, true);
	assert(err == EOK);
	(void)err;
}


////////////////////////////////////////////////////////////////////////////////

/* sync objects */
static syscall_mutex_t sm;
static syscall_cond_t sc;

static futex_mutex_t fm;
static futex_cond_t fc;

static lwsync_mutex_t lm;
static lwsync_cond_t lc;

static mon_t mon;

static volatile int thread_run;

////////////////////////////////////////////////////////////////////////////////

static void bench_self_suspender(void *unused __unused)
{
	while (thread_run) {
		sys_thread_suspend();
	}
	sys_thread_exit();
}

static void bench_building_blocks(void)
{
	TIME_T before, after;
	unsigned int runs;
	spin_t s;
	uint32_t a, r;

	spin_init(&s);

	printf("- atomic_acquire_barrier\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		ACQUIRE_BARRIER();
	}
	after = GETTS();
	delta(before, after);


	printf("- atomic_release_barrier\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		RELEASE_BARRIER();
	}
	after = GETTS();
	delta(before, after);


	printf("- atomic_cas_relaxed success\t\t\t\t");
	before = GETTS();
	a = 0;
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		CAS_RELAXED(&a, r, r+1);
		r++;
	}
	after = GETTS();
	delta(before, after);


	printf("- null syscall\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self_syscall();
	}
	after = GETTS();
	delta(before, after);


	printf("- preempt syscall (no preemption)\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_preempt();
	}
	after = GETTS();
	delta(before, after);


	printf("- futex CAS 0>1/acq/req/1>0 seq\t\t\t\t");
	before = GETTS();
	a = 0;
	for (runs = 0; runs < RUNS; runs++) {
		CAS_RELAXED(&a, 0, 1);
		ACQUIRE_BARRIER();
		RELEASE_BARRIER();
		CAS_RELAXED(&a, 1, 0);
	}
	after = GETTS();
	delta(before, after);


	printf("- uncontended prio_raise/restore pair\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		prio_t prev_prio = prio_raise(max_prio);
		prio_restore(prev_prio);
	}
	after = GETTS();
	delta(before, after);


	printf("- uncontended spin_lock/unlock pair\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		spin_lock(&s);
		spin_unlock(&s);
	}
	after = GETTS();
	delta(before, after);


	printf("- two prio+/spin_lock/unlock/prio- seq\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		prio_t prev_prio = prio_raise(max_prio);
		spin_lock(&s);
		spin_unlock(&s);
		prio_restore(prev_prio);
		prev_prio = prio_raise(max_prio);
		spin_lock(&s);
		spin_unlock(&s);
		prio_restore(prev_prio);
	}
	after = GETTS();
	delta(before, after);


	printf("- two NULL syscalls\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self_syscall();
		sys_thread_self_syscall();
	}
	after = GETTS();
	delta(before, after);


	printf("- futex_wait failed compare\t\t\t\t");
	a = 47;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)sys_futex_wait(&a, 11, INFINITE);
	}
	after = GETTS();
	delta(before, after);


	printf("- futex_wake empty wait queue\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)sys_futex_wake(&a, 1);
	}
	after = GETTS();
	delta(before, after);


	printf("- lwsync_wait failed compare\t\t\t\t");
	a = 47;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)sys_lwsync_wait(&a, 11, INFINITE, 0);
	}
	after = GETTS();
	delta(before, after);


	printf("- lwsync_wake failed compare\t\t\t\t");
	a = 47;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)sys_lwsync_wake(0, &a, 11, BENCH_THREAD_PRIO);
	}
	after = GETTS();
	delta(before, after);


	printf("- resume high-prio thread -> suspend (same CPU)\t\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_self_suspender, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_resume(1);
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- resume high-prio thread -> suspend (other CPU)\t");
	if (num_cpus > 1) {
		// start waiter thread
		thread_run = 1;
		create_thread_cpu(1, bench_self_suspender, NULL, WAITER_THREAD_PRIO, 1);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			sys_thread_resume(1);
		}
		after = GETTS();
		delta(before, after);

		// end waiter thread
		thread_run = 0;
		sys_thread_resume(1);

		/* short break to let the thread on the other CPU stop */
		sys_sleep(sys_time_get() + 1000*1000);
	} else {
		printf("skipped\n");
	}
}

////////////////////////////////////////////////////////////////////////////////

static void bench_mutex_suspender_syscall(void *unused __unused)
{
	bool_t success;

	while (thread_run) {
		sys_thread_suspend();

		success = syscall_mutex_timedlock(&sm, INFINITE);
		assert(success != FALSE);
		(void)success;

		syscall_mutex_unlock(&sm);
	}
	sys_thread_exit();
}

static void bench_mutex_suspender_futex(void *unused __unused)
{
	bool_t success;

	while (thread_run) {
		sys_thread_suspend();

		success = futex_mutex_timedlock(&fm, INFINITE);
		assert(success != FALSE);
		(void)success;

		futex_mutex_unlock(&fm);

	}
	sys_thread_exit();
}

static void bench_mutex_suspender_lwsync(void *unused __unused)
{
	bool_t success;

	while (thread_run) {
		sys_thread_suspend();

		success = lwsync_mutex_timedlock(&lm, INFINITE);
		assert(success != FALSE);
		(void)success;

		lwsync_mutex_unlock(&lm);
	}
	sys_thread_exit();
}

static void bench_mutex_lock_unlock(void)
{
	TIME_T base1, base2, base3;
	TIME_T before, after;
	unsigned int runs;
	bool_t success;

	printf("- syscall: mutex lock/unlock pair (uncontended)\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = syscall_mutex_timedlock(&sm, INFINITE);
		assert(success != FALSE);
		(void)success;
		syscall_mutex_unlock(&sm);
	}
	after = GETTS();
	delta(before, after);
	base1 = after - before;


	printf("- syscall: mutex lock/unlock pair + resume high prio\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_self_suspender, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = syscall_mutex_timedlock(&sm, INFINITE);
		assert(success != FALSE);
		(void)success;

		sys_thread_resume(1);

		syscall_mutex_unlock(&sm);
	}
	after = GETTS();
	delta(before, after);
	base2 = after - before;

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- syscall: mutex lock/unlock pair (contended)\t\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_mutex_suspender_syscall, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = syscall_mutex_timedlock(&sm, INFINITE);
		assert(success != FALSE);
		(void)success;

		sys_thread_resume(1);

		syscall_mutex_unlock(&sm);
	}
	after = GETTS();
	delta(before, after);
	base3 = after - before;

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- syscall: -- overhead removed -- (contended)\t\t");
	(void)base1;
	delta(base2, base3);

	//--------------------------------------------------------------------------

	printf("- futex:   mutex lock/unlock pair (uncontended)\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = futex_mutex_timedlock(&fm, INFINITE);
		assert(success != FALSE);
		(void)success;
		futex_mutex_unlock(&fm);
	}
	after = GETTS();
	delta(before, after);
	base1 = after - before;


	printf("- futex:   mutex lock/unlock pair + resume high prio\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_self_suspender, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = futex_mutex_timedlock(&fm, INFINITE);
		assert(success != FALSE);
		(void)success;

		sys_thread_resume(1);

		futex_mutex_unlock(&fm);
	}
	after = GETTS();
	delta(before, after);
	base2 = after - before;

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- futex:   mutex lock/unlock pair (contended)\t\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_mutex_suspender_futex, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = futex_mutex_timedlock(&fm, INFINITE);
		assert(success != FALSE);
		(void)success;

		sys_thread_resume(1);

		futex_mutex_unlock(&fm);
	}
	after = GETTS();
	delta(before, after);
	base3 = after - before;

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- futex:   -- overhead removed -- (contended)\t\t");
	(void)base1;
	delta(base2, base3);

	//--------------------------------------------------------------------------

	printf("- lwsync:  mutex lock/unlock pair (uncontended)\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = lwsync_mutex_timedlock(&lm, INFINITE);
		assert(success != FALSE);
		(void)success;
		lwsync_mutex_unlock(&lm);
	}
	after = GETTS();
	delta(before, after);
	base1 = after - before;


	printf("- lwsync:  mutex lock/unlock pair + resume high prio\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_self_suspender, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = lwsync_mutex_timedlock(&lm, INFINITE);
		assert(success != FALSE);
		(void)success;

		sys_thread_resume(1);

		lwsync_mutex_unlock(&lm);
	}
	after = GETTS();
	delta(before, after);
	base2 = after - before;

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- lwsync:  mutex lock/unlock pair (contended)\t\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_mutex_suspender_lwsync, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		success = lwsync_mutex_timedlock(&lm, INFINITE);
		assert(success != FALSE);
		(void)success;

		sys_thread_resume(1);

		lwsync_mutex_unlock(&lm);
	}
	after = GETTS();
	delta(before, after);
	base3 = after - before;

	// end waiter thread
	thread_run = 0;
	sys_thread_resume(1);


	printf("- lwsync:  -- overhead removed -- (contended)\t\t");
	(void)base1;
	delta(base2, base3);
}

////////////////////////////////////////////////////////////////////////////////

static uint32_t wait_barrier __aligned(64);

static void bench_mutex_smp_empty(void *unused __unused)
{
	uint32_t my_mask = 1u << sys_cpu_get();

	while (thread_run) {
		/* spin as long as our bit is set */
		while ((atomic_load_relaxed(&wait_barrier) & my_mask) != 0) {
			;
		}
		/* our bit was cleared */

		/* empty */

		/* set our bit */
		atomic_set_mask_relaxed(&wait_barrier, my_mask);
	}
	sys_thread_exit();
}

static void bench_mutex_smp_syscall(void *unused __unused)
{
	uint32_t my_mask = 1u << sys_cpu_get();

	while (thread_run) {
		/* spin as long as our bit is set */
		while ((atomic_load_relaxed(&wait_barrier) & my_mask) != 0) {
			;
		}
		/* our bit was cleared */

		/* spin to acquire the lock */
		while (syscall_mutex_trylock(&sm) == FALSE) {
			;
		}
		syscall_mutex_unlock(&sm);

		/* set our bit */
		atomic_set_mask_relaxed(&wait_barrier, my_mask);
	}
	sys_thread_exit();
}

static void bench_mutex_smp_futex(void *unused __unused)
{
	uint32_t my_mask = 1u << sys_cpu_get();

	while (thread_run) {
		/* spin as long as our bit is set */
		while ((atomic_load_relaxed(&wait_barrier) & my_mask) != 0) {
			;
		}
		/* our bit was cleared */

		/* spin to acquire the lock */
		while (futex_mutex_trylock(&fm) == FALSE) {
			;
		}
		futex_mutex_unlock(&fm);

		/* set our bit */
		atomic_set_mask_relaxed(&wait_barrier, my_mask);
	}
	sys_thread_exit();
}

static void bench_mutex_smp_lwsync(void *unused __unused)
{
	uint32_t my_mask = 1u << sys_cpu_get();

	while (thread_run) {
		/* spin as long as our bit is set */
		while ((atomic_load_relaxed(&wait_barrier) & my_mask) != 0) {
			;
		}
		/* our bit was cleared */

		/* spin to acquire the lock */
		while (lwsync_mutex_trylock(&lm) == FALSE) {
			;
		}
		lwsync_mutex_unlock(&lm);

		/* set our bit */
		atomic_set_mask_relaxed(&wait_barrier, my_mask);
	}
	sys_thread_exit();
}

static void bench_mutex_smp(unsigned int cpus)
{
	TIME_T before, after;
	unsigned int runs;
	unsigned int cpu;

	uint32_t others_mask = ((1u << cpus) - 1) & ~1u;

	//--------------------------------------------------------------------------

	printf("- ---:     ----------------- barrier --- (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_empty, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* empty */

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);

	//--------------------------------------------------------------------------

	printf("- syscall: mutex lock/unlock barrier me  (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_empty, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* spin to acquire the lock */
		while (syscall_mutex_trylock(&sm) == FALSE) {
			;
		}
		syscall_mutex_unlock(&sm);

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);

	//--------------------------------------------------------------------------

	printf("- syscall: mutex lock/unlock barrier all (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_syscall, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* spin to acquire the lock */
		while (syscall_mutex_trylock(&sm) == FALSE) {
			;
		}
		syscall_mutex_unlock(&sm);

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);

	//--------------------------------------------------------------------------

	printf("- futex:   mutex lock/unlock barrier me  (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_empty, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* spin to acquire the lock */
		while (futex_mutex_trylock(&fm) == FALSE) {
			;
		}
		futex_mutex_unlock(&fm);

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);

	//--------------------------------------------------------------------------

	printf("- futex:   mutex lock/unlock barrier all (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_futex, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* spin to acquire the lock */
		while (futex_mutex_trylock(&fm) == FALSE) {
			;
		}
		futex_mutex_unlock(&fm);

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);

	//--------------------------------------------------------------------------

	printf("- lwsync:  mutex lock/unlock barrier me  (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_empty, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* spin to acquire the lock */
		while (lwsync_mutex_trylock(&lm) == FALSE) {
			;
		}
		lwsync_mutex_unlock(&lm);

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);

	//--------------------------------------------------------------------------

	printf("- lwsync:  mutex lock/unlock barrier all (%d CPUs)\t", cpus);
	// start waiter threads
	access_once(wait_barrier) = others_mask;
	atomic_store_release(&thread_run, 1);
	for (cpu = 1; cpu < cpus; cpu++) {
		create_thread_cpu(cpu, bench_mutex_smp_lwsync, NULL, WAITER_THREAD_PRIO, cpu);
	}

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* let other CPUs run */
		assert(atomic_load_relaxed(&wait_barrier) == others_mask);
		atomic_store_relaxed(&wait_barrier, 0);

		/* spin to acquire the lock */
		while (lwsync_mutex_trylock(&lm) == FALSE) {
			;
		}
		lwsync_mutex_unlock(&lm);

		/* wait until they have reached the barrier */
		while (atomic_load_relaxed(&wait_barrier) != others_mask) {
			;
		}
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	atomic_store_release(&thread_run, 0);
	atomic_store_release(&wait_barrier, 0);
	/* wait until they have reached the barrier */
	while (atomic_load_relaxed(&wait_barrier) != others_mask) {
		;
	}
	sys_sleep(sys_time_get() + BENCH_INTERTEST_DELAY);
}

////////////////////////////////////////////////////////////////////////////////

static void bench_mon_cond_waiter(void *unused __unused)
{
	while (thread_run) {
		mon_enter(&mon);
		mon_timedwait(&mon, INFINITE);
		mon_leave(&mon);
	}
	sys_thread_exit();
}

static void bench_mon_cond(void)
{
	TIME_T before, after;
	unsigned int runs;

	// testcase: wait for monitor
	mon_enter(&mon);
	bool_t notified = mon_timedwait(&mon, 0);
	assert(notified == FALSE);
	(void)notified;
	mon_leave(&mon);


	printf("- lwsync: monitor enter/leave seq\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mon_enter(&mon);
		mon_leave(&mon);
	}
	after = GETTS();
	delta(before, after);


	printf("- lwsync: monitor enter/notify/leave seq (no waiter)\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mon_enter(&mon);
		mon_notify(&mon);
		mon_leave(&mon);
	}
	after = GETTS();
	delta(before, after);


	printf("- lwsync: monitor enter/notify/leave seq (w/ waiter)\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_mon_cond_waiter, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mon_enter(&mon);
		mon_notify(&mon);
		mon_leave(&mon);
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	thread_run = 0;
	mon_enter(&mon);
	mon_notify(&mon);
	mon_leave(&mon);
}

////////////////////////////////////////////////////////////////////////////////

static void bench_syscall_cond_waiter(void *unused __unused)
{
	while (thread_run) {
		syscall_mutex_timedlock(&sm, INFINITE);
		syscall_cond_timedwait(&sc, &sm, INFINITE);
		syscall_mutex_unlock(&sm);
	}
	sys_thread_exit();
}

static void bench_syscall_cond(void)
{
	TIME_T before, after;
	unsigned int runs;

	printf("- syscall: mutex lock/notify/unlock seq (no waiter)\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		syscall_mutex_timedlock(&sm, INFINITE);
		syscall_cond_notify_locked(&sc, &sm);
		syscall_mutex_unlock(&sm);
	}
	after = GETTS();
	delta(before, after);


	printf("- syscall: mutex lock/notify/unlock seq (w/ waiter)\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_syscall_cond_waiter, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		syscall_mutex_timedlock(&sm, INFINITE);
		syscall_cond_notify_locked(&sc, &sm);
		syscall_mutex_unlock(&sm);
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	thread_run = 0;
	syscall_mutex_timedlock(&sm, INFINITE);
	syscall_cond_notify_locked(&sc, &sm);
	syscall_mutex_unlock(&sm);
}

////////////////////////////////////////////////////////////////////////////////

static void bench_futex_cond_waiter(void *unused __unused)
{
	while (thread_run) {
		futex_mutex_timedlock(&fm, INFINITE);
		futex_cond_timedwait(&fc, &fm, INFINITE);
		futex_mutex_unlock(&fm);
	}
	sys_thread_exit();
}

static void bench_futex_cond(void)
{
	TIME_T before, after;
	unsigned int runs;

	printf("- futex: mutex lock/notify/unlock seq (no waiter)\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		futex_mutex_timedlock(&fm, INFINITE);
		futex_cond_notify_locked(&fc, &fm);
		futex_mutex_unlock(&fm);
	}
	after = GETTS();
	delta(before, after);


	printf("- futex: mutex lock/notify/unlock seq (w/ waiter)\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_futex_cond_waiter, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		futex_mutex_timedlock(&fm, INFINITE);
		futex_cond_notify_locked(&fc, &fm);
		futex_mutex_unlock(&fm);
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	thread_run = 0;
	futex_mutex_timedlock(&fm, INFINITE);
	futex_cond_notify_locked(&fc, &fm);
	futex_mutex_unlock(&fm);
}

////////////////////////////////////////////////////////////////////////////////

static void bench_lwsync_cond_waiter(void *unused __unused)
{
	while (thread_run) {
		lwsync_mutex_timedlock(&lm, INFINITE);
		lwsync_cond_timedwait(&lc, &lm, INFINITE);
		lwsync_mutex_unlock(&lm);
	}
	sys_thread_exit();
}

static void bench_lwsync_cond(void)
{
	TIME_T before, after;
	unsigned int runs;

	printf("- lwsync: mutex lock/notify/unlock seq (no waiter)\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		lwsync_mutex_timedlock(&lm, INFINITE);
		lwsync_cond_notify_locked(&lc, &lm);
		lwsync_mutex_unlock(&lm);
	}
	after = GETTS();
	delta(before, after);


	printf("- lwsync: mutex lock/notify/unlock seq (w/ waiter)\t");
	// start waiter thread
	thread_run = 1;
	create_thread(1, bench_lwsync_cond_waiter, NULL, WAITER_THREAD_PRIO);

	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		lwsync_mutex_timedlock(&lm, INFINITE);
		lwsync_cond_notify_locked(&lc, &lm);
		lwsync_mutex_unlock(&lm);
	}
	after = GETTS();
	delta(before, after);

	// end waiter thread
	thread_run = 0;
	lwsync_mutex_timedlock(&lm, INFINITE);
	lwsync_cond_notify_locked(&lc, &lm);
	lwsync_mutex_unlock(&lm);
}

////////////////////////////////////////////////////////////////////////////////

static void bench_header(char separator, const char *msg)
{
	unsigned int i;

	printf("\n");

	for (i = 0; i < 20; i++) {
		printf("%c", separator);
	}

	printf(" ");
	i++;

	for (; *msg != '\0'; msg++, i++) {
		printf("%c", *msg);
	}

	printf(" ");
	i++;

	for (; i < 72; i++) {
		printf("%c", separator);
	}

	printf("\n\n");
}


void bench_ecrts2020(void)
{
	prio_t orig_prio;

	/* init global state */
	max_prio = sys_prio_max();
	orig_prio = sys_prio_set(BENCH_THREAD_PRIO);
	assert(SELF->sys.next_prio <= BENCH_THREAD_PRIO);

	/* some tests require an SMP system */
	num_cpus = 0;
	for (unsigned long cpu = 0, cpu_mask = sys_cpu_mask();
			(cpu < sizeof(cpu_mask)*8) && ((cpu_mask & (1ul << cpu)) != 0);
	     cpu++) {
		num_cpus++;
	}
	assert(num_cpus > 0);
	assert(num_cpus <= 64);

	syscall_mutex_init(&sm);
	futex_mutex_init(&fm);
	lwsync_mutex_init(&lm);
	syscall_cond_init(&sc);
	futex_cond_init(&fc);
	lwsync_cond_init(&lc);
	mon_init(&mon);

	bench_header('#', "URAMAKI -- ECRTS 2020 PAPER -- START");

	bench_building_blocks();
	printf("\n");
	bench_mutex_lock_unlock();
	printf("\n");
	for (unsigned int cpus = 2; cpus <= num_cpus; cpus++) {
		bench_mutex_smp(cpus);
		printf("\n");
	}
	bench_syscall_cond();
	printf("\n");
	bench_futex_cond();
	printf("\n");
	bench_lwsync_cond();
	printf("\n");
	bench_mon_cond();

	bench_header('#', "URAMAKI -- ECRTS 2020 PAPER -- END");

	sys_prio_set(orig_prio);

#ifdef RUN_ONCE
	sys_sleep(TIMEOUT_INFINITE);
#endif
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/* the number of rounds is the square of this value
 * to fit the Feistel permutation:
 *   8 -> 2^16 rounds
 *   9 -> 2^18 rounds
 *  10 -> 2^20 rounds
 *  11 -> 2^22 rounds
 *  12 -> 2^24 rounds
 */
#define HALF_BITS 12
#define MAX_NUM ((1u << HALF_BITS)*(1u << HALF_BITS))

/* buckets: up to 1024, in power of two, given in bits */
#define BUCKET_BITS 6
#define BUCKETS (1u << BUCKET_BITS)

#define DISTRIB_F(x) distrib_f_2_0(x)

/* Cycles per microsecond (i.MX6 runs at 792 MHz) */
#define CYCLES_PER_US 792

/* Feistel permutation: 2*HALF_BITS bit input -> 2*HALF_BITS bit output */
static inline uint32_t feistel(uint32_t in)
{
	assert(HALF_BITS >= 1 && HALF_BITS <= 16);
	uint32_t half_mask = (1u << HALF_BITS) - 1;
	uint32_t l = in & half_mask;
	uint32_t r = (in >> HALF_BITS) & half_mask;

	/* four rounds are sufficient for a strong pseudorandom permutation */
	for (unsigned int i = 0; i < 4; i++) {
		uint32_t r_prev = r;
		uint32_t f = (((r * 11) + (r >> 5) + 7 * 127) ^ r) & half_mask;
		r = l ^ f;
		l = r_prev;
	}

	uint32_t full_mask = (((1u << (2*HALF_BITS - 1)) - 1) << 1) | 1;
	return ((r << HALF_BITS) | l) & full_mask;
}

/* integer square root:
 * https://en.wikipedia.org/wiki/Integer_square_root
 */
static inline uint32_t isqrt(uint32_t in)
{
	uint32_t op = in;
	uint32_t res = 0;
	uint32_t one = 1u << 30;

	while (one > op) {
		one >>= 2;
	}

	while (one != 0) {
		if (op >= res + one) {
			op = op - (res + one);
			res = res +  2 * one;
		}
		res >>= 1;
		one >>= 2;
	}
	return res;
}


/* distribution function: f(x) = x^1.5 */
static inline uint32_t distrib_f_1_5(uint32_t x)
{
	uint64_t y;

	assert(x < MAX_NUM);
	assert((4 * HALF_BITS) < 64);

	y = x;
	/* max out the integer square root, e.g. 2^24 * 2^8 is 2^32 at most */
	y *= isqrt(x * (1u << (32 - 2*HALF_BITS)));
	y /= (1u << ((32 - 2*HALF_BITS) / 2));
	y >>= (3 * HALF_BITS - BUCKET_BITS);

	assert(y < BUCKETS);
	return y;
}

/* distribution function: f(x) = x^2 */
static inline uint32_t distrib_f_2_0(uint32_t x)
{
	uint64_t y;

	assert(x < MAX_NUM);
	assert((4 * HALF_BITS) < 64);
	y = x;
	y *= x;
	y >>= (4 * HALF_BITS - BUCKET_BITS);

	assert(y < BUCKETS);
	return y;
}

/* distribution function: f(x) = x^2.5 */
static inline uint32_t distrib_f_2_5(uint32_t x)
{
	uint64_t y;


	assert(x < MAX_NUM);
	y = x;
	/* max out the integer square root, e.g. 2^24 * 2^8 is 2^32 at most */
	y *= isqrt(x * (1u << (32 - 2*HALF_BITS)));
	y /= (1u << ((32 - 2*HALF_BITS) / 2));
	y *= x;
	y >>= (5 * HALF_BITS - BUCKET_BITS);

	assert(y < BUCKETS);
	return y;
}


/* distribution function: f(x) = x^3 */
static inline uint32_t distrib_f_3_0(uint32_t x)
{
	uint64_t y;

	assert(x < MAX_NUM);
	assert((5 * HALF_BITS) < 64);
	y = x;
	y *= x;
	/* early shift, 2^14*2^14*2^14 bits would exceed the 64-bit value */
	y >>= HALF_BITS;
	y *= x;
	y >>= (5 * HALF_BITS - BUCKET_BITS);

	assert(y < BUCKETS);
	return y;
}

/* distribution function: f(x) = x^4 */
static inline uint32_t distrib_f_4_0(uint32_t x)
{
	uint64_t y;

	assert(x < MAX_NUM);
	y = x;
	y *= x;
	/* early shift */
	y >>= HALF_BITS;
	y *= x;
	y >>= (2 * HALF_BITS);
	y *= x;
	y >>= (5 * HALF_BITS - BUCKET_BITS);

	assert(y < BUCKETS);
	return y;
}

////////////////////////////////////////////////////////////////////////////////

enum lock_mode {
	NOLOCK_MODE = 0,
	SYSCALL_MODE,
	FUTEX_MODE,
	LWSYNC_MODE,
};

static enum lock_mode lock_mode;
static int delay_mode;

static uint32_t round __aligned(64);

struct bucket {
	union {
		syscall_mutex_t sm;
		futex_mutex_t fm;
		lwsync_mutex_t lm;
	} lock;

	/*TIME_T*/ uint32_t time;
	uint32_t events;
} __aligned(64);

struct bucket buckets[BUCKETS];

static void distrib_init(enum lock_mode mode, int delay)
{
	round = 0;
	lock_mode = mode;
	delay_mode = delay;

	for (unsigned int i = 0; i < BUCKETS; i++) {
		struct bucket *b = &buckets[i];
		switch (lock_mode) {
		case NOLOCK_MODE:
			/* no locking */
			break;
		case SYSCALL_MODE:
			syscall_mutex_init_explicit(&b->lock.sm, i);
			break;
		case FUTEX_MODE:
			futex_mutex_init(&b->lock.fm);
			break;
		case LWSYNC_MODE:
			lwsync_mutex_init(&b->lock.lm);
			break;
		}
		b->time = 0;
		b->events = 0;
	}
}

static unsigned int distrib_coreloop(uint32_t offset)
{
	TIME_T before, after, delta;
	TIME_T start, now;
	uint32_t x;
	uint32_t y;
	struct bucket *b;
	unsigned int ops;
	bool_t success;

	ops = 0;
	while (TRUE) {
		x = atomic_fetch_add_relaxed(&round, 1);
		if (x >= MAX_NUM) {
			break;
		}

		uint32_t x_ = (x + offset) % MAX_NUM;
		y = DISTRIB_F(feistel(x_));
		b = &buckets[y];

		before = GETTS();

		switch (lock_mode) {
		case NOLOCK_MODE:
			/* no locking */
			break;
		case SYSCALL_MODE:
			success = syscall_mutex_timedlock(&b->lock.sm, INFINITE);
			assert(success == TRUE);
			break;
		case FUTEX_MODE:
			success = futex_mutex_timedlock(&b->lock.fm, INFINITE);
			assert(success == TRUE);
			break;
		case LWSYNC_MODE:
			success = lwsync_mutex_timedlock(&b->lock.lm, INFINITE);
			assert(success == TRUE);
			break;
		}
		(void)success;

		/* waste time time */
		start = GETTS();
		do {
			now = GETTS();
		} while ((int)(now - start) < delay_mode * CYCLES_PER_US);

		switch (lock_mode) {
		case NOLOCK_MODE:
			/* no locking */
			break;
		case SYSCALL_MODE:
			syscall_mutex_unlock(&b->lock.sm);
			break;
		case FUTEX_MODE:
			futex_mutex_unlock(&b->lock.fm);
			break;
		case LWSYNC_MODE:
			lwsync_mutex_unlock(&b->lock.lm);
			break;
		}

		after = GETTS();
		delta = after - before;
		atomic_add_relaxed(&b->time, delta);

		atomic_add_relaxed(&b->events, 1);
		ops++;
	}

	return ops;
}

static void distrib_print_results(const char *key)
{
	unsigned int all_events = 0;

	//printf("; cycles_per_us: %d\n", CYCLES_PER_US);
	//printf("; delay_mode: %d us\n", delay_mode);
	//printf("; lock_mode: %s\n", (lock_mode == NOLOCK_MODE) ? "NOLOCK" :
	//                            (lock_mode == SYSCALL_MODE) ? "SYSCALL" :
	//                            (lock_mode == FUTEX_MODE) ? "FUTEX" : "LWSYNC");

	for (unsigned int i = 0; i < BUCKETS; i++) {
		struct bucket *b = &buckets[i];
		lock_stat_t s;

		switch (lock_mode) {
		case NOLOCK_MODE:
			/* no locking */
			s.lu = b->events;
			s.lc = 0;
			s.lf = 0;
			s.uu = b->events;
			s.uc = 0;
			s.uf = 0;
			break;
		case SYSCALL_MODE:
			syscall_mutex_stat(&b->lock.sm, &s);
			break;
		case FUTEX_MODE:
			futex_mutex_stat(&b->lock.fm, &s);
			break;
		case LWSYNC_MODE:
			lwsync_mutex_stat(&b->lock.lm, &s);
			break;
		}

		all_events += b->events;
		printf("%s %4d %7d %10d   %7d %6d %6d   %7d %6d %6d\n",
		       key, i, b->events, b->time,
		       s.lu, s.lc, s.lf,
		       s.uu, s.uc, s.uf);
	}

	printf("; all events: %d, expect: %d\n", all_events, MAX_NUM);
	assert(all_events == MAX_NUM);
	(void)all_events;
}

////////////////////////////////////////////////////////////////////////////////

#define NUM_THREADS 4

static uint32_t thread_control;

static uint32_t thread_ops[NUM_THREADS];

static void distrib_thread(void *unused __unused)
{
	unsigned int slot;

	slot = sys_thread_self() - 1;
	assert(slot < NUM_THREADS);

	/* sync to main thread */
	(void)sys_futex_wait(&thread_control, NUM_THREADS, INFINITE);

	thread_ops[slot] = distrib_coreloop(slot * 4711);

	/* the last finishes wakes the main thread */
	uint32_t finisher = atomic_fetch_add_relaxed(&thread_control, 1);
	if (finisher == NUM_THREADS - 1) {
		(void)sys_futex_wake(&thread_control, 1);
	}
	sys_thread_exit();
}

static void distrib_run(unsigned int threads, enum lock_mode mode, int delay)
{
	uint32_t sum_ops;
	char key[4];

	distrib_init(mode, delay);

	key[0] = '0' + threads;
	if (mode == NOLOCK_MODE) {
		key[1] = 'n';
	} else if (mode == SYSCALL_MODE) {
		key[1] = 's';
	} else if (mode == FUTEX_MODE) {
		key[1] = 'f';
	} else if (mode == LWSYNC_MODE) {
		key[1] = 'l';
	} else {
		key[1] = '?';
	}
	if (delay == 0) {
		key[2] = '0';
	} else if (delay == 1) {
		key[2] = '1';
	} else if (delay == 10) {
		key[2] = '2';
	} else if (delay == 100) {
		key[2] = '3';
	} else {
		key[2] = '?';
	}
	key[3] = '\0';

	printf("; *** mode: threads %d, mode %s, delay %d\n",
	       threads,
	       (lock_mode == NOLOCK_MODE) ? "NOLOCK" :
	       (lock_mode == SYSCALL_MODE) ? "SYSCALL" :
	       (lock_mode == FUTEX_MODE) ? "FUTEX" : "LWSYNC",
	       delay);

	thread_control = NUM_THREADS;

	for (unsigned int t = 0; t < threads; t++) {
		thread_ops[t] = 0;
		create_thread_cpu(t + 1, distrib_thread, NULL, WORKER_THREAD_PRIO, t);
	}

	/* short break to let the other threads start properly */
	sys_sleep(sys_time_get() + 100*1000*1000);

	/* kick and wait for completion */
	thread_control = NUM_THREADS - threads;
	(void)sys_futex_wake(&thread_control, NUM_THREADS);

	(void)sys_futex_wait(&thread_control, NUM_THREADS - threads, INFINITE);
	assert(thread_control == NUM_THREADS);

	/* short break to let the other threads exit properly */
	sys_sleep(sys_time_get() + 100*1000*1000);

	distrib_print_results(key);

	/* dump distribution results */
	printf("; distrib:");
	sum_ops = 0;
	for (unsigned int t = 0; t < threads; t++) {
		sum_ops += thread_ops[t];
		printf(" %d", thread_ops[t]);
	}
	assert(sum_ops == MAX_NUM);
	printf("\n");
}

void bench_ecrts2020_distrib(void)
{
	prio_t orig_prio;

	/* init global state */
	max_prio = sys_prio_max();
	orig_prio = sys_prio_set(BENCH_THREAD_PRIO);
	assert(SELF->sys.next_prio <= BENCH_THREAD_PRIO);

	/* some tests require an SMP system */
	num_cpus = 0;
	for (unsigned long cpu = 0, cpu_mask = sys_cpu_mask();
			(cpu < sizeof(cpu_mask)*8) && ((cpu_mask & (1ul << cpu)) != 0);
	     cpu++) {
		num_cpus++;
	}
	assert(num_cpus > 0);
	assert(num_cpus <= 64);

	bench_header('#', "URAMAKI -- ECRTS 2020 PAPER -- START2");

	distrib_run(1, NOLOCK_MODE, 0);
	distrib_run(1, NOLOCK_MODE, 1);
	//distrib_run(1, NOLOCK_MODE, 10);
	//distrib_run(1, NOLOCK_MODE, 100);

	distrib_run(1, SYSCALL_MODE, 0);
	distrib_run(1, SYSCALL_MODE, 1);
	//distrib_run(1, SYSCALL_MODE, 10);
	//distrib_run(1, SYSCALL_MODE, 100);

	distrib_run(1, FUTEX_MODE, 0);
	distrib_run(1, FUTEX_MODE, 1);
	//distrib_run(1, FUTEX_MODE, 10);
	//distrib_run(1, FUTEX_MODE, 100);

	distrib_run(1, LWSYNC_MODE, 0);
	distrib_run(1, LWSYNC_MODE, 1);
	//distrib_run(1, LWSYNC_MODE, 10);
	//distrib_run(1, LWSYNC_MODE, 100);

	if (num_cpus >= NUM_THREADS) {
		distrib_run(NUM_THREADS, NOLOCK_MODE, 0);
		distrib_run(NUM_THREADS, NOLOCK_MODE, 1);
		//distrib_run(NUM_THREADS, NOLOCK_MODE, 10);
		//distrib_run(NUM_THREADS, NOLOCK_MODE, 100);

		distrib_run(NUM_THREADS, SYSCALL_MODE, 0);
		distrib_run(NUM_THREADS, SYSCALL_MODE, 1);
		//distrib_run(NUM_THREADS, SYSCALL_MODE, 10);
		//distrib_run(NUM_THREADS, SYSCALL_MODE, 100);

		distrib_run(NUM_THREADS, FUTEX_MODE, 0);
		distrib_run(NUM_THREADS, FUTEX_MODE, 1);
		//distrib_run(NUM_THREADS, FUTEX_MODE, 10);
		//distrib_run(NUM_THREADS, FUTEX_MODE, 100);

		distrib_run(NUM_THREADS, LWSYNC_MODE, 0);
		distrib_run(NUM_THREADS, LWSYNC_MODE, 1);
		//distrib_run(NUM_THREADS, LWSYNC_MODE, 10);
		//distrib_run(NUM_THREADS, LWSYNC_MODE, 100);
	}

	bench_header('#', "URAMAKI -- ECRTS 2020 PAPER -- END2");

	sys_prio_set(orig_prio);

#ifdef RUN_ONCE
	sys_sleep(TIMEOUT_INFINITE);
#endif
}
