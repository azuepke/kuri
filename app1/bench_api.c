/* SPDX-License-Identifier: MIT */
/*
 * bench_api.c
 *
 * CPU core benchmark
 *
 * azuepke, 2014-03-04: initial
 * azuepke, 2015-01-26: add low-level syscall benchmarks
 * azuepke, 2020-01-31: adapted for Marron
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <compiler.h>
#include "app.h"
#include "bench.h"
#include "assert.h"
#include <arch_tls.h>


void bench_api(void)
{
	TIME_T before, after;
	unsigned int runs;
	unsigned int prio;
	uint32_t futex;
	err_t err;

	printf("\n*** core API:\n");

	printf("- GETTS()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		GETTS();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_gettime()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_time_get();
	}
	after = GETTS();
	delta(before, after);


	printf("- null TLS access\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self();
	}
	after = GETTS();
	delta(before, after);


	printf("- null syscall\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self_syscall();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_yield()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_yield();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_preempt()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_preempt();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_futex_wait() compare fail\t\t\t\t");
	futex = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_futex_wait(&futex, 1, 0);
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_futex_wake(&futex, 0)\t\t\t\t");
	futex = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_futex_wake(&futex, 0);
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_lwsync_wait() compare fail\t\t\t");
	futex = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_lwsync_wait(&futex, 1, 0, 0);
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_lwsync_wake() state mismatch\t\t\t");
	prio = sys_prio_get();
	futex = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_lwsync_wake(0, &futex, 0, prio);
	}
	after = GETTS();
	delta(before, after);


	/* fast prio switching */
	printf("- prio fast: set/restore pair inlined\t\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		struct sys_tls *tls = (struct sys_tls *)__tls_base();
		/* NOTE: inlined version of fast priority switching */
		tls->user_prio = prio;
		barrier();
		tls->user_prio = prio - 1;
		if (tls->next_prio >= prio-1) {
			sys_preempt();
		}
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- prio fast: set/restore pair\t\t\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_prio_set(prio);
		sys_prio_set(prio-1);
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- prio fast: set/restore pair + syscall\t\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* NOTE: we hack next_prio in TLS to enforce a system call */
		sys_prio_set(prio+1);
		tls_set_1(offsetof(struct sys_tls, next_prio), 0xff);
		sys_prio_set(prio);
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- prio slow: set/restore pair with syscalls\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_prio_set_syscall(prio);
		sys_prio_set_syscall(prio-1);
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- sys_mutex_lock/unlock pair\t\t\t\t");
	err = sys_mutex_init(0, 0);
	assert((err == EOK) || (err == ESTATE));
	(void)err;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_mutex_lock(0, 0, 0);
		sys_mutex_unlock(0);
	}
	after = GETTS();
	delta(before, after);
}
