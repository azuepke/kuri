/* SPDX-License-Identifier: MIT */
/*
 * my_tls.h
 *
 * Application-specific TLS (for Uramaki Futexes)
 *
 * azuepke, 2020-02-02: initial
 */

#ifndef __MY_TLS_H__
#define __MY_TLS_H__

#include <marron/types.h>
#include <compiler.h>
#include <list.h>
#include <tls.h>

struct my_tls {
	/* system TLS */
	struct sys_tls sys;

	/* thread ID */
	uint32_t tid;
	/* user wait state */
	uint32_t ustate;

	/* wait queue information */
	list_t waitq_node;
	unsigned int waitq_prio;
} __aligned(64);

#define MY_TLS_INIT(_name) {	\
	.sys = SYS_TLS_INIT(_name, 0, 0xff, 0),	\
	}

#define MY_TLS_DEFINE(name)	\
	struct my_tls name = MY_TLS_INIT(name)

#define MY_TLS_FIXUP() do {	\
		SYS_TLS_FIXUP();	\
	} while (0)

#endif
