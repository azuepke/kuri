# README_RASPBERRY_PI3.TXT
#
# azuepke, 2021-01-22: initial

Raspberry Pi 2 Model B V1.2 and Raspberry Pi 3 Model B and B+
==============================================================

This text covers both the Raspberry Pi 2 Model B V1.2
and the Raspberry Pi 3 Model B and Model B+.
The first two have a Broadcom BCM2837 processor with a quad-core Cortex A53
and 256 KB L2 cache. The Raspberry Pi 2 clocks up to 900 MHz,
while the Raspberry Pi 3 B handles up to 1.2 GHz.
The Raspberry Pi 3 B+ uses a BCM2837B0 processor revision with up to 1.4 GHz.
However, the default ARM core clock is at 600 MHz, and that is what we will use.

The hardware follows the Raspberry Pi Model B V1.1 with the BCM2836 chip,
however, the UART settings are different.
But note that the Raspberry Pi 3 Model B and B+ use the 8250-style UART
at 0x3f215040 as primary UART and not the PL011 UART at 0x3f201000.

See README_RASPBERRY_PI2.TXT for technical details.


Firmware
=========

0. Install U-Boot host tools:

  $ sudo apt install device-tree-compiler u-boot-tools


1. Download the VideoCore bootloader:
  https://github.com/raspberrypi/firmware/tree/master/boot
  - bootcode.bin                Videocore bootloader
  - start.elf                   Videocore firmware
  - fixup.dat                   Fixups
  - bcm2710-rpi-3-b.dtb         DTB-Blob
  - bcm2710-rpi-3-b-plus.dtb    DTB-Blob

  NOTE: You should have a valid DTB-file as well.

2. Enable the UART in the bootcode.bin
    $ strings bootcode.bin | grep BOOT_UART
    BOOT_UART=0

    $ sed -i -e "s/BOOT_UART=0/BOOT_UART=1/" bootcode.bin

3. Create a config.txt (parsed by the bootloader):

  $ nano config.txt

    enable_uart=1
    arm_64bit=0

  Note that the last setting forces to load the 32-bit version of U-Boot,
  Set "arm_64bit" to 1 when 64-bit is needed. In this case, the bootloader
  will load "kernel8.img" (64-bit) instead of "kernel7.img" (32-bit).

4. Download and compile U-Boot:

  # We download version 2020.10 (works with GCC 9.3.0 from Ubuntu 20.04)

  $ git clone --depth 1 --branch v2020.10 git://git.denx.de/u-boot.git
  $ cd u-boot
  $ git checkout v2020.10

  The following depends if you have a Raspberry Pi 3 B or 3 B+ model.

  For the 3 B model, do the following:

  # Compile 32-bit version
  $ export CROSS_COMPILE=arm-linux-gnueabihf-
  $ make rpi_3_32b_defconfig
  $ make
  $ cp u-boot.bin ../sdcard/kernel7.img    # <--- adapt path to sdcard!
  $ make distclean

  # Compile 64-bit version (for 64-bit OS hacking)
  $ export CROSS_COMPILE=aarch64-linux-gnu-
  $ make rpi_3_defconfig
  $ make
  $ cp u-boot.bin ../sdcard/kernel8.img    # <--- adapt path to sdcard!
  $ make distclean

  For the 3 B+ model, do this:

  U-Boot does not provide a 32-bit build config for the 3 B+ model. Therefore,
  we first need to create a 32-bit build config from the existing 3 B config:
  $ sed -e "s/bcm2837-rpi-3-b/bcm2837-rpi-3-b-plus/" configs/rpi_3_32b_defconfig > configs/rpi_3_b_plus_32b_defconfig

  # Compile 32-bit version
  $ export CROSS_COMPILE=arm-linux-gnueabihf-
  $ make rpi_3_b_plus_32b_defconfig
  $ make
  $ cp u-boot.bin ../sdcard/kernel7.img    # <--- adapt path to sdcard!
  $ make distclean

  # Compile 64-bit version (for 64-bit OS hacking)
  $ export CROSS_COMPILE=aarch64-linux-gnu-
  $ make rpi_3_b_plus_defconfig
  $ make
  $ cp u-boot.bin ../sdcard/kernel8.img    # <--- adapt path to sdcard!
  $ make distclean


5. Create a U-boot config

  Create the file boot.cmd as follows:

  $ nano boot.cmd

    setenv ipaddr 192.168.1.15
    setenv serverip 192.168.1.2
    tftpboot 0x10000000 rpi3
    bootm

  Adapt the IP addresses and the file name according to your setup.

  Compile this into a U-boot boot script:

  $ mkimage -T script -C none -n 'Loader Script' -d boot.cmd boot.scr


6. Create a FAT32 partition on an SD card

  $ sudo fdisk /dev/sdX

   o       new DOS partition table
   n p 1   new primary partiton
   t c     type "C" for FAT32
   w

  $ sudo mkfs.fat -F 32 -n RPI3BOOT /dev/sdX1


8. Copy all files to the root directory of the FAT partition


9. Summary

  The RPi will now download a raw binary image named "rpi3" from a TFTP server.
  After compilation, prepare the boot image as follows:
  $ cp bootfile.img /tftpboot/rpi3

  Also, adapt config.txt and set "arm_64bit=1" when booting a 64-bit image!


Serial settings
================

https://www.raspberrypi.org/documentation/usage/gpio/

Note: these settings are the same as on RPi2, RPi3 and RPi4!

- make sure you can read the text on the PCB
- the pin header at the top (pins are named 2, 4, 6, 8, 10 in schematics)
- top row; pins from the left
- 1: +5V
- 2: +5V
- 3: GND       <--> GND
- 4: UART0 TX  <--> TX
- 5: UART0 RX  <--> RX

Make sure the UART adaptor uses 3.3V.

The serial setting is 115200, 8N1


Board Reset
============

- solder a pin header to the RST / PEN holes
- connect PEN to GND to reset (e.g. pin 39 on the pin header at the top)
