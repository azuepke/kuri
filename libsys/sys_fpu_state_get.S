/* SPDX-License-Identifier: MIT */
/* sys_fpu_state_get.S -- system call stub for sys_fpu_state_get() */
/* GENERATED BY scripts/generate_syscall_stubs.sh -- DO NOT EDIT */

#include <syscalls.h>
#include <arch_syscall.h>

SYSCALL_FUNC(IN0, sys_fpu_state_get, SYSCALL_FPU_STATE_GET)
