/* SPDX-License-Identifier: MIT */
/*
 * sys_thread_self.c
 *
 * System call library
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <arch_tls.h>
#include <compiler.h>


unsigned int sys_thread_self(void)
{
	return tls_get_4(offsetof(struct sys_tls, thr_id));
}
