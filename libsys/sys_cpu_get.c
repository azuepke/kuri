/* SPDX-License-Identifier: MIT */
/*
 * sys_cpu_get.c
 *
 * System call library
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <arch_tls.h>
#include <compiler.h>


unsigned int sys_cpu_get(void)
{
	return tls_get_1(offsetof(struct sys_tls, cpu_id));
}
