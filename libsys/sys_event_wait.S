/* SPDX-License-Identifier: MIT */
/* sys_event_wait.S -- system call stub for sys_event_wait() */
/* GENERATED BY scripts/generate_syscall_stubs.sh -- DO NOT EDIT */

#include <syscalls.h>
#include <arch_syscall.h>

SYSCALL_FUNC(IN4, sys_event_wait, SYSCALL_EVENT_WAIT)
