/* SPDX-License-Identifier: MIT */
/*
 * board.c
 *
 * Board initialization, ARM specific.
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2013-12-23: Beaglebone Black port
 * azuepke, 2020-01-31: ported to Marron
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <assert.h>
#include <arm_insn.h>
#include <arm_io.h>
#include <bsp.h>

/** BSP name string */
const char bsp_name[] = "am335x";

#ifdef SMP
volatile ulong_t bsp_cpu_online_mask = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == bsp_cpu_id());
	bsp_cpu_online_mask |= (1u << cpu);
}
#endif

// initialize BSP
__init void bsp_init(void)
{
	serial_init(115200);
	printf("Starting up ...\n");

	irq_init();
	dmtimer_init(1000);	/* HZ */
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb; wfi" : : : "memory");
}

/** AM335x specific reset */
static inline void am335x_reset(void)
{
	/* set RST_GLOBAL_COLD_SW bit in PRM_RSTCTRL */
	/* PRM_DEVICE is at 44e0'0f00 */
	writel((volatile void *)(PRM_BASE + 0x000), 0x2);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
	if (mode == BOARD_RESET) {
		am335x_reset();
	}

	__bsp_halt();
	unreachable();
}
