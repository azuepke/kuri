/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for Raspberry Pi 2 Model B V1.1 with Cortex A7.
 *
 * azuepke, 2020-08-05: Raspberry Pi port
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* 8250_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the Raspberry Pi 2 board */
#define BOARD_RAM_PHYS		0x00000000
#define BOARD_RAM_SIZE		0x40000000	/* RPi 2 has 1 GB RAM */
#define BOARD_RAM_VIRT		0x00000000

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#ifdef __aarch64__
#define LOAD_ADDR			0x00080000	/* physical kernel load address */
#else /* 32-bit ARM */
#define LOAD_ADDR			0x00008000	/* physical kernel load address */
#endif

/* linker setting */
#define LD_TEXT_BASE		LOAD_ADDR
#define LD_DATA_BASE		0x00100000

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE		0xff841000
#define GIC_PERCPU_BASE		0xff842000
#define GIC_NUM_IRQS	224
#define GIC_CPU_BIT(x)	(1u << (x))

/*
 * UARTs on Raspberry Pi 4:
 *
 * UART1: 0xfe215040  8250 Mini-UART
 */
#define X8250_UART_REGS		0xfe215040
#define X8250_UART_REGSIZE	4
#define X8250_UART_CLOCK	250000000
#define X8250_UART_KEEPBAUD	1	/* keep baudrate of bootloader */

/** BCM specific addresses and frequencies */
#define CORE_FREQ	900000000	/* 900 MHz */
#define BCM_FREQ	396000000	/* 250 MHz */

#define CPU_COUNTER_FREQ	19200000	/* 19.2 MHz */

#define GPU_IRQ_REGS	0xfe00b200	/* GPU interrupt controller */
#define ARM_IRQ_REGS	0xff800000	/* ARM interrupt router/controller */
#define WDOG_REGS		0xfe100000	/* Watchdog */

#endif
