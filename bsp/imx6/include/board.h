/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for NXP IMX6 Cortex A9.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored board.h
 * azuepke, 2016-04-14: ported from ZYNQ
 * awerner, 2020-01-30: imported from am57xx and ported to imx6
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* imx_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the i.MX6 SABRELITE board */
#define BOARD_RAM_PHYS		0x10000000
#define BOARD_RAM_SIZE		0x3f000000	/* 1 GB - 16 MB */
#define BOARD_RAM_VIRT		0x10000000

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR	0x10008000	/* physical kernel load address */

/* linker setting */
#define LD_TEXT_BASE		0x10008000
#define LD_DATA_BASE		0x10100000

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE			0x00a01000
#define GIC_PERCPU_BASE			0x00a00100
#define GIC_NUM_IRQS			256
#define GIC_CPU_BIT(x)			(1u << (x))
#define MPCORE_SCU_BASE_PHYS	0x00a00000
#define MPCORE_GTIMER_BASE		0x00a00200
#define MPCORE_PTIMER_BASE		0x00a00600

#define MPCORE_TIMER_CLOCK		396000000	/* 396 MHz */

/*
 * UARTs on the i.MX6:
 * UART1: 0x02020000
 * UART2: 0x021e8000
 */
#define IMX_UART_REGS		0x021e8000
#define IMX_UART_CLOCK		80000000

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
