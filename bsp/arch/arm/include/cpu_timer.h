/* SPDX-License-Identifier: MIT */
/*
 * cpu_timer.h
 *
 * ARM AARCH64 generic per-CPU virtual timer
 *
 * azuepke, 2015-07-24: initial
 * azuepke, 2018-03-05: imported
 */

#ifndef __CPU_TIMER_H__
#define __CPU_TIMER_H__

void cpu_timer_init(unsigned int freq);
void cpu_timer_start(void);

#endif
