/* SPDX-License-Identifier: MIT */
/*
 * start64.S
 *
 * Assembler startup code for AARCH64
 *
 * azuepke, 2015-07-14: initial, from 32-bit ARM startup code
 * azuepke, 2020-11-23: SMP support
 * azuepke, 2020-11-24: 64-bit HV mini stub
 * azuepke, 2021-01-16: ported to mini-OS
 */

#include <board.h>
#include <arm_cr.h>

/* get number of configured CPUs */
#ifdef FINAL_RUN
#include "final.ld.h"
#else
#include "dummy.ld.h"
#endif


	.global _start
#ifdef QEMU_SMP_STARTUP
	.global __boot_release
#endif
	.global __boot_args
	.global __bsp_halt
#ifdef SMP
	.global bsp_cpu_id
#endif


.macro pcrel, reg, sym
	adrp	\reg, \sym
	add		\reg, \reg, :lo12:\sym
.endm


	.text
	.section .text.start, "ax"

	/* entry point from boot loader */
	/* NOTE: the boot loader either calls us in EL1 or EL2 mode! */
_start:
	bl		_real_start

_real_start:
	/*
	 * save boot args in x20..23 and keep pointer to _start in x24
	 */
	mov		x20, x0
	mov		x21, x1
	mov		x22, x2
	mov		x23, x3
	sub		x24, x30, #4

	/* detect hypervisor mode */
	/* x25 defines the boot mode, whether EL1, EL2, or EL3 */
	mrs		x1, CURRENTEL
	and		x25, x1, #0x3 << 2
	cmp		x25, #1 << 2
	b.eq	_el1_mode_setup
	cmp		x25, #2 << 2
	b.eq	_el2_mode_setup

_el3_mode_setup:
	/* minimal EL3 setup code */

	msr		VBAR_EL3, xzr			/* invalid vector table */

	ldr		w27, =0x30c50830		/* RES1 bits */
	msr		SCTLR_EL3, x27			/* clear SCTLR_EL3 */

	mov		x27, #0x00000030		/* RES1 bits */
	orr		x27, x27, #1<<10		/* set SCR.RW (EL2 is AARCH64) */
	orr		x27, x27, #1<<8			/* set SCR.HCE (enable EVC) */
	orr		x27, x27, #1<<7			/* set SCR.SMD (disable SMC) */
	orr		x27, x27, #1<<0			/* set SCR.NS (not secure) */
	msr		SCR_EL3, x27

	msr		CPTR_EL3, xzr			/* clear CPTR_EL3 */
	msr		MDCR_EL3, xzr			/* clear MDCR_EL3 */

	msr		TTBR0_EL3, xzr			/* clear translation tables */

	/* we're in EL3, switch to EL2 */
	mov		x26, #(CPSR_D | CPSR_A | CPSR_I | CPSR_F | CPSR_MODE_EL2h)
	msr		SPSR_EL3, x26
	adr		x27, _el2_mode_setup
	msr		ELR_EL3, x27
	eret

	.ltorg

_el2_mode_setup:
	/* minimal hypervisor mode setup */

	msr		VBAR_EL2, xzr			/* invalid vector table */

	ldr		w27, =0x30c50830		/* RES1 bits */
	msr		SCTLR_EL2, x27			/* clear SCTLR_EL2 */

	mov		x27, #0x80000000		/* set HCR.RW (EL1 is AARCH64) */
	msr		HCR_EL2, x27

	ldr		w27, =0x000033ff		/* RES1 bits */
	msr		CPTR_EL2, x27			/* clear HCPTR */
	msr		HSTR_EL2, xzr			/* clear HSTR */

	mrs		x26, MDCR_EL2			/* get MDCR_EL2 */
	and		x26, x26, #0x1f			/* keep HPMN bits */
	msr		MDCR_EL2, x26			/* set MDCR_EL2 */

	msr		VTTBR_EL2, xzr			/* clear translation tables */

	msr		CNTVOFF_EL2, xzr		/* clear virtual timer offset */

	/* set VPIDR = MIDR */
	mrs		x26, MIDR_EL1			/* get MIDR */
	msr		VPIDR_EL2, x26			/* set VPIDR */

	/* set VMPIDR = MPIDR */
	mrs		x27, MPIDR_EL1			/* get MPIDR */
	msr		VMPIDR_EL2, x27			/* set VMPIDR */

#if 0
	/* setup non-secure SCTLR */
	mrs		x26, SCTLR_EL1			/* get SCTLR_EL1 */
	orr		x26, #SCTLR_CP15BEN
	bic		x26, #SCTLR_ITD
	bic		x26, #SCTLR_SED
	bic		x26, #SCTLR_WXN
	bic		x26, #SCTLR_UWXN
	msr		SCTLR_EL1, x26			/* set SCTLR_EL1 */
#endif

	/* we're in EL2, switch to EL1 */
	mov		x26, #(CPSR_D | CPSR_A | CPSR_I | CPSR_F | CPSR_MODE_EL1h)
	msr		SPSR_EL2, x26
	adr		x27, _el1_mode_setup
	msr		ELR_EL2, x27
	eret

	.ltorg

_el1_mode_setup:
	/* now in EL1 mode */
	/* select handler stack mode (if not already in handler mode) */
	msr		SPSel, #1

#ifdef SMP
_smp_check:
	/* get SMP CPU ID */
	bl		bsp_cpu_id
	cmp		x0, #0
	b.eq	_boot_cpu0

#ifdef QEMU_SMP_STARTUP
	/* NOTE: QEMU starts all cores directly at boot, so we keep the other CPUs
	 * in a spinlock until they shall be released.
	 * See bsp_cpu_start_secondary() counterpart for protocol
	 */
	pcrel	x6, __boot_release
1:	ldr		w5, [x6]
	cmp		w5, w0
	b.eq	_common_start
	wfe
	b		1b

	/* keep __boot_release in .data section (.bss is cleared by CPU 0) */
	.pushsection .data
	.balign 4
__boot_release:
	.word	0x0
	.popsection
#else
	b		_common_start
#endif
#endif

_boot_cpu0:

	/*
	 * clear .bss
	 */
_clear_bss:
	pcrel	x0, __bss_start
	pcrel	x1, __bss_end

1:	stp		xzr, xzr, [x0], #16
	cmp		x0, x1
	b.cc	1b

	/*
	 * save boot_args in .bss section
	 */
_save_boot_args:
	pcrel	x0, __boot_args
	stp		x20, x21, [x0, #0]
	stp		x22, x23, [x0, #16]

	.pushsection .bss
	.balign 8
__boot_args:
	.quad	0, 0, 0, 0
	.popsection

_common_start:
	/* NOTE: this sequence deliberately lacks ISBs and DSBs!
	 * These are required at the time we finally go virtual!
	 */

	/* get pointers to kernel page tables */
	/* TTBR0 */
	pcrel	x0, adspace_cfg
	ldr		x0, [x0]
	/* TTBR1 -- disabled */
	mov		x1, #0
	/* TCR: 3 level page tables, 4K page size, 48 bit physical addresses */
	ldr		x2, =(0x1000000000 | (5<<32) | 0x00800000 | (3<<12) | (1<<10) | (1<<8)) | (25<<0)
	ldr		x3, =(MAIR1_DEFAULT << 32 | MAIR0_DEFAULT)
	msr		TTBR0_EL1, x0
	msr		TTBR1_EL1, x1
	msr		TCR_EL1, x2
	msr		MAIR_EL1, x3

	/* invalidate instruction cache and unified TLB */
	ic		IALLU
	tlbi	VMALLE1
	dsb		sy
	isb		sy

	/* invalidate branch prediction */
	/* NOTE: not possible in 32-bit mode */

	/* invalidate data cache */
#if 0
	/* ARMv7 requires complex code to invalidate the data cache */
	/*
	* based on ARM example code
	* ARM ARM DDI 0406A, "Example code for cache maintenance operations"
	* clobbers r0 .. r5
	*/
	mov		r0, #0					@ select cache level 0
	mcr		p15, 2, r0, c0, c0, 0	@ write the Cache Size selection register
	isb								@ ISB to sync the change to the CacheSizeID reg
	mrc		p15, 1, r0, c0, c0, 0	@ reads current Cache Size ID register
	and		r2, r0, #0x7			@ extract the line length field
	add		r2, r2, #4				@ add 4 for the line length offset (log2 16 bytes)
	ldr		r4, =0x3ff
	ands	r4, r4, r0, lsr #3		@ r4 is the max number on the way size (right aligned)
	clz		r5, r4					@ r5 is the bit position of the way size increment
	ldr		r1, =0x7fff
	ands	r1, r1, r0, lsr #13		@ r1 is the max number of the index size (right aligned)
1:
	mov		r3, r4					@ r3 working copy of the max way size (right aligned)
2:
	mov		r0, r3, lsl r5			@ factor in the way number into r0 (for cache number #0)
	orr		r0, r0, r1, lsl r2		@ factor in the index number
	mcr		p15, 0, r0, c7, c6, 2	@ invalidate by set/way
	subs	r3, r3, #1				@ decrement the way number
	bge		2b
	subs	r1, r1, #1				@ decrement the index
	bge		1b
#endif

	/* enable MMU and caches */
	mrs		x0, SCTLR_EL1
	ldr		x1, =SCTLR_CLR_64BIT
	bic		x0, x0, x1
	ldr		x1, =SCTLR_SET_64BIT
	orr		x0, x0, x1

	/* NOTE: for testing: keep MMU and caches disabled! */
	//bic		x0, x0, #SCTLR_M
	//bic		x0, x0, #SCTLR_C
	//bic		x0, x0, #SCTLR_I

	/* magic moment! */
	isb		sy
	dsb		sy

	/* update SCTLR */
	msr		SCTLR_EL1, x0
	isb		sy
	dsb		sy

	/* go virtual */
	ldr		x30, =_virtual
	br		x30
_virtual:

	/* init and enable FPU */
	/* allow only kernel to access FPU registers */
	mov		x0, CPACR_FPEN_K
	msr		CPACR_EL1, x0

	/* finally ... */
	isb		sy
	dsb		sy

	/* call arch_entry(kernel_stack, fiq_stack) */
	ldr		x5, =_stacks
#ifdef SMP
	/* get SMP CPU ID */
	bl		bsp_cpu_id
	lsl		x4, x0, #5
	add		x5, x5, x4
#endif
	ldr		x0, [x5, #0]
	ldr		x1, [x5, #8]
	ldr		x2, [x5, #16]
	b		arch_entry
	/* does not return */

	/* table with top of kernel stack and FIQ stack per CPU */
	.balign 8
_stacks:
#ifndef CFG_THREADS
#error need CFG_THREADS
#endif
#if CFG_THREADS >= 1
	.quad	kern_stack_cpu0_top
	.quad	fiq_stack_cpu0_top
	.quad	serror_stack_cpu0_top
	.quad	0
#endif
#if CFG_THREADS >= 2
	.quad	kern_stack_cpu1_top
	.quad	fiq_stack_cpu1_top
	.quad	serror_stack_cpu1_top
	.quad	0
#endif
#if CFG_THREADS >= 3
	.quad	kern_stack_cpu2_top
	.quad	fiq_stack_cpu2_top
	.quad	serror_stack_cpu2_top
	.quad	0
#endif
#if CFG_THREADS >= 4
	.quad	kern_stack_cpu3_top
	.quad	fiq_stack_cpu3_top
	.quad	serror_stack_cpu3_top
	.quad	0
#endif
#if CFG_THREADS >= 5
	.quad	kern_stack_cpu4_top
	.quad	fiq_stack_cpu4_top
	.quad	serror_stack_cpu4_top
	.quad	0
#endif
#if CFG_THREADS >= 6
	.quad	kern_stack_cpu5_top
	.quad	fiq_stack_cpu5_top
	.quad	serror_stack_cpu5_top
	.quad	0
#endif
#if CFG_THREADS >= 7
	.quad	kern_stack_cpu6_top
	.quad	fiq_stack_cpu6_top
	.quad	serror_stack_cpu6_top
	.quad	0
#endif
#if CFG_THREADS >= 8
	.quad	kern_stack_cpu7_top
	.quad	fiq_stack_cpu7_top
	.quad	serror_stack_cpu7_top
	.quad	0
#endif

	.ltorg

	.section .text, "ax"
	.type __bsp_halt, %function
__bsp_halt:
	msr		DAIFSET, #0x7
1:	dsb		sy
	wfi
	b		1b

	.ltorg
	.size __bsp_halt, . - __bsp_halt


#ifdef SMP
/* determine current CPU ID
 *
 * Prototype: unsigned int bsp_cpu_id(void);
 *
 * NOTE: This function is coded in assembler, as it is needed
 *       in the early boot phase and it must not access any stack.
 */
	.section .text, "ax"
	.type bsp_cpu_id, %function
bsp_cpu_id:
	/* get SMP CPU ID */
	mrs		x1, MPIDR_EL1
#if defined(ARM_MPIDR_AFF0_BITS)
	and		x0, x1, #(1 << ARM_MPIDR_AFF0_BITS) - 1
#else
	and		x0, x1, #0xf
#endif
#if defined(ARM_MPIDR_AFF1_BITS) && (ARM_MPIDR_AFF1_BITS > 0)
	lsr		x2, x1, #8
	and		x2, x2, #(1 << ARM_MPIDR_AFF1_BITS) - 1
	lsl		x2, x2, #ARM_MPIDR_AFF0_BITS
	orr		x0, x0, x2
#endif
#if defined(ARM_MPIDR_AFF2_BITS) && (ARM_MPIDR_AFF2_BITS > 0)
	lsr		x2, x1, #16
	and		x2, x2, #(1 << ARM_MPIDR_AFF2_BITS) - 1
	lsl		x2, x2, #ARM_MPIDR_AFF0_BITS + ARM_MPIDR_AFF1_BITS
	orr		x0, x0, x2
#endif
#if defined(ARM_MPIDR_AFF3_BITS) && (ARM_MPIDR_AFF3_BITS > 0)
	lsr		x2, x1, #32
	and		x2, x2, #(1 << ARM_MPIDR_AFF3_BITS) - 1
	lsl		x2, x2, #ARM_MPIDR_AFF0_BITS + ARM_MPIDR_AFF1_BITS + ARM_MPIDR_AFF2_BITS
	orr		x0, x0, x2
#endif
#if defined(ARM_MPIDR_REMAP_TABLE)
	adr		x2, _remap_table
	ldrb	w0, [x2, x0]
#endif
	ret

#if defined(ARM_MPIDR_REMAP_TABLE)
_remap_table:
	.byte	ARM_MPIDR_REMAP_TABLE
#endif

	.ltorg
	.size bsp_cpu_id, . - bsp_cpu_id
#endif
