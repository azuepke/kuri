#ifndef OMAP_SMC_
#define OMAP_SMC_

#define OMAP5_DRA7_MON_SET_CNTFRQ_INDEX 0x109
#define OMAP5_MON_AMBA_IF_INDEX         0x108
#define OMAP5_DRA7_MON_SET_ACR_INDEX    0x107

void omap_smc1(uint32_t fn, uint32_t arg);
#endif
