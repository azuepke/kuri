/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for QEMU ARM "virt" board with Cortex-A57.
 *
 * azuepke, 2015-07-13: initial
 * azuepke, 2021-01-16: adapted to mini-OS
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the QEMU "virt" board,
 * see qemu/hw/arm/virt.c in QEMU source:
 *
 * GIC dist: 0x08000000
 * GIC CPU:  0x08010000
 * GIC V2M:  0x08020000
 * UART:     0x09000000
 */
#define BOARD_RAM_PHYS		0x40000000
#define BOARD_RAM_SIZE		0x04000000	/* 64 MB */
#define BOARD_RAM_VIRT		0x40000000

/* 1st IO region, peripherals, e.g. serial, timer */
#define BOARD_IO1_PHYS		0x09000000
#define BOARD_IO1_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO1_VIRT		0x09000000

/* 3rd IO region, MPCore region */
#define BOARD_IO3_PHYS		0x08000000
#define BOARD_IO3_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO3_VIRT		0x08000000

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			0x40080000	/* physical kernel load address */

/* linker setting */
#define LD_TEXT_BASE		0x40080000
#define LD_DATA_BASE		0x40100000

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE			(BOARD_IO3_VIRT + 0x00000)
#define GIC_PERCPU_BASE			(BOARD_IO3_VIRT + 0x10000)
#define GIC_NUM_IRQS	288
#define GIC_CPU_BIT(x)	(1u << (x))

/*
 * The PL011 UART on QEMU ARM "virt" board is located
 * at physical address 0x09000000.
 * See also qemu/hw/arm/virt.c in QEMU sources.
 */
#define PL011_SERIAL_REGS	(BOARD_IO1_VIRT + 0x000000)
#define PL011_SERIAL_CLOCK	24000000

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

/** QEMU implements PSCI calls via HVC instead of SMC when EL2 */
/* enable EL2 mode in QEMU: $ qemu-system-aarch64 -M virt,virtualization=on */
/* #define QEMU_PSCI_CALL 1 */

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
