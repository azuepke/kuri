/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for Xilinx Zynq UltraScale+ with Cortex-A53.
 *
 * azuepke, 2021-01-22: adapted from QEMU
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* zynq_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		0x00000000
#define BOARD_RAM_SIZE		0x04000000	/* 64 MB */
#define BOARD_RAM_VIRT		0x00000000

/* 1st IO region, peripherals, e.g. serial, timer */
#define BOARD_IO1_PHYS		0xff000000
#define BOARD_IO1_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO1_VIRT		0xff000000

/* 3rd IO region, MPCore region */
#define BOARD_IO3_PHYS		0xf9000000
#define BOARD_IO3_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO3_VIRT		0xf9000000

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			0x00080000	/* physical kernel load address */

/* linker setting */
#define LD_TEXT_BASE		0x00080000
#define LD_DATA_BASE		0x00100000

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE		(BOARD_IO3_VIRT + 0x10000)
#define GIC_PERCPU_BASE		(BOARD_IO3_VIRT + 0x20000)
#define GIC_NUM_IRQS	192
#define GIC_CPU_BIT(x)	(1u << (x))

/*
 * The Ultrascale has two UARTs:
 * UART1: 0xff000000
 * UART2: 0xff010000
 */
#define ZYNQ_UART_REGS		0xff000000
#define ZYNQ_UART_CLOCK		100000000

#endif
