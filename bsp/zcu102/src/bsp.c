/* SPDX-License-Identifier: MIT */
/*
 * bsp.c
 *
 * Board specific code for Xilinx Zynq UltraScale+ with Cortex-A53.
 *
 * azuepke, 2015-07-13: cloned from QEMU-ARM
 * azuepke, 2021-01-16: adapted to mini-OS
 * azuepke, 2021-01-22: adapted from QEMU
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <mpcore.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <panic.h>
#include <psci.h>


/** BSP name string */
const char bsp_name[] = "zcu102";

#ifdef SMP
// CPUs online
volatile ulong_t bsp_cpu_online_mask = 0;
// CPUs started scheduling
volatile int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned long mpidr;
	unsigned long entry;
	unsigned int cpu;
	int psci_err;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
#ifdef QEMU_SMP_STARTUP
		__boot_release = cpu;
		arm_dmb();
		arm_clean_dcache(&__boot_release);
		arm_dsb();
		arm_sev();
#endif

		/* for QEMU, the MPIDR of the target CPU is just the CPU ID */
		mpidr = cpu;
		entry = BOARD_KERNEL_TO_PHYS((unsigned long)_start);
		psci_err = psci_cpu_on(mpidr, entry, mpidr);
		if (psci_err != PSCI_ERR_SUCCESS) {
			panic("PSCI: problem starting CPU %d, error:%d\n", cpu, psci_err);
		}

		/* wait until CPU is up */
		while (!(bsp_cpu_online_mask & (1u << cpu))) {
			arm_wfe();
		}
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == bsp_cpu_id());
	bsp_cpu_online_mask |= (1u << cpu);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu == 0) {
		bsp_cpu_go = 1;
		arm_dsb();
		arm_sev();
	} else {
		arm_dsb();
		arm_sev();
		/* wait until we see the "go" signal */
		while (bsp_cpu_go == 0) {
			arm_wfe();
		}
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(void)
{
	unsigned int version;
	unsigned int cpu_id;

#ifdef SMP
	cpu_id = bsp_cpu_id();
#else
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printf("Starting up ...\n");

#ifdef SMP
		mpcore_init_smp();
#endif

		version = psci_version();
		printf("PSCI version: %d.%d\n", version >> 16, version & 0xffff);

		mpcore_irq_init();
		mpcore_gic_enable();
		cpu_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		mpcore_gic_enable();
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb sy; wfi" : : : "memory");
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		mpcore_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		psci_system_reset();
	}
	if (mode == BOARD_POWEROFF) {
		psci_system_off();
	}

	__bsp_halt();
	unreachable();
}
