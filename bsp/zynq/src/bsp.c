/* SPDX-License-Identifier: MIT */
/*
 * board.c
 *
 * Board initialization for NXP IMX6 Cortex A9.
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2021-10-06: adapted to Marron
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <mpcore.h>
#include <assert.h>
#include <mpcore_timer.h>
#include <arm_insn.h>
#include <arm_io.h>


/** BSP name string */
const char bsp_name[] = "zynq";

#ifdef SMP
// CPUs online
volatile ulong_t bsp_cpu_online_mask = 0;
// CPUs started scheduling
volatile int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	/* section "6.1.10 Starting Code on CPU 1" in ug585 Zynq 7000 manual:
	 * - CPU 1 kept in WFE
	 * - after receiving SEV:
	 *   - read new PC from 0xfffffff0
	 *   - jumps to new PC
	 */
	volatile uint32_t *vector = (volatile uint32_t *)(0xfffffff0);

	unsigned int cpu;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	*vector = BOARD_KERNEL_TO_PHYS((unsigned long)_start);

	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
		arm_dsb();
		arm_sev();

		/* wait until CPU is up */
		while (!(bsp_cpu_online_mask & (1u << cpu))) {
			arm_wfe();
		}
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == bsp_cpu_id());
	bsp_cpu_online_mask |= (1u << cpu);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu == 0) {
		bsp_cpu_go = 1;
		arm_dsb();
		arm_sev();
	} else {
		arm_dsb();
		arm_sev();
		/* wait until we see the "go" signal */
		while (bsp_cpu_go == 0) {
			arm_wfe();
		}
	}

	/* start timer here on SMP */
	mpcore_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(void)
{
	unsigned int cpu_id;

#ifdef SMP
	cpu_id = bsp_cpu_id();
#else
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printf("Starting up ...\n");

#ifdef SMP
		mpcore_init_smp();
#endif

		mpcore_irq_init();
		mpcore_gic_enable();
		mpcore_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		mpcore_timer_start();
#endif
	}
#ifdef SMP
	else {
		printf("secondary CPU %d came up\n", cpu_id);
		mpcore_gic_enable();
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	/* use WFE instead of WFI to keep the cycle counter running */
	__asm__ volatile ("dsb; wfe" : : : "memory");
}

/** ZYNQ specific reset */
static inline void zynq_reset(void)
{
	/* the SLCR is mapped at 0xf8000000 */

	/* unlock SLCR region */
	writel((volatile void *)(0xf8000000 + 0x8), 0xDF0D);

	/* reset */
	writel((volatile void *)(0xf8000000 + 0x200), 1);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		mpcore_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		zynq_reset();
	}

	__bsp_halt();
	unreachable();
}
