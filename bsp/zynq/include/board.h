/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for ARM ZYNQ with Cortex A9.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored board.h
 * azuepke, 2021-10-06: adapted to Marron
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* zynq_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the ZYNQ board */
#define BOARD_RAM_PHYS		0x00000000
#define BOARD_RAM_SIZE		0x40000000	/* 1 GB */
#define BOARD_RAM_VIRT		0x00000000

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR	0x00108000	/* physical kernel load address */

/* linker setting */
#define LD_TEXT_BASE		0x00108000
#define LD_DATA_BASE		0x00200000

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE			0xf8f01000
#define GIC_PERCPU_BASE			0xf8f00100
#define GIC_NUM_IRQS			256
#define GIC_CPU_BIT(x)			(1u << (x))
#define MPCORE_SCU_BASE_PHYS	0xf8f00000
#define MPCORE_GTIMER_BASE		0xf8f00200
#define MPCORE_PTIMER_BASE		0xf8f00600

#if 0
#define MPCORE_TIMER_CLOCK		100000000	/* 100 MHz @ QEMU */
#endif
#define MPCORE_TIMER_CLOCK		333333333	/* 333 MHz @ Zybo Z7 (half CPU freq) */

/*
 * UARTs on the Zynq 7000:
 * UART1: 0xe0000000
 * UART2: 0xe0001000
 */
#define ZYNQ_UART_REGS		0xe0001000
#define ZYNQ_UART_CLOCK		100000000

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
