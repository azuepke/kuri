# Kuri Kernel

This is a legacy version of the Marron kernel.
For the latest version, see the [Marron](https://gitlab.com/azuepke/marron/) repository.

## What is Kuri

"Kuri" is a small, statically configured microkernel targeting MMU
based systems. The current implementation focuses on MMU-based 32-bit ARM
Cortex-A multicore processors, e.g. Cortex A8, A9 or A15.
Recently, Kuri gained support for 64-bit ARM processors as well,
e.g. Cortex A53, A57 and A72.

The Kuri kernel is a fork of the [legacy Marron kernel](https://gitlab.com/azuepke/marron_legacy/) revision 372:bb5eb1fa682a.
The Marron kernel is a C language baseline for operating systems research by
Alex Zuepke of RheinMain University of Applied Sciences https://www.cs.hs-rm.de/
to explore SPARK Ada for teaching and research in the context of the AQUAS
(Aggregated Quality Assurance for Systems) project https://aquas-project.eu/.

Kuri (栗) is the Japanese word for chestnut. The chestnuts of Japanese chestnut
trees (castanea crenata) differ from European chestnuts (castanea sativa):
the fruits are bigger and more watery, have a milder taste with yellow flesh.
In this sense, Kuri is an experimental platform to explore different
synchronization mechanisms for Alex' research and PhD thesis.

This version was under active development in 2020 and beginning of 2021.

Kuri is licensed under MIT license, see [LICENSE.TXT](LICENSE.TXT).

See the original [README.TXT](README.TXT) for more information.
