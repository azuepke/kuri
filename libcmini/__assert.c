/* SPDX-License-Identifier: MIT */
/*
 * __assert.c
 *
 * Internal __assert()
 *
 * azuepke, 2018-01-04: imported
 */

#include <assert.h>
#include <stdio.h>
#include <marron/api.h>

/** user space assertion */
void __assert_fail(const char *cond, const char *file, int line, const char *func);
void __assert_fail(const char *cond, const char *file, int line, const char *func)
{
	printf("%s:%d: %s: assertion '%s' failed.\n", file, line, func, cond);

	sys_abort();
}
