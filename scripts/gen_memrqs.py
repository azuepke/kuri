#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
#
# gen_memrqs.py -- Generate memrqs.xml from XML config and dummy ELF files
#
# azuepke, 2017-09-22: initial
# azuepke, 2017-10-06: header generator
# azuepke, 2017-10-19: forked from header generator
# azuepke, 2017-10-20: first working version
# azuepke, 2017-10-23: new data model
# azuepke, 2017-10-26: factored out common code library and merged generators
#
#
# What does this tool do?
#
# This tool generates memrqs.xml from the XML config and dummy ELF files
# - it parses both hardware.xml and config.xml to locate all pieces of memory
# - it scans the dummy ELF files and extracts the size of the requested regions
# - finally, it dumps all memory requirements for all partitions as memrqs.xml
#
# example for dummy run:
# $ ./gen_memrqs.py -hw hardware.xml -c config.xml memrqs.xml

import gen_core as gen
import argparse


# --- main ---

# main
def main():
    prog = 'gen_memrqs.py'
    gen.set_progname(prog)

    parser = argparse.ArgumentParser(prog=prog,
                                     description='Generate memrqs.xml from XML config and dummy ELF files.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    parser.add_argument('-hw', metavar='hardware.xml', nargs=1, type=str,
                        help='hardware description (XML)')
    parser.add_argument('-c', metavar='config.xml', nargs=1, type=str,
                        help='configuration (XML)')
    parser.add_argument('-nm', metavar='nm tool', nargs=1, type=str,
                        help='name mangling tool to use, e.g. arm-linux-gnueabihf-nm')
    parser.add_argument('m', metavar='memrqs.xml', nargs=1, type=str,
                        help='memory requirements (XML)')

    args = parser.parse_args()
    gen.set_verbose(args.verbose)

    # default argument for nm
    if args.nm is not None:
        gen.set_nm_tool(args.nm[0])

    # parse hardware.xml
    if args.hw is None:
        gen.die("need hardware.xml")
    gen.parse_hardware_xml(args.hw[0])

    # parse config.xml
    if args.c is None:
        gen.die("need config.xml")
    gen.parse_config_xml(args.c[0])

    # resolve addresses
    gen.Partition.resolve_all(False)

    gen.gen_memrqs(args.m[0])


if __name__ == '__main__':
    main()
