/* SPDX-License-Identifier: MIT */
/*
 * app.ld.S
 *
 * Application linker script
 *
 * azuepke, 2017-11-02: initial, from BSP linker script
 */

#ifdef FINAL_RUN
#include "final.ld.h"
#else
#include "dummy.ld.h"
#endif

#ifdef __aarch64__
OUTPUT_FORMAT("elf64-littleaarch64")
OUTPUT_ARCH("aarch64")
#else
OUTPUT_FORMAT("elf32-littlearm")
OUTPUT_ARCH("arm")
#endif
ENTRY(_start)

SECTIONS
{
	.text (CFG_MEMRQ_ADDR_text) : {
		__text_start = .;
		*(.text.start)
		*(.text.init)
		*(.text.cold)
		*(.text)
		*(.text.*)
	}

	.rodata : {
		*(.rodata)
		*(.rodata.*)
		*(.rodata1)
		*(.sdata2)
		*(.sbss2)
		__text_end = .;
	}

	/* .data is kept behind .text and .rodata in the flash section */
	__data_origin = ALIGN(16);

	.data (CFG_MEMRQ_ADDR_data) : AT(__data_origin) {
		__data_start = .;
		*(.data)
		*(.data.*)
		*(.data1)
		*(.sdata)
		__data_end = .;
		__text_real_end = ABSOLUTE(__data_origin) + (__data_end - __data_start);
	}
	.bss ALIGN(16) : {
		__bss_start = .;
		*(.sbss)
		*(.scommon)

		*(.bss)
		*(.bss.*)
		*(COMMON)
		__bss_end = .;
		/* the heap is placed after .bss */
		__heap_start = ALIGN(16);
		. += CFG_HEAP_SIZE;
		__heap_end = .;
	}
	.shm1 (CFG_SHM_ADDR_shm1) (NOLOAD) : {
		_shm1_start = .;
		. += CFG_SHM_SIZE_shm1;
		_shm1_end = .;
	}
	.shm2 (CFG_SHM_ADDR_shm2) (NOLOAD) : {
		_shm2_start = .;
		. += CFG_SHM_SIZE_shm2;
		_shm2_end = .;
	}

	/DISCARD/ : {
		*(.eh_frame)
		*(.ARM.exidx)
	}
}
