# LICENSE.TXT
#
# azuepke, 2019-12-18: initial
# azuepke, 2021-01-27: clarify components

As of December 18th, 2019, the kernels of Marron and Kuri, the BSPs, libcmini,
libsys, the demo applications app1 and app2, the documentation, related
scripts and tools are released as licensed by the MIT license as shown below.

Marron Copyright (c) 2017-2021 Alexander Züpke
Kuri Copyright (c) 2020-2021 Alexander Züpke
Some portions of this code Copyright (c) 2013-2021 Alexander Züpke

################################################################################

From: https://spdx.org/licenses/MIT.html

MIT License

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next
paragraph) shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
