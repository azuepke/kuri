/* SPDX-License-Identifier: MIT */
/*
 * exception64.c
 *
 * Architecture specific exception handling
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2021-01-16: port to 64-bit ARM
 */

#include <arch.h>
#include <panic.h>
#include <kernel.h>
#include <bsp.h>
#include <assert.h>
#include <stdio.h>
#include <arm_private.h>
#include <arm_insn.h>
#include <arm_cr.h>
#include <arch_context.h>
#include <user.h>
#include <arch_adspace_types.h>
#include <arm_perf.h>


__cold void arch_dump_registers(struct regs *regs)
{
	assert(regs != NULL);

	printf("x0-3        %lx %lx %lx %lx\n",
	       regs->r[0], regs->r[1], regs->r[2], regs->r[3]);
	printf("x4-7        %lx %lx %lx %lx\n",
	       regs->r[4], regs->r[5], regs->r[6], regs->r[7]);
	printf("x8-11       %lx %lx %lx %lx\n",
	       regs->r[8], regs->r[9], regs->r[10], regs->r[11]);
	printf("x12-15      %lx %lx %lx %lx\n",
	       regs->r[12], regs->r[13], regs->r[14], regs->r[15]);
	printf("x16-19      %lx %lx %lx %lx\n",
	       regs->r[16], regs->r[17], regs->r[18], regs->r[19]);
	printf("x20-23      %lx %lx %lx %lx\n",
	       regs->r[20], regs->r[21], regs->r[22], regs->r[23]);
	printf("x24-27      %lx %lx %lx %lx\n",
	       regs->r[24], regs->r[25], regs->r[26], regs->r[27]);
	printf("x28-lr,sp   %lx %lx %lx %lx\n",
	       regs->r[28], regs->r[29], regs->lr, regs->sp);
	printf("pc,sr,pf,ex %lx         %08x %lx         %08x\n",
	       regs->pc, (unsigned int)regs->cpsr,
	       regs->addr, (unsigned int)regs->esr);
}

__cold void arm_handler_fiq(struct regs *regs)
{
	regs->esr = 0;
	regs->addr = 0;
	panic_regs(regs, "Unsupported FIQ\n");
}

__cold void arm_handler_serror(struct regs *regs, ulong_t esr, ulong_t far)
{
	regs->esr = esr;
	regs->addr = far;
	panic_regs(regs, "Unsupported SError exception\n");
}

void arm_handler_irq_user(struct regs *regs __unused)
{
	bsp_irq_dispatch(0);
}

__cold void arm_handler_irq_kern(struct regs *regs)
{
	regs->esr = 0;
	regs->addr = 0;
	panic_regs(regs, "Unsupported IRQ in kernel mode\n");
}

__cold void arm_handler_sync_kern(struct regs *regs, ulong_t esr, ulong_t far)
{
	regs->esr = esr;
	regs->addr = far;
	panic_regs(regs, "Unsupported exception in kernel mode\n");
}

void arm_handler_sync_user(struct regs *regs, ulong_t esr, ulong_t far)
{
	unsigned long ec;
	unsigned long info;

	regs->esr = esr;

	ec = esr >> ESR_EC_SHIFT;
	switch (ec) {
	case ESR_EC_IABT_USER:
	case ESR_EC_DABT_USER:
		if ((esr & ESR_FAULT_FNV) == 0) {
			regs->addr = far;
		} else {
			regs->addr = 0;
		}

		/* instruction or data abort needs further dispatching */
		switch (esr & ESR_FSC_MASK) {
		case ESR_FSC_ADDR_SIZE:
		case ESR_FSC_TRANSLATION:
			info = EX_TYPE_PAGEFAULT;
			break;

		case ESR_FSC_ACCESS_FLAG:
		case ESR_FSC_PERM:
			info = EX_TYPE_PAGEFAULT | EX_PERM;
			break;

		case ESR_FSC_SYNCEA:
		case ESR_FSC_SYNCEA_PT:
		case ESR_FSC_PARITY:
		case ESR_FSC_PARITY_PT:
			info = EX_TYPE_BUS;
			break;

		case ESR_FSC_ALIGN:
			info = EX_TYPE_ALIGN;
			break;

		case ESR_FSC_IMPL_DEF:
			if ((esr & 0x3) == 0x1) {
				/* unsupported exclusive access */
				info = EX_TYPE_BUS;
				break;
			}
			/* fall-through */

		default:
			panic_regs(regs, "Unsupported abort or TLB error\n");
		}

		/* Fault access type:
		 * cache maintenance are reported as write faults,
		 * but actually require read permissions.
		 * We report a read fault to user space.
		 */
		if (((esr & ESR_FAULT_WR) != 0) && ((esr & ESR_FAULT_CM) == 0)) {
			info |= EX_WRITE;
		} else if (ec == ESR_EC_DABT_USER) {
			info |= EX_READ;
		} else {
			info |= EX_EXEC;
		}
		break;

	case ESR_EC_FPU_UNAVAIL:
		info = EX_TYPE_FPU_UNAVAIL;
		break;

	case ESR_EC_FPU32:
	case ESR_EC_FPU64:
		info = EX_TYPE_FPU_ERROR;
		break;

	case ESR_EC_ILLEGAL:
		panic_regs(regs, "Illegal execution state\n");

	case ESR_EC_SVC32:
	case ESR_EC_SVC64:
		info = EX_TYPE_SYSCALL;
		break;

	case ESR_EC_BREAK_USER:
		info = EX_TYPE_DEBUG | EX_EXEC;
		break;

	case ESR_EC_STEP_USER:
		info = EX_TYPE_DEBUG;
		break;

	case ESR_EC_WATCH_USER:
		regs->addr = far;
		if (((esr & ESR_FAULT_WR) != 0) && ((esr & ESR_FAULT_CM) == 0)) {
			info = EX_TYPE_DEBUG | EX_WRITE;
		} else {
			info = EX_TYPE_DEBUG | EX_READ;
		}
		break;

	case ESR_EC_BRK32:
	case ESR_EC_BRK64:
		info = EX_TYPE_TRAP;
		break;

	case ESR_EC_PC_ALIGN:
		regs->addr = far;
		info = EX_TYPE_ALIGN | EX_EXEC;
		break;

	case ESR_EC_SP_ALIGN:
		/* The hardware does not provide a fault address,
		 * so we provide the stack pointer here.
		 */
		regs->addr = regs->sp;
		info = EX_TYPE_ALIGN | EX_READ | EX_WRITE;
		break;

	default:
		/* We map the rest as illegal instruction. */
		info = EX_TYPE_ILLEGAL;
		break;
	}
	kernel_exception(regs, info);
}

static __init void arm_enable_timer_access(void)
{
#define CNTKCTL_EL0PCTEN	0x001	/* enable user read of physical counter */
#define CNTKCTL_EL0VCTEN	0x002	/* enable user read of virtual counter */
#define CNTKCTL_EVNTEN		0x004	/* enable event generation */
#define CNTKCTL_EVNTDIR		0x008	/* event direction, 0:0->1, 1:1->0 */
#define CNTKCTL_EVNTI(x)	((x)<<4)	/* counter bit that triggers event */
#define CNTKCTL_EL0PTEN		0x100	/* enable user write to physical counter */
#define CNTKCTL_EL0VTEN		0x200	/* enable user write to virtual counter */

	/* allow user to read the virtual timer value */
	arm_set_cnt_access(CNTKCTL_EL0VCTEN);
}

static __init void arm_enable_performance_monitor(void)
{
	unsigned int num_counters;
	unsigned long ctrl;

	/*
	 * ARMv7 defines a cycle counter and up to 32 individual counters
	 * we keep the individual counters disabled
	 * except for L2 cache activity monitoring in counters 0 and 1
	 * and we enable the cycle counter by default
	 */
	num_counters = (arm_perf_get_ctrl() >> 11) & 0x1f;

	/* disable all counters, IRQs, overflows, except CCTR */
	arm_perf_disable_counter(ARM_PERF_MASK_ALL);
	arm_perf_int_mask(ARM_PERF_MASK_ALL);
	arm_perf_int_ack(ARM_PERF_MASK_ALL);

	/* enable L2 cache activity monitoring in counters 0 to 2 */
	if (num_counters >= 2) {
		arm_perf_type0(ARM_PERF_EVENT_DC2R);
		arm_perf_type1(ARM_PERF_EVENT_DC2W);
		arm_perf_type2(ARM_PERF_EVENT_BA);
		arm_perf_enable_counter((1<<0) | (1<<1) | (1<<2));
	}
	arm_perf_enable_counter(ARM_PERF_MASK_CCNT);

	/* enable only the cycle counter + reset cycle and event counters */
	ctrl = arm_perf_get_ctrl();
	ctrl &= ~ARM_PERF_PMCR_D;
	ctrl |= ARM_PERF_PMCR_C | ARM_PERF_PMCR_P | ARM_PERF_PMCR_E;
	arm_perf_set_ctrl(ctrl);

	/* enable access to performance monitor registers from user space */
	arm_perf_set_useren(ARM_PERF_USERENR_EN);
}

__init void arch_init_exceptions(void)
{
	/* finally switch to direct vectors, if supported */
	arm_set_vbar((unsigned long)arm_vectors_el1);

	arm_enable_timer_access();
	arm_enable_performance_monitor();
}

/** change MMU page tables on context switch */
void arch_adspace_switch(const struct arch_adspace_cfg *cfg)
{
	assert(cfg != NULL);

	arm_set_ttbr0_lpae(cfg->ttbr0, cfg->asid);
	arm_isb();
}
