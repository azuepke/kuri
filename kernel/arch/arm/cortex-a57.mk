# SPDX-License-Identifier: MIT
# cortex-a57.mk -- for Cortex-A57 (64-bit)
#
# ARM architecture specific build rules
#
# azuepke, 2017-09-20: imported

_ARCH := -mcpu=cortex-a57
_MODE := $(call cc-option,-mno-outline-atomics,)
_DEFS := -DARM_V8 -DARM_CORTEX_A57

ARCH_CFLAGS := $(_ARCH) $(_MODE) $(_DEFS) -mgeneral-regs-only

ARCH_CFLAGS_DEBUG := -Og
ARCH_CFLAGS_NDEBUG := -O2 -fomit-frame-pointer

ARCH_AFLAGS := $(_ARCH) $(_MODE)
ARCH_LDFLAGS := -maarch64linux

ARCH_MODS := entry64 exception64


# Recommended user compiler and linker flags
ARCH_USER_CFLAGS := $(_ARCH) $(_MODE)
ARCH_USER_AFLAGS := $(_ARCH) $(_MODE)
ARCH_USER_LDFLAGS := -maarch64linux
