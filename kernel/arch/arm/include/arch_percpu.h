/* SPDX-License-Identifier: MIT */
/*
 * arch_percpu.h
 *
 * Getter to per-CPU state.
 *
 * azuepke, 2018-03-05: initial
 */

#ifndef __ARCH_PERCPU_H__
#define __ARCH_PERCPU_H__

#include <arm_insn.h>
#include <compiler.h>

/* forward declarations */
struct percpu;

/** get pointer to per-CPU data */
static inline struct percpu *arch_percpu(void)
{
	return (struct percpu *)arm_get_tls2();
}

/** initializer for architecture-specific per-CPU data */
static inline void arch_percpu_init(
	struct arch_percpu *arch __unused,
	unsigned int cpu_id __unused)
{
}

#endif
