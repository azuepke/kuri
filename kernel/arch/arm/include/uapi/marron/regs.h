/* SPDX-License-Identifier: MIT */
/*
 * marron/regs.h
 *
 * Register frame on ARM.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-09-29: imported and simplified
 * azuepke, 2018-03-16: split into arch_regs.h and arch_regs_types.h
 * azuepke, 2018-03-16: merge integer and FPU context into struct regs
 * azuepke, 2018-12-21: standalone version, rename to marron/regs.h
 */

#ifndef __REGS_H__
#define __REGS_H__

#include <marron/types.h>

/** exception register frame (user registers) */
struct regs {
#ifdef __aarch64__
	ulong_t r[30];
	ulong_t lr;		/* x30 */
	ulong_t sp;		/* x31 */
	ulong_t pc;
	ulong_t cpsr;	/* PSTATE */

	/* register frame saved up until here on exception entry */

	/* only valid for exceptions from user space */
	ulong_t esr;	/* exception syndrome register (ESR) */
	ulong_t addr;	/* fault address */
	unsigned int info;	/* generic exception information */

	/* saved on context switching */
	unsigned int mdscr;	/* only MDSCR_EL1.SS (bit 0 ) is accessible */
	ulong_t tls0;	/* TPIDRURW */
	ulong_t tls1;	/* TPIDRURO */

	/** FPU registers */
	unsigned int fpsr;
	unsigned int fpcr;
	__uint128_t fpregs[32];
#else
	ulong_t r[13];
	ulong_t sp;		/* r13 */
	ulong_t lr;		/* r14 */
	ulong_t pc;		/* r15 */
	ulong_t cpsr;

	/* register frame saved up until here on exception entry */

	/* only valid for exceptions from user space */
	ulong_t fsr;	/* fault status register (FSR) */
	ulong_t addr;	/* fault address or undefined instruction */
	unsigned int info;	/* generic exception information */

	/* saved on context switching */
	ulong_t tls0;	/* TPIDRURW */
	ulong_t tls1;	/* TPIDRURO */

	/** FPU registers */
	unsigned int fpscr;
	unsigned int fp_padding;
	unsigned long long fpregs[32];
#endif
};

#endif
