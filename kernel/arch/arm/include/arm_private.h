/* SPDX-License-Identifier: MIT */
/*
 * arm_private.h
 *
 * ARM private functions
 *
 * azuepke, 2017-10-02: initial
 */

#ifndef __ARM_PRIVATE_H__
#define __ARM_PRIVATE_H__

#include <marron/regs.h>

/* entry.S and entry64.S */
#ifdef __aarch64__
void arch_entry(unsigned long kernel_stack, unsigned long fiq_stack, unsigned long serror_stack);
extern char arm_vectors_el1[];
#else
void arch_entry(unsigned long kernel_stack, unsigned long fiq_stack);
extern char arm_vectors[];
#endif
void arm_fpu_save(void *fpu_regs);
void arm_fpu_restore(const void *fpu_regs);

#ifdef __aarch64__
/* exception64.S */
void arm_handler_sync_kern(struct regs *regs, ulong_t esr, ulong_t far);
void arm_handler_sync_user(struct regs *regs, ulong_t esr, ulong_t far);
void arm_handler_irq_user(struct regs *regs);
void arm_handler_irq_kern(struct regs *regs);
void arm_handler_fiq(struct regs *regs);
void arm_handler_serror(struct regs *regs, ulong_t esr, ulong_t far);
#else
/* exception.S */
void arm_handler_undef_user(struct regs *regs);
void arm_handler_undef_kern(struct regs *regs);
void arm_handler_pabt_user(struct regs *regs, ulong_t ifsr, ulong_t ifar);
void arm_handler_pabt_kern(struct regs *regs, ulong_t ifsr, ulong_t ifar);
void arm_handler_dabt_user(struct regs *regs, ulong_t dfsr, ulong_t dfar);
void arm_handler_dabt_kern(struct regs *regs, ulong_t dfsr, ulong_t dfar);
void arm_handler_irq(struct regs *regs);
void arm_handler_fiq(struct regs *regs);
#endif

#endif
