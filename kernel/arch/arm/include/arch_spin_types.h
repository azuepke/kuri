/*
 * arch_spin_types.h
 *
 * ARM ticket spin locks
 *
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef __ARCH_SPIN_TYPES_H__
#define __ARCH_SPIN_TYPES_H__

#include <stdint.h>

/** spin lock data type */
typedef union {
	uint32_t lock;
	struct {
		/* little endian order: ticket is as MSB position */
		uint16_t owner_cpu;
		uint8_t served;
		uint8_t ticket;
	} s;
} arch_spin_t;

/** spin lock static initializer */
#define ARCH_SPIN_INIT { 0 }

#endif
