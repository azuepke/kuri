/* SPDX-License-Identifier: MIT */
/*
 * arch_percpu_types.h
 *
 * ARM-specific per-CPU state.
 *
 * azuepke, 2017-10-02: initial
 * azuepke, 2018-03-05: renamed from struct arch_cpu
 */

#ifndef __ARCH_PERCPU_TYPES_H__
#define __ARCH_PERCPU_TYPES_H__

#ifndef __ASSEMBLER__

#include <stdint.h>

/** per-CPU structure, kept in TLS2. The kernel stack is put before */
struct arch_percpu {
#ifdef __aarch64__
	unsigned long fiq_stack;
	unsigned long serror_stack;
#else /* 32-bit ARM */
	int unused;
#endif
};

#endif

#define ARCH_PERCPU_FIQ_STACK_OFFSET	0
#define ARCH_PERCPU_SERROR_STACK_OFFSET	8

#endif
