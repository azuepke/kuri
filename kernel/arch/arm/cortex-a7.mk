# SPDX-License-Identifier: MIT
# cortex-a7.mk -- for Cortex-A7
#
# ARM architecture specific build rules
#
# azuepke, 2020-07-17: imported

_ARCH := -mcpu=cortex-a7
#_MODE := -mthumb -Wa,-mthumb -Wa,-mimplicit-it=always # Thumb mode
_MODE := -marm -mno-thumb-interwork # ARM mode
_DEFS := -DARM_V7 -DARM_CORTEX_A7 -DARM_VFP32

ARCH_CFLAGS := $(_ARCH) $(_MODE) $(_DEFS) -mabi=aapcs-linux -msoft-float

ARCH_CFLAGS_DEBUG := -Og
ARCH_CFLAGS_NDEBUG := -O2 -fomit-frame-pointer

ARCH_AFLAGS := $(_ARCH) $(_MODE)
ARCH_LDFLAGS := $(call ld-option,-marmelf_linux_eabi,)

ARCH_MODS := entry exception div


# Recommended user compiler and linker flags
ARCH_USER_CFLAGS := $(_ARCH) $(_MODE)
ARCH_USER_AFLAGS := $(_ARCH) $(_MODE)
ARCH_USER_LDFLAGS := $(call ld-option,-marmelf_linux_eabi,)
