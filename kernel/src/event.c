/* SPDX-License-Identifier: MIT */
/*
 * event.c
 *
 * Event implementation
 *
 * azuepke, 2018-04-10: initial
 */

#include <marron/error.h>
#include <event.h>
#include <thread.h>
#include <assert.h>
#include <sched.h>
#include <part.h>


/** initialize all event objects at boot time */
__init void event_init_all(void)
{
	for (unsigned int i = 0; i < event_wait_num; i++) {
		struct event_wait *ew = &event_wait_dyn[i];

		ew->waiter = NULL;
		ew->pending = false;
		ew->part = NULL;
	}
}

/** Assign event to partition */
__init void event_assign_part(struct event_wait *event_wait, struct part *part)
{
	assert(event_wait != NULL);
	assert(part != NULL);

	assert(event_wait->part == NULL);
	event_wait->part = part;
}

/** Block current thread on event object */
err_t event_wait(struct event_wait *event_wait, sys_timeout_t timeout)
{
	struct thread *thr;
	err_t err;

	assert(event_wait != NULL);

	part_lock(event_wait->part);

	if (event_wait->pending != false) {
		/* event immediately delivered */
		event_wait->pending = false;
		err = EOK;
		goto out;
	}

	if (event_wait->waiter != NULL) {
		/* someone else is already waiting */
		err = EAGAIN;
		goto out;
	}

	/* register the current thread as waiter on the event */
	thr = current_thread();
	thr->event_wait = event_wait;
	event_wait->waiter = thr;

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_EVENT, timeout);
	sched_unlock(thr->sched);

	/* set error code to ETIMEOUT in case the timeout triggers */
	err = ETIMEOUT;
out:
	part_unlock(event_wait->part);

	return err;
}

/** Callback for cleanup during wakeup (timeout, thread deletion) */
void event_wait_cancel(struct thread *thr)
{
	struct event_wait *event_wait;

	assert(thr != NULL);
	event_wait = thr->event_wait;

	/* NOTE: SMP: requires double checking */
	if (event_wait != NULL) {
		part_lock(event_wait->part);

		if (event_wait->waiter == thr) {
			/* unregister thread from event object and vice versa */
			thr->event_wait = NULL;
			event_wait->waiter = NULL;
		}

		part_unlock(event_wait->part);
	}
}

/** Wake a thread waiting on event object, if any */
void event_send(const struct event_send_cfg *event_cfg)
{
	struct event_wait *event_wait;
	struct thread *thr;

	assert(event_cfg != NULL);
	event_wait = event_cfg->event_wait;
	assert(event_wait != NULL);

	part_lock(event_wait->part);

	thr = event_wait->waiter;
	if (thr != NULL) {
		/* wake waiting waiter */
		event_wait->waiter = NULL;
		assert(thr->event_wait == event_wait);
		thr->event_wait = NULL;

		/* wake waiting thread with EOK */
		REG_OUT0(thr->regs) = EOK;
		sched_lock(thr->sched);
		thread_wakeup(thr);
		sched_unlock(thr->sched);
	} else {
		/* no waiter, just mark event as pending */
		event_wait->pending = true;
	}

	part_unlock(event_wait->part);
}
