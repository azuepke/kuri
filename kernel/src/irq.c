/* SPDX-License-Identifier: MIT */
/*
 * irq.c
 *
 * IRQ handling.
 *
 * azuepke, 2018-01-13: initial
 */

#include <marron/error.h>
#include <irq.h>
#include <compiler.h>
#include <bsp.h>
#include <assert.h>
#include <stddef.h>
#include <thread.h>
#include <current.h>
#include <sched.h>


/** initialize all IRQ objects at boot time */
__init void irq_init_all(void)
{
	const struct irq_cfg *cfg;
	struct irq *irq;

	assert(irq_table_num <= bsp_irq_num);

	for (unsigned int i = 0; i < irq_num; i++) {
		cfg = &irq_cfg[i];
		irq = &irq_dyn[i];
		//assert(irq == cfg->irq);
		assert(cfg->irq_id < irq_table_num);
		assert(irq_table[cfg->irq_id] == irq);

		//irq->irq_cfg = cfg;
		irq->irq_id = cfg->irq_id;
		irq->enabled = false;
		irq->waiter = NULL;
		irq->count = 0;

		spin_init(&irq->lock);
	}
}

/** assign IRQ to partition at boot time */
__init void irq_assign_part(struct irq *irq, const struct part_cfg *cfg)
{
	assert(irq != NULL);
	assert(cfg != NULL);

	irq->part_cfg = cfg;
}

/** wake thread waiting for interrupt */
static void irq_wake(struct irq *irq, err_t wakeup_code)
{
	struct thread *thr;

	assert(irq != NULL);
	assert(irq->enabled == true);
	assert((wakeup_code == EOK) || (wakeup_code == ECANCEL));
	assert_irq_locked(irq);

	thr = irq->waiter;
	assert(thr != NULL);
	irq->waiter = NULL;

	/* wake waiting thread */
	assert(thr->state == THREAD_STATE_WAIT_IRQ);
	REG_OUT0(thr->regs) = wakeup_code;
	sched_lock(thr->sched);
	thread_wakeup(thr);
	sched_unlock(thr->sched);
}

/** handle an IRQ */
void irq_handle(struct irq *irq)
{
	assert(irq != NULL);

	/* wake waiting thread */
	irq_lock(irq);

	assert(irq->enabled == true);
	if (irq->waiter != NULL) {
		irq_wake(irq, EOK);
	}

	irq_unlock(irq);
}

/** configure interrupt mode and enable interrupt */
err_t irq_enable(struct irq *irq, irq_mode_t irq_mode)
{
	err_t err;

	assert(irq != NULL);

	irq_lock(irq);

	if (irq->enabled == true) {
		/* IRQ already enabled */
		err = ESTATE;
		goto out;
	}
	assert(irq->enabled == false);

	err = bsp_irq_config(irq->irq_id, irq_mode);
	if (err != EOK) {
		/* IRQ mode not available */
		goto out;
	}

	/* mark IRQ as enabled */
	irq->enabled = true;
	err = EOK;

out:
	irq_unlock(irq);

	return err;
}

/** disable interrupt (internal function; called locked) */
static err_t irq_disable_internal(struct irq *irq)
{
	assert(irq != NULL);
	assert_irq_locked(irq);

	if (irq->enabled == false) {
		/* IRQ not enabled */
		return ESTATE;
	}
	assert(irq->enabled == true);

	if (irq->waiter != NULL) {
		/* disable IRQ in interrupt controller and wake waiter */
		bsp_irq_disable(irq->irq_id);
		irq_wake(irq, ECANCEL);
	}

	irq->enabled = false;

	return EOK;
}

/** disable interrupt */
err_t irq_disable(struct irq *irq)
{
	err_t err;

	assert(irq != NULL);

	irq_lock(irq);
	err = irq_disable_internal(irq);
	irq_unlock(irq);

	return err;
}

/** current thread waits for interrupt */
err_t irq_wait(struct irq *irq, sys_timeout_t timeout)
{
	struct thread *thr;
	err_t err;

	assert(irq != NULL);

	irq_lock(irq);

	if (irq->enabled == false) {
		/* IRQ not enabled */
		err = ESTATE;
		goto out;
	}

	if (irq->waiter != NULL) {
		/* other thread already waiting */
		err = EAGAIN;
		goto out;
	}

	/* let thread wait for IRQ and enable IRQ in hardware */
	thr = current_thread();
	thr->irq = irq;
	irq->waiter = thr;

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_IRQ, timeout);
	sched_unlock(thr->sched);

	bsp_irq_enable(irq->irq_id, thr->sched->cpu);

	/* set error code to ETIMEOUT in case the timeout triggers */
	err = ETIMEOUT;

out:
	irq_unlock(irq);

	return err;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
void irq_wait_cancel(struct thread *thr)
{
	struct irq *irq;

	assert(thr != NULL);
	irq = thr->irq;
	assert(irq != NULL);

	irq_lock(irq);
	if (irq->waiter == thr) {
		assert(irq->enabled);

		/* disable IRQ in interrupt controller, but do not wake waiter */
		irq->waiter = NULL;

		bsp_irq_disable(irq->irq_id);
	}
	irq_unlock(irq);
}

/** kill interrupt at partition shutdown */
void irq_shutdown(struct irq *irq)
{
	assert(irq != NULL);

	irq_lock(irq);
	if (irq->enabled == true) {
		irq_disable_internal(irq);
	}
	assert(irq->enabled == false);
	irq_unlock(irq);
}
