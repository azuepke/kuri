/* SPDX-License-Identifier: MIT */
/*
 * thread.c
 *
 * Threading
 *
 * azuepke, 2018-01-02: initial
 */

#include <thread.h>
#include <part.h>
#include <sched.h>
#include <current.h>
#include <percpu.h>
#include <adspace_types.h>
#include <sig.h>
#include <irq.h>
#include <futex.h>
#include <sync.h>
#include <kernel.h>
#include <event.h>
#include <user.h>


/** initialize all thread objects at boot time to be blocked in STATE_DEAD */
__init void thread_init_all(void)
{
	for (unsigned int i = 0; i < thread_num; i++) {
		struct thread *thr = &thread_dyn[i];

		thr->regs = &thread_regs[i];
		thr->continuation = NULL;

		list_node_init(&thr->readyql);
		list_node_init(&thr->timeoutql);
		thr->timeout = TIMEOUT_INFINITE;

		thr->suspend_wakeup = 0;
		thr->lwsync_user_state_addr = 0;
		thr->sig_mask_blocked = 0;
		thr->sig_mask_pending = 0;
		thr->fpu_state = FPU_STATE_AUTO;

		thr->irq = NULL;
		thr->futex_addr = 0;
		list_node_init(&thr->futex_waitql);
		thr->event_wait = NULL;

		thr->prio = 0;
		thr->state = THREAD_STATE_DEAD;

		thr->cond_mutex = NULL;
		list_node_init(&thr->sync_waitql);
	}
}

/** bild thread object to a partition at boot time */
__init void thread_assign_part(
	struct thread *thr,
	unsigned int id,
	const struct part_cfg *this_part_cfg)
{
	assert(thr != NULL);
	assert(this_part_cfg != NULL);

	thr->part_cfg = this_part_cfg;
	thr->sched = sched_per_cpu(this_part_cfg->first_cpu);
	thr->id = id;
}

/** update next priority in user space */
static inline void update_next_prio(
	struct sched *sched,
	struct thread *thr)
{
	int next_prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(sched->current == thr);

	if (thr->user_tls != NULL) {
		next_prio = sched->next_highest_prio;
		if (next_prio < 0) {
			next_prio = 0;
		}
		user_put_1(&thr->user_tls->next_prio, next_prio & 0xffu);
	}
}

/** perform context switch */
/* NOTE: SMP: runs unlocked */
static struct regs *switch_threads(
	struct sched *sched,
	struct thread *old_thr,
	struct thread *new_thr)
{
	void (*continuation)(struct thread *);

	assert(old_thr != NULL);
	assert(new_thr != NULL);
	assert(new_thr != old_thr);
	assert(new_thr->state == THREAD_STATE_CURRENT);

	// switch additional registers
	arch_reg_save(old_thr->regs);

	if (old_thr->fpu_state == FPU_STATE_ON) {
		arch_fpu_save(old_thr->regs);
		arch_fpu_disable();
	}

	arch_reg_switch();
	percpu_thread_switch(new_thr);

	/* switch address space when partition changes */
	if (new_thr->part_cfg != old_thr->part_cfg) {
		arch_adspace_switch(&adspace_cfg[new_thr->part_cfg->id]);
	}

	arch_reg_restore(new_thr->regs);

	if (new_thr->fpu_state == FPU_STATE_ON) {
		arch_fpu_enable();
		arch_fpu_restore(new_thr->regs);
	}

	/* update next_prio in user space after address space switch */
	update_next_prio(sched, new_thr);

	/* continuation callback */
	continuation = new_thr->continuation;
	if (continuation != NULL) {
		new_thr->continuation = NULL;
		continuation(new_thr);
	}

	/* return to thread, send signals if any */
	return sig_send_if_pending(new_thr);
}

/* perform context switch on kernel exit if necessary
 *
 * NOTE: we keep the real switch in a dedicated function,
 * this helps to the compiler to optimize better
 */
struct regs *kernel_schedule(void)
{
	struct thread *current;
	struct thread *next;
	struct sched *sched;

	current = current_thread();
	sched = current->sched;

	if (sched->reschedule == 0) {
		assert(current->continuation == NULL);

		/* shortcut: immediately return to thread, send signals if any */
		return sig_send_if_pending(current);
	}

	current->prio = thread_prio_get(current);
	sched_lock(sched);
	sched_preempt(sched);
	sched_readyq_next(sched);
	sched_unlock(sched);

	next = sched->current;

	if (next == current) {
		assert(current->continuation == NULL);

		/* ready queue changed -- also update next_prio in user space */
		update_next_prio(sched, current);

		/* shortcut: immediately return to thread, send signals if any */
		return sig_send_if_pending(current);
	}

	return switch_threads(sched, current, next);
}

/** start a thread */
void thread_start(
	struct thread *thr,
	ulong_t entry,
	ulong_t arg,
	ulong_t stack,
	ulong_t tls,
	int prio,
	unsigned int cpu_id)
{
	struct sys_tls tls_data;

	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_DEAD);

	/* initial setup of TLS data */
	tls_data.tls = (void *)tls;
	tls_data.thr_id = thr->id;
	tls_data.user_prio = prio;
	tls_data.next_prio = 0;
	tls_data.max_prio = thr->part_cfg->max_prio;
	tls_data.cpu_id = cpu_id;
	(void)user_copy_out((void *)tls, &tls_data, sizeof(tls_data));
	thr->user_tls = (struct sys_tls *)tls;

	/* set prio + CPU + registers to given values */
	thr->prio = prio;
	thr->sched = sched_per_cpu(cpu_id);
	arch_reg_init(thr->regs, entry, stack, arg, tls);

	/* reset thread state */
	thr->suspend_wakeup = 0;
	thr->lwsync_user_state_addr = 0;
	thr->sig_mask_blocked = 0;
	thr->sig_mask_pending = 0;
	/* initialize FPU registers */
	thr->fpu_state = FPU_STATE_AUTO;
	arch_fpu_init(thr->regs);

	sched_lock(thr->sched);
	thread_wakeup(thr);
	sched_unlock(thr->sched);
}

/** kill a thread in any state */
void thread_kill(struct thread *thr)
{
	unsigned int prev_thr_state;
	struct sched *sched;

	assert(thr != NULL);

	sched = thr->sched;
	sched_lock(sched);

	prev_thr_state = thr->state;
	thr->state = THREAD_STATE_DEAD;

	switch (prev_thr_state) {
	case THREAD_STATE_CURRENT:
		/* CURRENT -> DEAD */
		// FIXME: not true when killing threads on other CPUs!
		assert(thr == current_thread());
		thr->timeout = TIMEOUT_INFINITE;

		/* The current thread dies:
		 * Perform a fake switch to the idle thread first
		 * to save our registers, then change the state to dead,
		 * so we can be restarted properly afterwards.
		*/
		sched->current = sched->idle;
		sched->idle->state = THREAD_STATE_CURRENT;
		switch_threads(sched, thr, sched->idle);
		break;

	case THREAD_STATE_READY:
		/* READY -> DEAD */
		sched_remove(sched, thr);
		break;

	case THREAD_STATE_WAIT_SLEEP:
		/* WAIT_SLEEP -> DEAD */
		/* clear any pending timeout */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_SUSPEND:
		/* WAIT_SUSPEND -> DEAD */
		/* no timeout here, just change state */
		assert(thr->timeout == TIMEOUT_INFINITE);
		break;

	case THREAD_STATE_WAIT_IRQ:
		/* WAIT_IRQ -> DEAD */
		/* clear any pending timeout, then release and mask IRQ */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_FUTEX:
		/* WAIT_FUTEX -> DEAD */
		/* clear any pending timeout, then remove from futex queue */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_LWSYNC:
		/* WAIT_LWSYNC -> DEAD */
		/* clear any pending timeout, but no further queues involved */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_MUTEX:
		/* WAIT_MUTEX -> DEAD */
		/* clear any pending timeout, then remove from sync object wait queue */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_COND:
		/* WAIT_COND -> DEAD */
		/* clear any pending timeout, then remove from sync object wait queue */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_EVENT:
		/* WAIT_EVENT -> DEAD */
		/* clear any pending timeout, then remove from event object */
		sched_timeout_release(sched, thr);
		break;

	default:
		assert(prev_thr_state == THREAD_STATE_DEAD);
		/* already DEAD */
		break;
	}

	sched_unlock(sched);

	/* NOTE: SMP: the following runs unlocked */
	switch (prev_thr_state) {
	case THREAD_STATE_WAIT_IRQ:
		/* release and mask IRQ */
		irq_wait_cancel(thr);
		break;

	case THREAD_STATE_WAIT_FUTEX:
		/* remove from futex queue */
		futex_wait_cancel(thr);
		break;

	case THREAD_STATE_WAIT_MUTEX:
		/* remove from sync object wait queue */
		sync_mutex_wait_cancel(thr);
		break;

	case THREAD_STATE_WAIT_COND:
		/* remove from sync object wait queue */
		(void)sync_cond_wait_cancel(thr, false);
		break;

	case THREAD_STATE_WAIT_EVENT:
		/* remove from event object*/
		event_wait_cancel(thr);
		break;
	}

	assert(thr->state == THREAD_STATE_DEAD);
}

/** let current thread wait in wait state with timeout */
void thread_wait(struct thread *thr, unsigned int state, sys_time_t timeout)
{
	assert(thr != NULL);
	assert(thr == current_thread());
	assert(state > THREAD_STATE_READY);
	assert_sched_locked(thr->sched);

	thr->state = state;
	thr->timeout = timeout;
	thr->prio = thread_prio_get(thr);

	sched_wait(thr->sched, thr);
}

/** let current thread wait in wait state with timeout and given wait priority */
void thread_wait_prio(struct thread *thr, unsigned int state, sys_time_t timeout, unsigned int wait_prio)
{
	assert(thr != NULL);
	assert(thr == current_thread());
	assert(state > THREAD_STATE_READY);
	assert_sched_locked(thr->sched);

	thr->state = state;
	thr->timeout = timeout;
	thr->prio = wait_prio;

	sched_wait(thr->sched, thr);
}

/** wake a thread and make it READY again */
void thread_wakeup(struct thread *thr)
{
	struct sched *sched;

	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);
	assert_sched_locked(thr->sched);

	sched = thr->sched;

	sched_wakeup(sched, thr);

	/* ready queue changed -- update next priority in user space */
	update_next_prio(sched, sched->current);

#ifdef SMP
	if (sched->cpu != current_cpu_id()) {
		/* notify target CPU */
		bsp_cpu_reschedule(sched->cpu);
	}
#endif
}

/** release the timeout of a blocked thread */
void thread_timeout_release(struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	sched_lock(thr->sched);
	sched_timeout_release(thr->sched, thr);
	sched_unlock(thr->sched);
}

/** do cleanup of special waiting states after timeout or thread deletion */
/* NOTE: SMP: this runs unlocked */
bool_t thread_wait_cancel(struct thread *thr)
{
	assert(thr != NULL);

	assert(thr->state > THREAD_STATE_READY);

	if (thr->state == THREAD_STATE_WAIT_IRQ) {
		irq_wait_cancel(thr);
	} else if (thr->state == THREAD_STATE_WAIT_FUTEX) {
		futex_wait_cancel(thr);
	} else if (thr->state == THREAD_STATE_WAIT_MUTEX) {
		sync_mutex_wait_cancel(thr);
	} else if (thr->state == THREAD_STATE_WAIT_COND) {
		/* condition variables are special -- requeue to mutex */
		return sync_cond_wait_cancel(thr, true);
	} else if (thr->state == THREAD_STATE_WAIT_EVENT) {
		event_wait_cancel(thr);
	}

	return true;
}

/** preempt current thread (enforce priority changes) */
/* NOTE: SMP: this runs unlocked */
void thread_preempt(struct thread *thr)
{
	assert(thr == current_thread());

	thr->prio = thread_prio_get(thr);
	//sched_check_preempt(thr->sched);
	/* FIXME: enforce scheduling to update next_prio in user space */
	thr->sched->reschedule = 1;
	/* NOTE: we schedule anyway, no need to sync next priority in user space */
}

/** yield current thread */
void thread_yield(struct thread *thr)
{
	assert(thr == current_thread());

	thr->prio = thread_prio_get(thr);
	sched_lock(thr->sched);
	sched_yield(thr->sched, thr);
	sched_unlock(thr->sched);
	/* NOTE: we schedule anyway, no need to sync next priority in user space */
}

/** get bounded user priority of current thread */
/* NOTE: SMP: this runs unlocked */
int thread_prio_get(struct thread *thr)
{
	int user_prio;

	assert(thr == current_thread());

	if (thr->user_tls == NULL) {
		/* only the idle thread does not have a TLS */
		assert(thr->prio == -1);
		return -1;
	}

	/* bound priority in 0..part_cfg->max_prio */
	/* NOTE: the user priority is of type uint8_t */
	user_prio = user_get_1(&thr->user_tls->user_prio);
	if (user_prio > thr->part_cfg->max_prio) {
		user_prio = thr->part_cfg->max_prio;
	}
	assert(user_prio >= 0);
	assert(user_prio < NUM_PRIOS);

	return user_prio;
}

/** change priority of current thread */
/* NOTE: SMP: this runs unlocked */
int thread_prio_change(struct thread *thr, int new_prio)
{
	int old_prio;

	assert(thr == current_thread());

	/* bound priority in 0..part_cfg->max_prio */
	/* NOTE: assume priority as unsigned and map negative ones to max */
	if ((new_prio < 0) || (new_prio > thr->part_cfg->max_prio)) {
		new_prio = thr->part_cfg->max_prio;
	}
	assert(new_prio >= 0);
	assert(new_prio < NUM_PRIOS);

	old_prio = thread_prio_get(thr);

	thr->prio = new_prio;
	assert(thr->user_tls != NULL);
	user_put_1(&thr->user_tls->user_prio, new_prio & 0xffu);

	// NOTE: SMP: no sched_lock here
	sched_check_preempt(thr->sched);

	return old_prio;
}

/** change CPU of current thread */
void thread_cpu_migrate(struct thread *thr, unsigned int new_cpu)
{
	struct sched *old_sched;
	struct sched *new_sched;

	assert(thr == current_thread());
	assert(new_cpu < cpu_num);

	/* sync user space priority before migration */
	thr->prio = thread_prio_get(thr);

	/* We must save all our registers before migration,
	 * otherwise we could we woken up too early on the new CPU.
	 * So we perform a fake switch to the idle thread first
	 * to save our registers, then we migrate,
	 * and finally reschedule on our CPU.
	 */
	old_sched = thr->sched;
	new_sched = sched_per_cpu(new_cpu);
	if (new_sched == old_sched) {
		return;
	}

	/* update TLS for sys_cpu_get() */
	assert(thr->user_tls != NULL);
	user_put_1(&thr->user_tls->cpu_id, new_cpu & 0xffu);

	/* lock both CPU's scheduling state for a smooth transition */
	if (old_sched < new_sched) {
		sched_lock(old_sched);
		sched_lock(new_sched);
	} else {
		sched_lock(new_sched);
		sched_lock(old_sched);
	}

	old_sched->current = old_sched->idle;
	old_sched->idle->state = THREAD_STATE_CURRENT;
	switch_threads(old_sched, thr, old_sched->idle);

	old_sched->reschedule = 1;

	sched_migrate(new_sched, thr);

	sched_unlock(old_sched);
	sched_unlock(new_sched);

	/* NOTE: we schedule anyway, no need to sync next priority in user space */
}

/** suspend current thread */
err_t thread_suspend(struct thread *thr)
{
	err_t err;

	assert(thr == current_thread());

	sched_lock(thr->sched);
	if (thr->suspend_wakeup != 0) {
		/* clear pending suspension wakeup request */
		thr->suspend_wakeup = 0;
		err = EAGAIN;
	} else {
		/* suspend thread */
		thread_wait(thr, THREAD_STATE_WAIT_SUSPEND, TIMEOUT_INFINITE);
		err = EOK;
	}
	sched_unlock(thr->sched);

	return err;
}

/** resume suspended thread */
err_t thread_resume(struct thread *thr)
{
	err_t err;

	assert(thr != NULL);

	sched_lock(thr->sched);
	if (thr->state == THREAD_STATE_DEAD) {
		/* thread is dead */
		err = ESTATE;
	} else if (thr->state != THREAD_STATE_WAIT_SUSPEND) {
		/* set pending suspension wakeup request */
		thr->suspend_wakeup = 1;
		err = EOK;
	} else {
		/* thread is suspended */
		thread_wakeup(thr);
		err = EOK;
	}
	sched_unlock(thr->sched);

	return err;
}

/* Kernel timer callback, "now" refers to the current system time
 * The kernel should reprogram the next timer expiry.
 * The timer interrupt is never masked.
 */
void kernel_timer(uint64_t now)
{
	/* NOTE: SMP: locks sched_lock internally */
	sched_timeout_expire(now, current_thread()->sched);

	/* ready queue changed -- update next priority in user space */
	update_next_prio(current_thread()->sched, current_thread());
}
