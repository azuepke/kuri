/* SPDX-License-Identifier: MIT */
/*
 * lwsync.c
 *
 * Light-weight synchronization implementation
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/error.h>
#include <lwsync.h>
#include <thread.h>
#include <current.h>
#include <user.h>
#include <sched.h>
#include <assert.h>


err_t lwsync_wait(
	addr_t user_state_addr,
	int compare,
	sys_timeout_t timeout,
	unsigned int wait_prio)
{
	struct thread *thr;
	int user_state;
	err_t err;

	thr = current_thread();

	sched_lock(thr->sched);

	/* compare user state value */
	user_state = user_get_4((int*)user_state_addr);
	if (user_state != compare) {
		err = EAGAIN;
		goto out;
	}

	/* let the current thread wait */
	thr->lwsync_user_state_addr = user_state_addr;

	thread_wait_prio(thr, THREAD_STATE_WAIT_LWSYNC, timeout, wait_prio);

	/* set error code to ETIMEOUT in case the timeout triggers */
	err = ETIMEOUT;

out:
	sched_unlock(thr->sched);

	return err;
}

err_t lwsync_wake(
	struct thread *thr,
	addr_t user_state_addr,
	int compare)
{
	int user_state;
	err_t err;

	assert(thr != NULL);

	sched_lock(thr->sched);

	if (thr->state != THREAD_STATE_WAIT_LWSYNC) {
		err = ESTATE;
		goto out;
	}
	if (thr->lwsync_user_state_addr != user_state_addr) {
		err = ESTATE;
		goto out;
	}

	/* compare user state value */
	user_state = user_get_4((int*)user_state_addr);
	if (user_state != compare) {
		err = EAGAIN;
		goto out;
	}

	/* wake waiting thread with EOK */
	REG_OUT0(thr->regs) = EOK;
	thread_wakeup(thr);
	err = EOK;

out:
	sched_unlock(thr->sched);

	return err;
}
