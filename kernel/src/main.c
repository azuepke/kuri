/* SPDX-License-Identifier: MIT */
/*
 * main.c
 *
 * Example OS
 *
 * azuepke, 2017-07-17: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-01-03: adding syscalls
 */

#include <arch.h>
#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <stdio.h>
#include <arch_context.h>
#include <thread.h>
#include <part.h>
#include <sched.h>
#include <sys_proto.h>
#include <marron/error.h>
#include <irq.h>
#include <current.h>
#include <sig.h>
#include <string.h>
#include <user.h>
#include <futex.h>
#include <event.h>
#include <lwsync.h>
#include <sync.h>
#include <memrq_types.h>


/* the console lock */
static spin_t the_console_lock = SPIN_INIT;

/* lock console lock */
void console_lock(void)
{
	spin_lock(&the_console_lock);
}

/* unlock console lock */
void console_unlock(void)
{
	spin_unlock(&the_console_lock);
}

/** Idle loop entry point
 *
 * The kernel idle threads comprises a logical thread, but executes in kernel
 * space. The idle thread has the lowest possible priority in the system.
 */
void kernel_idle(void)
{
	while (1) {
		bsp_idle();
	}
}

/* the kernel's main entry point */
__init void kernel_main(void)
{
	unsigned int cpu_id;

	/* initialize BSP at first */
	/* NOTE: we do this on all cores */
	bsp_init();

#ifdef SMP
	cpu_id = bsp_cpu_id();
#else
	cpu_id = 0;
#endif

	/* at first initialize per-CPU data for all CPUs
	 * to make current_cpu_id() works as expected
	 */
	if (cpu_id == 0) {
		for (unsigned int i = 0; i < cpu_num; i++) {
			struct percpu *percpu = percpu_cfg[i].percpu;

			arch_percpu_init(&percpu->arch, i);
			percpu->current_thread = &thread_dyn[i];
			percpu->current_part_cfg = &part_cfg[0];
			percpu->cpu_id = i;
		}
	}
	assert(current_cpu_id() == cpu_id);

	if (cpu_id == 0) {
		printf("*** The Kuri kernel, a fork of the Marron kernel ***\n");
		printf("kernel build ID: %s %s", kernel_buildid, ARCH_BANNER_NAME);
#ifdef SMP
		printf(" SMP %d CPU(s)", bsp_cpu_num);
#else
		printf(" UP");
#endif
#ifndef NDEBUG
		printf(" DEBUG");
#else
				printf(" RELEASE");
#endif
		printf("\n");
		printf("BSP build ID: %s\n", bsp_buildid);
		printf("Timer: %lld ns\n", (unsigned long long)bsp_timer_resolution);
	}

	printf("CPU #%d: kernel stack 0x%p, percpu: 0x%p\n", cpu_id, &cpu_id, arch_percpu());
	assert(arch_percpu() == percpu_cfg[cpu_id].percpu);

	arch_init_exceptions();

	if (cpu_id == 0) {
		irq_init_all();
		event_init_all();
		thread_init_all();
		part_init_all();
		sync_init_all();

		/* initialize scheduler on all CPUs */
		assert(cpu_num == sched_num);
		for (unsigned int i = 0; i < cpu_num; i++) {
			struct thread *thr = &thread_dyn[i];

			/* idle thread starts in RUNNING state */
			thr->prio = -1;
			thr->state = THREAD_STATE_CURRENT;
			thr->sched = sched_per_cpu(i);
			arch_reg_init_idle(thr->regs,
			                   (addr_t)kernel_idle,
			                   (addr_t)percpu_cfg[i].idle_stack_top);

			sched_init(sched_per_cpu(i), thr, i);
		}

#ifdef SMP
		printf("SMP: number of CPUs: %d/%d\n", bsp_cpu_num, cpu_num);
		if (bsp_cpu_num != cpu_num) {
			panic("Number of CPUs mismatch\n");
		}

		/* start all secondary CPUs */
		bsp_cpu_start_secondary();
#endif
	}

#ifdef SMP
	bsp_cpu_up(cpu_id);
#endif

	/* start partitions on the cores */
	for (unsigned int i = 1; i < part_num; i++) {
		const struct part_cfg *cfg = &part_cfg[i];

		if ((cfg->initial_state != PART_STATE_IDLE) &&
		    (cfg->first_cpu == cpu_id)) {
			part_start(cfg);
		}
	}

	/* start execution of first thread */
	console_lock();
	printf("CPU #%d: go!\n", cpu_id);
	console_unlock();
}

/* Kernel IRQ callback. "irq_id" is the interrupt dispatched by the BSP.
 * The kernel should wake waiting threads, for example.
 * When kernel_irq() is called, the interrupt source is masked.
 */
void kernel_irq(unsigned int irq_id)
{
	struct irq *irq;

	assert(irq_id < irq_table_num);
	irq = irq_by_id(irq_id);
	assert(irq != NULL);

	irq_handle(irq);
}

#ifdef SMP
/* Kernel SMP rescheduling callback
 * The kernel shall handle IPIs and, possibly, reschedule.
 */
void kernel_ipi(void)
{
	/* this handler is empty, rescheduling happens on return from interrupt */
}
#endif

/** print error header */
void kernel_print_current(void)
{
	printf("### partition '%s' thread %d ",
	       current_part_cfg()->name,
	       current_thread()->id);
}

/* exception_type_name() */
static const char *exception_type_name(unsigned int type)
{
	switch (type) {
	case EX_TYPE_ILLEGAL:		return "illegal instruction";
	case EX_TYPE_DEBUG:			return "debug exception";
	case EX_TYPE_TRAP:			return "trap or breakpoint instruction";
	case EX_TYPE_ARITHMETIC:	return "arithmetic error / overflow";
	case EX_TYPE_PAGEFAULT:		return "pagefault";
	case EX_TYPE_ALIGN:			return "alignment error";
	case EX_TYPE_BUS:			return "bus error";
	case EX_TYPE_FPU_UNAVAIL:	return "FPU unavailable / disabled";
	case EX_TYPE_FPU_ERROR:		return "FPU error";
	case EX_TYPE_SYSCALL:		return "system call";
	default:					return "???";
	}
}

/** Kernel exception entry point */
void kernel_exception(
	struct regs *regs,
	unsigned long info)
{
	unsigned int type;

	regs->info = info;
	type = EX_TYPE(info);
	assert(type > 0);
	assert(type < SIG_ABORT);

	if (type == EX_TYPE_FPU_UNAVAIL) {
		assert(current_thread()->fpu_state != FPU_STATE_ON);
		if (current_thread()->fpu_state == FPU_STATE_AUTO) {
			/* lazily enable FPU */
			current_thread()->fpu_state = FPU_STATE_ON;
			arch_fpu_enable();
			arch_fpu_restore(regs);
			return;
		}
	}

	// FIXME: debug output to help debugging
	console_lock();
	kernel_print_current();
	printf("%s", exception_type_name(type));
	if ((type == EX_TYPE_PAGEFAULT) ||
	    (type == EX_TYPE_ALIGN) ||
	    (type == EX_TYPE_BUS)) {
		printf(" at 0x%lx", regs->addr);
	}
	if ((type == EX_TYPE_DEBUG) ||
	    (type == EX_TYPE_PAGEFAULT) ||
	    (type == EX_TYPE_ALIGN) ||
	    (type == EX_TYPE_BUS)) {
		printf(" %c%c%c",
		       (info & EX_READ)  ? 'r' : '-',
		       (info & EX_WRITE) ? 'w' : '-',
		       (info & EX_EXEC)  ? 'x' : '-');
	}
	if (type == EX_TYPE_PAGEFAULT) {
		if (info & EX_PERM) {
			printf(" perm");
		}
	}
	printf("\n");
	arch_dump_registers(regs);
	console_unlock();

	sig_exception(current_thread(), type);
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// System calls

void sys_abort(struct regs *regs)
{
	// FIXME: debug output to help debugging
	console_lock();
	kernel_print_current();
	printf("abort called from 0x%lx\n", (ulong_t)REG_IN0(regs));
	console_unlock();

#ifdef AUTOBUILD
	console_lock();
	printf("AUTOBUILD: halt on sys_abort()\n");
	console_unlock();
	bsp_halt(BOARD_POWEROFF);
#endif

	/* clear exception information when sending a software signal */
	regs->info = EX_TYPE_NONE;

	sig_exception(current_thread(), SIG_ABORT);
}

void sys_putc(struct regs *regs)
{
	unsigned char c;
	err_t ret;

	c = (unsigned char)REG_IN0(regs);

	console_lock();
	ret = bsp_putc(c);
	console_unlock();

	REG_OUT0(regs) = ret;
}

void sys_thread_create(struct regs *regs)
{
	unsigned int thr_id;
	struct thread *thr;
	unsigned int cpu;
	int prio;
	addr_t tls_ptr;
	err_t err;

	thr_id = (unsigned int)REG_IN0(regs);
	if (thr_id >= current_part_cfg()->thread_num) {
		/* thread ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	/* bound priority in 0..part_cfg->max_prio */
	/* NOTE: assume priority as unsigned and map negative ones to max */
	prio = (int)REG_IN5(regs);
	if ((prio < 0) || (prio > current_part_cfg()->max_prio)) {
		prio = current_part_cfg()->max_prio;
	}

	cpu = (unsigned int)REG_IN6(regs);
	if ((cpu >= cpu_num) ||
	        (((1UL << cpu) & current_part_cfg()->cpu_mask) == 0)) {
		/* CPU out of bounds or not available to partition */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	tls_ptr = REG_IN4(regs);
	if (tls_ptr == 0) {
		/* Invalid TLS pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((tls_ptr & (alignof(struct sys_tls) - 1)) != 0) {
		/* TLS pointer not properly aligned */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((struct sys_tls *)tls_ptr, sizeof(struct sys_tls),
	                      MEMRQ_READ|MEMRQ_WRITE);
	if (err != EOK) {
		/* access error */
		REG_OUT0(regs) = err;
		return;
	}

	part_lock(current_part_cfg()->part);

	thr = &current_part_cfg()->thread[thr_id];
	assert(thr != NULL);
	if (thr->state != THREAD_STATE_DEAD) {
		/* thread still active */
		REG_OUT0(regs) = ESTATE;
	} else {
		thread_start(thr,
		             REG_IN1(regs), REG_IN2(regs), REG_IN3(regs), tls_ptr,
		             prio, cpu);
		REG_OUT0(regs) = EOK;
	}

	part_unlock(current_part_cfg()->part);
}

void sys_thread_exit(struct regs *regs __unused)
{
	struct thread *thr = current_thread();

	// FIXME: the last exiting thread should put the partition into an error state

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_DEAD, TIMEOUT_INFINITE);
	sched_unlock(thr->sched);
}

void sys_thread_self_syscall(struct regs *regs)
{
	REG_OUT0(regs) = current_thread()->id;
}

void sys_prio_get_syscall(struct regs *regs)
{
	REG_OUT0(regs) = thread_prio_get(current_thread());
}

void sys_prio_set_syscall(struct regs *regs)
{
	REG_OUT0(regs) = thread_prio_change(current_thread(), (int)REG_IN0(regs));
}

void sys_prio_max_syscall(struct regs *regs)
{
	REG_OUT0(regs) = current_part_cfg()->max_prio;
}

void sys_cpu_get_syscall(struct regs *regs)
{
	REG_OUT0(regs) = current_thread()->sched->cpu;
}

void sys_cpu_set(struct regs *regs)
{
	unsigned int cpu;

	cpu = (unsigned int)REG_IN0(regs);
	if ((cpu >= cpu_num) ||
	        (((1UL << cpu) & current_part_cfg()->cpu_mask) == 0)) {
		/* CPU out of bounds or not available to partition */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	/* update return register before migrating */
	REG_OUT0(regs) = EOK;
	thread_cpu_migrate(current_thread(), cpu);
}

void sys_cpu_mask(struct regs *regs)
{
	REG_OUT0(regs) = current_part_cfg()->cpu_mask;
}

void sys_fpu_state_get(struct regs *regs)
{
	REG_OUT0(regs) = current_thread()->fpu_state;
}

void sys_fpu_state_set(struct regs *regs)
{
	unsigned int new_fpu_state;
	struct thread *thr;

	new_fpu_state = (unsigned int)REG_IN0(regs);
	if ((new_fpu_state != FPU_STATE_OFF) &&
	    (new_fpu_state != FPU_STATE_AUTO) &&
	    (new_fpu_state != FPU_STATE_ON)) {
		/* invalid FPU state */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	REG_OUT0(regs) = EOK;

	thr = current_thread();

	if (new_fpu_state != thr->fpu_state) {
		/* the FPU state changes */
		/* make sure to disable the FPU if enabled */
		if (thr->fpu_state == FPU_STATE_ON) {
			arch_fpu_save(regs);
			arch_fpu_disable();
		}
		thr->fpu_state = new_fpu_state;
		if (new_fpu_state == FPU_STATE_ON) {
			/* FPU was disabled before */
			arch_fpu_enable();
			arch_fpu_restore(regs);
		}
	}
}

void sys_preempt(struct regs *regs __unused)
{
	thread_preempt(current_thread());
}

void sys_yield(struct regs *regs __unused)
{
	thread_yield(current_thread());
}

void sys_time_get(struct regs *regs)
{
	REG_OUT0_64(regs) = bsp_timer_get_time();
}

void sys_time_resolution(struct regs *regs)
{
	REG_OUT0_64(regs) = bsp_timer_resolution;
}

void sys_sleep(struct regs *regs)
{
	struct thread *thr = current_thread();

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_SLEEP, (sys_time_t)REG_IN0_64(regs));
	sched_unlock(thr->sched);

	/* set error code to ETIMEOUT */
	REG_OUT0(regs) = EOK;
}

void sys_thread_suspend(struct regs *regs)
{
	REG_OUT0(regs) = thread_suspend(current_thread());
}

void sys_thread_resume(struct regs *regs)
{
	unsigned int thr_id;
	struct thread *thr;

	thr_id = (unsigned int)REG_IN0(regs);
	if (thr_id >= current_part_cfg()->thread_num) {
		/* thread ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	thr = &current_part_cfg()->thread[thr_id];
	assert(thr != NULL);

	REG_OUT0(regs) = thread_resume(thr);
}

void sys_part_self(struct regs *regs)
{
	REG_OUT0(regs) = current_part_cfg()->id;
}

void sys_part_state(struct regs *regs)
{
	REG_OUT0(regs) = current_part_cfg()->part->state;
}

void sys_part_shutdown(struct regs *regs __unused)
{
	part_stop(current_part_cfg());
}

void sys_part_restart(struct regs *regs __unused)
{
	part_restart(current_part_cfg());
}

void sys_part_state_other(struct regs *regs)
{
	unsigned int part_id;
	struct part *part;

	if ((current_part_cfg()->perm & PART_PERM_PART_OTHER) == 0) {
		REG_OUT0(regs) = EPERM;
		return;
	}

	part_id = (unsigned int)REG_IN0(regs);
	if ((part_id < 1) || (part_id >= part_num)) {
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	part = &part_dyn[part_id];
	assert(part != NULL);

	REG_OUT0(regs) = EOK;
	REG_OUT1(regs) = part->state;
}

void sys_part_shutdown_other(struct regs *regs)
{
	unsigned int part_id;
	const struct part_cfg *cfg;

	if ((current_part_cfg()->perm & PART_PERM_PART_OTHER) == 0) {
		REG_OUT0(regs) = EPERM;
		return;
	}

	part_id = (unsigned int)REG_IN0(regs);
	if ((part_id < 1) || (part_id >= part_num)) {
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	cfg = &part_cfg[part_id];
	assert(cfg != NULL);

	/* we must set the error code before we shut down (probably) ourself */
	REG_OUT0(regs) = EOK;

	part_stop(cfg);
}

void sys_part_restart_other(struct regs *regs)
{
	unsigned int part_id;
	const struct part_cfg *cfg;

	if ((current_part_cfg()->perm & PART_PERM_PART_OTHER) == 0) {
		REG_OUT0(regs) = EPERM;
		return;
	}

	part_id = (unsigned int)REG_IN0(regs);
	if ((part_id < 1) || (part_id >= part_num)) {
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	cfg = &part_cfg[part_id];
	assert(cfg != NULL);

	/* we must set the error code before we shut down (probably) ourself */
	REG_OUT0(regs) = EOK;

	part_restart(cfg);
}

void sys_bsp_shutdown(struct regs *regs)
{
	if ((current_part_cfg()->perm & PART_PERM_SHUTDOWN) == 0) {
		REG_OUT0(regs) = EPERM;
		return;
	}

	switch ((int)REG_IN0(regs)) {
	case 0:
		bsp_halt(BOARD_RESET);
		unreachable();

	case 1:
		bsp_halt(BOARD_HALT);
		unreachable();

	case 2:
		bsp_halt(BOARD_POWEROFF);
		unreachable();

	default:
		REG_OUT0(regs) = EINVAL;
		break;
	}
}

void sys_bsp_name(struct regs *regs)
{
	addr_t name_user;
	size_t size_user;
	size_t size;
	err_t err;

	name_user = (addr_t)REG_IN0(regs);
	size_user = (addr_t)REG_IN1(regs);

	size = strlen(bsp_name) + 1;
	if (size > size_user) {
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	err = user_copy_out((void *)name_user, bsp_name, size);

	REG_OUT0(regs) = err;
}

void sys_irq_enable(struct regs *regs)
{
	unsigned int irq_id;
	irq_mode_t irq_mode;
	err_t err;
	struct irq *irq;

	irq_id = (unsigned int)REG_IN0(regs);
	if (irq_id >= irq_table_num) {
		/* IRQ out of bounds */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	irq = irq_by_id(irq_id);
	if ((irq == NULL) || (irq->part_cfg != current_part_cfg())) {
		/* IRQ not available to partition */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	/* try to enable IRQ */
	irq_mode = (irq_mode_t)REG_IN1(regs);
	err = irq_enable(irq, irq_mode);
	REG_OUT0(regs) = err;
}

void sys_irq_disable(struct regs *regs)
{
	unsigned int irq_id;
	struct irq *irq;
	err_t err;

	irq_id = (unsigned int)REG_IN0(regs);
	if (irq_id >= irq_table_num) {
		/* IRQ out of bounds */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	irq = irq_by_id(irq_id);
	if ((irq == NULL) || (irq->part_cfg != current_part_cfg())) {
		/* IRQ not available to partition */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	/* disable IRQ */
	err = irq_disable(irq);
	REG_OUT0(regs) = err;
}

void sys_irq_wait(struct regs *regs)
{
	unsigned int irq_id;
	unsigned int flags;
	sys_timeout_t timeout;
	struct irq *irq;
	err_t err;

	irq_id = (unsigned int)REG_IN0(regs);
	if (irq_id >= irq_table_num) {
		/* IRQ out of bounds */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	irq = irq_by_id(irq_id);
	if ((irq == NULL) || (irq->part_cfg != current_part_cfg())) {
		/* IRQ not available to partition */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	flags = (unsigned int)REG_IN1(regs);
	if (flags != 0) {
		/* invalid flags -- must be zero */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	timeout = (sys_timeout_t)REG_IN2_64(regs);

	err = irq_wait(irq, timeout);
	REG_OUT0(regs) = err;
}

void sys_cache(struct regs *regs)
{
	cache_op_t op;
	addr_t start;
	size_t size;
	addr_t alias;
	err_t err;

	op = (cache_op_t)REG_IN0(regs);
	start = (addr_t)REG_IN1(regs);
	size = (size_t)REG_IN2(regs);
	alias = (addr_t)REG_IN3(regs);

	// FIXME: check addresses and op!
	err = bsp_cache(op, start, size, alias);

	REG_OUT0(regs) = err;
}

void sys_sig_return(struct regs *regs)
{
	addr_t user_frame;
	uint32_t sig_mask;

	user_frame = (addr_t)REG_IN0(regs);
	sig_mask = (uint32_t)REG_IN1(regs);

	sig_return(current_thread(), user_frame, sig_mask);

	/* at kernel exit, the kernel checks for new pending signals */
}

void sys_sig_register(struct regs *regs)
{
	uint32_t sig_mask;
	unsigned int sig;
	addr_t handler;
	unsigned int flags;

	sig = (unsigned int)REG_IN0(regs);
	if ((sig == 0) || (sig >= NUM_SIGS)) {
		/* invalid signal ID */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	handler = (addr_t)REG_IN1(regs);
	sig_mask = (uint32_t)REG_IN2(regs);

	flags = (unsigned int)REG_IN3(regs);
	if (flags != 0) {
		/* invalid flags -- must be zero */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	sig_register(current_part_cfg()->part, sig, handler, sig_mask);

	REG_OUT0(regs) = EOK;
}

void sys_sig_mask(struct regs *regs)
{
	uint32_t clear_mask;
	uint32_t set_mask;
	uint32_t previous_mask;

	clear_mask = (uint32_t)REG_IN0(regs);
	set_mask = (uint32_t)REG_IN1(regs);

	previous_mask = sig_mask(current_thread(), clear_mask, set_mask);

	REG_OUT0(regs) = previous_mask;

	/* at kernel exit, the kernel checks for new pending signals */
}

void sys_sig_send(struct regs *regs)
{
	unsigned int thr_id;
	struct thread *thr;
	unsigned int sig;

	thr_id = (unsigned int)REG_IN0(regs);
	if (thr_id >= current_part_cfg()->thread_num) {
		/* thread ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}

	thr = &current_part_cfg()->thread[thr_id];
	assert(thr != NULL);
	if (thr->state == THREAD_STATE_DEAD) {
		/* thread is dead */
		REG_OUT0(regs) = ESTATE;
		return;
	}

	sig = (unsigned int)REG_IN1(regs);
	if (sig == 0) {
		/* signal 0 just performs error checks */
		REG_OUT0(regs) = EOK;
		return;
	}
	if (sig >= NUM_SIGS) {
		/* invalid signal ID */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if (SIG_TO_MASK(sig) & SIG_MASK_EXCEPTION) {
		/* signal refers to an exception */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	/* we must set the return code before sending (probably us) the signal */
	REG_OUT0(regs) = EOK;
	sig_send(thr, sig);

	/* at kernel exit, the kernel checks for new pending signals */
}

void sys_futex_wait(struct regs *regs)
{
	addr_t futex_addr;
	int compare;
	sys_timeout_t timeout;
	err_t err;

	futex_addr = (addr_t)REG_IN0(regs);
	if (futex_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((futex_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)futex_addr, sizeof(int), MEMRQ_READ);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	compare = (int)REG_IN1(regs);
	timeout = (sys_timeout_t)REG_IN2_64(regs);

	part_lock(current_part_cfg()->part);
	err = futex_wait(futex_addr, compare, timeout);
	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_futex_wake(struct regs *regs)
{
	addr_t futex_addr;
	unsigned int count;
	err_t err;

	futex_addr = (addr_t)REG_IN0(regs);
	if (futex_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((futex_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)futex_addr, sizeof(int), MEMRQ_READ);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	count = (unsigned int)REG_IN1(regs);

	part_lock(current_part_cfg()->part);
	err = futex_wake(futex_addr, count);
	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_futex_requeue(struct regs *regs)
{
	addr_t futex_addr;
	unsigned int count;
	int compare;
	addr_t futex2_addr;
	unsigned int count2;
	err_t err;

	futex_addr = (addr_t)REG_IN0(regs);
	if (futex_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((futex_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)futex_addr, sizeof(int), MEMRQ_READ);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	count = (unsigned int)REG_IN1(regs);
	compare = (int)REG_IN2(regs);
	futex2_addr = (addr_t)REG_IN3(regs);
	if (futex2_addr != 0) {
		if ((futex2_addr & (sizeof(int) - 1)) != 0) {
			/* bad alignment */
			REG_OUT0(regs) = EINVAL;
			return;
		}
		err = user_check_range((int *)futex2_addr, sizeof(int), MEMRQ_READ|MEMRQ_WRITE);
		if (err != EOK) {
			/* not accessible */
			REG_OUT0(regs) = err;
			return;
		}
		if (futex_addr == futex2_addr) {
			/* same futex */
			REG_OUT0(regs) = EINVAL;
			return;
		}
	}
	count2 = (unsigned int)REG_IN4(regs);
	if ((count2 > 0) && (futex2_addr == 0)) {
		/* second futex is null pointer, but we have threads to wake up */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	part_lock(current_part_cfg()->part);
	err = futex_requeue(futex_addr, count, compare, futex2_addr, count2);
	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_futex_lock(struct regs *regs)
{
	addr_t futex_addr;
	sys_timeout_t timeout;
	unsigned int flags;
	err_t err;

	futex_addr = (addr_t)REG_IN0(regs);
	if (futex_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((futex_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)futex_addr, sizeof(int), MEMRQ_READ|MEMRQ_WRITE);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	flags = (unsigned int)REG_IN1(regs);
	timeout = (sys_timeout_t)REG_IN2_64(regs);

	part_lock(current_part_cfg()->part);
	err = futex_lock(futex_addr, flags, timeout);
	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_futex_unlock(struct regs *regs)
{
	unsigned int flags;
	addr_t futex_addr;
	err_t err;

	futex_addr = (addr_t)REG_IN0(regs);
	if (futex_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((futex_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)futex_addr, sizeof(int), MEMRQ_READ|MEMRQ_WRITE);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	flags = (unsigned int)REG_IN1(regs);

	part_lock(current_part_cfg()->part);
	err = futex_unlock(futex_addr, flags);
	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}


void sys_event_wait(struct regs *regs)
{
	struct event_wait *event;
	unsigned int id;
	unsigned int flags;
	sys_timeout_t timeout;
	err_t err;

	id = (unsigned int)REG_IN0(regs);
	if (id >= current_part_cfg()->event_wait_num) {
		/* event wait ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	event = &current_part_cfg()->event_wait[id];

	flags = (unsigned int)REG_IN1(regs);
	if (flags != 0) {
		/* invalid flags -- must be zero */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	timeout = (sys_timeout_t)REG_IN2_64(regs);

	err = event_wait(event, timeout);
	REG_OUT0(regs) = err;
}

void sys_event_send(struct regs *regs)
{
	const struct event_send_cfg *event_cfg;
	unsigned int id;

	id = (unsigned int)REG_IN0(regs);
	if (id >= current_part_cfg()->event_send_num) {
		/* event send ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	event_cfg = &current_part_cfg()->event_send_cfg[id];

	event_send(event_cfg);
	REG_OUT0(regs) = EOK;
}

void sys_lwsync_wait(struct regs *regs)
{
	addr_t user_state_addr;
	int compare;
	sys_timeout_t timeout;
	int wait_prio;
	err_t err;

	user_state_addr = (addr_t)REG_IN0(regs);
	if (user_state_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((user_state_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)user_state_addr, sizeof(int), MEMRQ_READ);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	compare = (int)REG_IN1(regs);
	timeout = (sys_timeout_t)REG_IN2_64(regs);

	/* bound priority in 0..part_cfg->max_prio */
	/* NOTE: assume priority as unsigned and map negative ones to max */
	wait_prio = (int)REG_IN4(regs);
	if ((wait_prio < 0) || (wait_prio > current_part_cfg()->max_prio)) {
		wait_prio = current_part_cfg()->max_prio;
	}

	err = lwsync_wait(user_state_addr, compare, timeout, wait_prio);

	REG_OUT0(regs) = err;
}

void sys_lwsync_wake(struct regs *regs)
{
	unsigned int thr_id;
	struct thread *thr;
	addr_t user_state_addr;
	int compare;
	int new_prio;
	err_t err;

	thr_id = (unsigned int)REG_IN0(regs);
	if (thr_id >= current_part_cfg()->thread_num) {
		/* thread ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	thr = &current_part_cfg()->thread[thr_id];
	assert(thr != NULL);

	user_state_addr = (addr_t)REG_IN1(regs);
	if (user_state_addr == 0) {
		/* null pointer */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	if ((user_state_addr & (sizeof(int) - 1)) != 0) {
		/* bad alignment */
		REG_OUT0(regs) = EINVAL;
		return;
	}
	err = user_check_range((int *)user_state_addr, sizeof(int), MEMRQ_READ);
	if (err != EOK) {
		/* not accessible */
		REG_OUT0(regs) = err;
		return;
	}
	compare = (int)REG_IN2(regs);
	new_prio = (int)REG_IN3(regs);

	err = lwsync_wake(thr, user_state_addr, compare);
	REG_OUT0(regs) = err;

	thread_prio_change(current_thread(), new_prio);
}

void sys_mutex_init(struct regs *regs)
{
	unsigned int mutex_id;
	unsigned int flags;
	struct sync *mutex;

	mutex_id = (unsigned int)REG_IN0(regs);
	if (mutex_id >= current_part_cfg()->sync_num) {
		/* mutex ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	mutex = &current_part_cfg()->sync[mutex_id];
	assert(mutex != NULL);

	flags = (unsigned int)REG_IN1(regs);
	if (flags != 0) {
		/* flags must be 0 */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	part_lock(current_part_cfg()->part);

	if (mutex->type == SYNC_TYPE_NONE) {
		mutex->type = SYNC_TYPE_MUTEX;
		REG_OUT0(regs) = EOK;
	} else if (mutex->type == SYNC_TYPE_MUTEX) {
		/* reset statistics */
		mutex->stat_lu = 0;	// STATISTICS
		mutex->stat_lc = 0;	// STATISTICS
		mutex->stat_uu = 0;	// STATISTICS
		mutex->stat_uc = 0;	// STATISTICS

		/* sync object already typed */
		REG_OUT0(regs) = ESTATE;
	} else {
		/* sync object already typed */
		REG_OUT0(regs) = ESTATE;
	}

	part_unlock(current_part_cfg()->part);
}

void sys_mutex_lock(struct regs *regs)
{
	unsigned int mutex_id;
	sys_timeout_t timeout;
	unsigned int flags;
	struct sync *mutex;
	err_t err;

	mutex_id = (unsigned int)REG_IN0(regs);
	if (mutex_id >= current_part_cfg()->sync_num) {
		/* mutex ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	mutex = &current_part_cfg()->sync[mutex_id];
	assert(mutex != NULL);

	flags = (unsigned int)REG_IN1(regs);
	if (flags != 0) {
		/* flags must be 0 */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	timeout = (sys_timeout_t)REG_IN2_64(regs);

	part_lock(current_part_cfg()->part);

	err = sync_mutex_lock(mutex, timeout);

	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_mutex_trylock(struct regs *regs)
{
	unsigned int mutex_id;
	unsigned int flags;
	struct sync *mutex;
	err_t err;

	mutex_id = (unsigned int)REG_IN0(regs);
	if (mutex_id >= current_part_cfg()->sync_num) {
		/* mutex ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	mutex = &current_part_cfg()->sync[mutex_id];
	assert(mutex != NULL);

	flags = (unsigned int)REG_IN1(regs);
	if (flags != 0) {
		/* flags must be 0 */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	part_lock(current_part_cfg()->part);

	err = sync_mutex_trylock(mutex);

	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_mutex_unlock(struct regs *regs)
{
	unsigned int mutex_id;
	struct sync *mutex;
	err_t err;

	mutex_id = (unsigned int)REG_IN0(regs);
	if (mutex_id >= current_part_cfg()->sync_num) {
		/* mutex ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	mutex = &current_part_cfg()->sync[mutex_id];
	assert(mutex != NULL);

	part_lock(current_part_cfg()->part);

	err = sync_mutex_unlock(mutex);

	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_mutex_stat(struct regs *regs)
{
	unsigned int mutex_id;
	struct sync *mutex;
	uint32_t *stat_lu;
	uint32_t *stat_lc;
	uint32_t *stat_uu;
	uint32_t *stat_uc;
	err_t err;

	mutex_id = (unsigned int)REG_IN0(regs);
	if (mutex_id >= current_part_cfg()->sync_num) {
		/* mutex ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	mutex = &current_part_cfg()->sync[mutex_id];
	assert(mutex != NULL);

	stat_lu = (uint32_t *)REG_IN1(regs);
	stat_lc = (uint32_t *)REG_IN2(regs);
	stat_uu = (uint32_t *)REG_IN3(regs);
	stat_uc = (uint32_t *)REG_IN4(regs);

	part_lock(current_part_cfg()->part);

	err = sync_mutex_stat(mutex, stat_lu, stat_lc, stat_uu, stat_uc);

	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_cond_init(struct regs *regs)
{
	unsigned int cond_id;
	unsigned int flags;
	struct sync *cond;

	cond_id = (unsigned int)REG_IN0(regs);
	if (cond_id >= current_part_cfg()->sync_num) {
		/* cond ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	cond = &current_part_cfg()->sync[cond_id];
	assert(cond != NULL);

	flags = (unsigned int)REG_IN1(regs);
	if (flags != 0) {
		/* flags must be 0 */
		REG_OUT0(regs) = EINVAL;
		return;
	}

	part_lock(current_part_cfg()->part);

	if (cond->type == SYNC_TYPE_NONE) {
		cond->type = SYNC_TYPE_COND;
		REG_OUT0(regs) = EOK;
	} else {
		/* sync object already typed */
		REG_OUT0(regs) = ESTATE;
	}

	part_unlock(current_part_cfg()->part);
}

void sys_cond_wait(struct regs *regs)
{
	sys_timeout_t timeout;
	unsigned int mutex_id;
	unsigned int cond_id;
	struct sync *mutex;
	struct sync *cond;
	err_t err;

	cond_id = (unsigned int)REG_IN0(regs);
	if (cond_id >= current_part_cfg()->sync_num) {
		/* cond ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	cond = &current_part_cfg()->sync[cond_id];
	assert(cond != NULL);

	mutex_id = (unsigned int)REG_IN1(regs);
	if (mutex_id >= current_part_cfg()->sync_num) {
		/* mutex ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	mutex = &current_part_cfg()->sync[mutex_id];
	assert(mutex != NULL);

	timeout = (sys_timeout_t)REG_IN2_64(regs);

	part_lock(current_part_cfg()->part);

	err = sync_cond_wait(cond, mutex, timeout);

	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_cond_wake(struct regs *regs)
{
	unsigned int cond_id;
	int notify_all;
	struct sync *cond;
	err_t err;

	cond_id = (unsigned int)REG_IN0(regs);
	if (cond_id >= current_part_cfg()->sync_num) {
		/* cond ID out of limits */
		REG_OUT0(regs) = ELIMIT;
		return;
	}
	cond = &current_part_cfg()->sync[cond_id];
	assert(cond != NULL);

	notify_all = (int)REG_IN1(regs);

	part_lock(current_part_cfg()->part);

	err = sync_cond_wake(cond, (notify_all != 0));

	part_unlock(current_part_cfg()->part);

	REG_OUT0(regs) = err;
}

void sys_ni_syscall(struct regs *regs)
{
	REG_OUT0(regs) = ENOSYS;
}
