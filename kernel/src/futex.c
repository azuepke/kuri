/* SPDX-License-Identifier: MIT */
/*
 * futex.c
 *
 * Futex implementation
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2018-04-19: simplified implementation
 * azuepke, 2020-02-02: lock and unlock primitives
 */

#include <marron/error.h>
#include <futex.h>
#include <thread.h>
#include <current.h>
#include <user.h>
#include <sched.h>
#include <part.h>
#include <assert.h>


/** get thread object from futex_waitql node */
#define THR_FROM_FUTEX_WAITQ(t) list_entry((t), struct thread, futex_waitql)


/** Block current thread on user space locking object */
err_t futex_wait(
	addr_t futex_addr,
	int compare,
	sys_timeout_t timeout)
{
	struct thread *thr;
	struct part *part;
	int futex_value;

	/* compare futex value */
	futex_value = user_get_4((int*)futex_addr);
	if (futex_value != compare) {
		return EAGAIN;
	}

	/* enqueue the current thread on the futex wait queue in priority order */
	thr = current_thread();
	thr->futex_addr = futex_addr;

	thr->prio = thread_prio_get(thr);

	part = thr->part_cfg->part;
	list_add_sorted(&part->futex_waitqh, &thr->futex_waitql,
	                THR_FROM_FUTEX_WAITQ(__ITER__)->prio < thr->prio);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_FUTEX, timeout);
	sched_unlock(thr->sched);

	/* set error code to ETIMEOUT in case the timeout triggers */
	return ETIMEOUT;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
void futex_wait_cancel(
	struct thread *thr)
{
	struct part *part;

	assert(thr != NULL);

	part = thr->part_cfg->part;

	/* remove thread from wait queue */
	part_lock(part);
	list_del(&thr->futex_waitql);
	part_unlock(part);
}

/** Wake threads waiting on user space locking object */
err_t futex_wake(
	addr_t futex_addr,
	unsigned int count)
{
	struct thread *thr;
	struct part *part;
	list_t *node, *tmp;

	part = current_part_cfg()->part;

	if (count > 0) {
		list_for_each_safe(&part->futex_waitqh, node, tmp) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->futex_addr == futex_addr) {
				/* remove thread from wait queue */
				list_del(&thr->futex_waitql);

				/* wake waiting thread with EOK */
				REG_OUT0(thr->regs) = EOK;
				sched_lock(thr->sched);
				thread_wakeup(thr);
				sched_unlock(thr->sched);

				count--;
				if (count == 0) {
					break;
				}
			}
		}
	}

	return EOK;
}

/** Wake and/or requeue threads waiting on user space locking object */
err_t futex_requeue(
	addr_t futex_addr,
	unsigned int count,
	int compare,
	addr_t futex2_addr,
	unsigned int count2)
{
	struct thread *thr;
	struct part *part;
	int futex_value;
	int futex2_value;
	unsigned int requeued;
	list_t *node, *tmp;
	list_t tmp_head;

	/* compare futex value */
	futex_value = user_get_4((int*)futex_addr);
	if (futex_value != compare) {
		return EAGAIN;
	}

	part = current_part_cfg()->part;

	/* wake up count threads */
	if (count > 0) {
		list_for_each_safe(&part->futex_waitqh, node, tmp) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->futex_addr == futex_addr) {
				/* remove thread from wait queue */
				list_del(&thr->futex_waitql);

				/* wake waiting thread with EOK */
				REG_OUT0(thr->regs) = EOK;
				sched_lock(thr->sched);
				thread_wakeup(thr);
				sched_unlock(thr->sched);

				count--;
				if (count == 0) {
					break;
				}
			}
		}
	}

	/* requeue count2 threads */
	if (count2 > 0) {
		/* walk wait queue again and remember threads to requeue in tmp_head */
		list_head_init(&tmp_head);
		requeued = 0;

		list_for_each_safe(&part->futex_waitqh, node, tmp) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->futex_addr == futex_addr) {
				/* move thread from wait queue to tmp queue */
				list_del(&thr->futex_waitql);
				list_add_last(&tmp_head, &thr->futex_waitql);
				requeued++;

				count2--;
				if (count2 == 0) {
					break;
				}
			}
		}

		list_for_each_remove_first(&tmp_head, node) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			/* release any timeout, but let thread continue to wait */
			thread_timeout_release(thr);

			thr->futex_addr = futex2_addr;

			list_add_sorted(&part->futex_waitqh, &thr->futex_waitql,
			                THR_FROM_FUTEX_WAITQ(__ITER__)->prio < thr->prio);
		}

		/* set waiters bit in mutex futex */
		if (requeued > 0) {
			futex2_value = user_get_4((int*)futex2_addr);
			futex2_value |= SYS_FUTEX_WAITERS;
			user_put_4((int*)futex2_addr, futex2_value);
		}
	}

	return EOK;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/* Linux-like futex API -- FUTEX_LOCK_PI */
err_t futex_lock(
	addr_t futex_addr,
	unsigned int flags,
	sys_timeout_t timeout)
{
	struct thread *thr;
	struct part *part;
	unsigned int futex_value;
	unsigned int new;

	/* compare futex value */
retry:
	futex_value = user_get_4((int*)futex_addr);

	/* fast path -- unlocked mutex */
	if (futex_value == 0) {
		new = current_thread()->id | SYS_FUTEX_LOCKED;
		if (user_cas((unsigned int*)futex_addr, futex_value, new) == 0) {
			goto retry;
		}
		/* acquire barrier w.r.t. concurrent sys_futex_unlock() operations */
		atomic_acquire_barrier();
		/* STATISTICS: lock fast path */
		if (flags & 0x1) {
			return 0x100;
		}
		return EOK;
	}

	/* set WAITERS bit */
	if ((futex_value & SYS_FUTEX_WAITERS) == 0) {
		new = futex_value | SYS_FUTEX_WAITERS;
		if (user_cas((unsigned int*)futex_addr, futex_value, new) == 0) {
			goto retry;
		}
	}

	/* slow path */
	/* enqueue the current thread on the futex wait queue in priority order */
	thr = current_thread();
	thr->futex_addr = futex_addr;

	thr->prio = thread_prio_get(thr);

	part = thr->part_cfg->part;
	list_add_sorted(&part->futex_waitqh, &thr->futex_waitql,
	                THR_FROM_FUTEX_WAITQ(__ITER__)->prio < thr->prio);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_FUTEX, timeout);
	sched_unlock(thr->sched);

	/* set error code to ETIMEOUT in case the timeout triggers */
	return ETIMEOUT;
}

/* Linux-like futex API -- FUTEX_UNLOCK_PI */
err_t futex_unlock(
	addr_t futex_addr,
	unsigned int flags)
{
	struct thread *thr;
	struct part *part;
	list_t *node, *tmp;
	unsigned int futex_value;
	unsigned int tid;
	bool_t woken;

	tid = current_thread()->id | SYS_FUTEX_LOCKED;

	/* compare futex value */
	futex_value = user_get_4((int*)futex_addr);
	if ((futex_value | SYS_FUTEX_WAITERS) != (tid | SYS_FUTEX_WAITERS)) {
		return ESTATE;
	}

	/* slow path */
	part = current_part_cfg()->part;
	futex_value = 0;
	woken = false;
	list_for_each_safe(&part->futex_waitqh, node, tmp) {
		thr = THR_FROM_FUTEX_WAITQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_FUTEX);

		if (thr->futex_addr == futex_addr) {
			if (woken == false) {
				woken = true;

				/* remove thread from wait queue */
				list_del(&thr->futex_waitql);

				/* wake waiting thread with EOK */
				REG_OUT0(thr->regs) = EOK;
				sched_lock(thr->sched);
				thread_wakeup(thr);
				sched_unlock(thr->sched);

				futex_value = thr->id | SYS_FUTEX_LOCKED | SYS_FUTEX_WAITERS;
			} else {
				// more waiters
				futex_value |= SYS_FUTEX_WAITERS;
				break;
			}
		}
	}

	/* release barrier w.r.t. concurrent sys_futex_lock() operations */
	atomic_release_barrier();
	user_put_4((int*)futex_addr, futex_value);

	if ((flags & 0x1) && (futex_value == 0)) {
		/* STATISTICS: unlock fast path (spurious WAITERS bit set) */
		return 0x100;
	}

	return EOK;
}
