/* SPDX-License-Identifier: MIT */
/*
 * user.c
 *
 * User space accessors
 *
 * azuepke, 2018-03-22: initial
 */

#include <marron/error.h>
#include <user.h>
#include <part.h>
#include <memrq_types.h>
#include <current.h>
#include <string.h>


/** check address range in current user space for requested access */
err_t user_check_range(
	const void *user_ptr,
	size_t size,
	unsigned int access)
{
	addr_t addr = (addr_t)user_ptr;
	const struct part_cfg *cfg;
	const struct memrq_cfg *m;
	unsigned int i;

	assert(size > 0);
	assert((access & (MEMRQ_READ | MEMRQ_WRITE | MEMRQ_EXEC)) != 0);

	if (addr + size < addr) {
		/* address space overflow */
		return EINVAL;
	}

	cfg = current_part_cfg();

	for (i = 0; i < cfg->memrq_num; i++) {
		m = &cfg->memrq_cfg[i];
		if ((addr >= m->addr) &&
		    (addr + size <= m->addr + m->size) &&
		    ((access & m->access) == access)) {
			return EOK;
		}
	}

	return EFAULT;
}

/** copy data to or from user space */
err_t user_copy(void *to, const void *from, size_t size)
{
	memcpy(to, from, size);
	return EOK;
}

/** copy data from current user space to kernel space */
err_t user_copy_in(void *to_kernel, const void *from_user, size_t size)
{
	err_t err;

	assert(to_kernel != NULL);
	assert(size > 0);

	err = user_check_range(from_user, size, MEMRQ_READ);
	if (err != EOK) {
		return err;
	}

	return user_copy(to_kernel, from_user, size);
}

/** copy data from kernel space to current user space */
err_t user_copy_out(void *to_user, const void *from_kernel, size_t size)
{
	err_t err;

	assert(from_kernel != NULL);
	assert(size > 0);

	err = user_check_range(to_user, size, MEMRQ_WRITE);
	if (err != EOK) {
		return err;
	}

	return user_copy(to_user, from_kernel, size);
}
