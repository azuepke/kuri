/* SPDX-License-Identifier: MIT */
/*
 * part.c
 *
 * Partitioning
 *
 * azuepke, 2018-01-02: initial
 */

#include <part.h>
#include <thread.h>
#include <compiler.h>
#include <assert.h>
#include <irq.h>
#include <kernel.h>
#include <stdio.h>
#include <current.h>
#include <sync.h>
#include <event.h>


/** initialize all partition objects at boot time */
__init void part_init_all(void)
{
	const struct part_cfg *cfg;
	struct part *part;

	for (unsigned int i = 0; i < part_num; i++) {
		cfg = &part_cfg[i];
		part = &part_dyn[i];

		spin_init(&part->lock);

		part->part_cfg = cfg;
		part->state = PART_STATE_IDLE;

		/* empty futex wait queue */
		list_head_init(&part->futex_waitqh);

		/* clear all signal handlers */
		for (unsigned int sig = 0; sig < NUM_SIGS; sig++) {
			part->sig_handler[sig] = 0;
			part->sig_mask[sig] = 0;
		}

		/* assign IRQs to partition */
		for (unsigned int j = 0; j < cfg->irq_num; j++) {
			irq_assign_part(irq_by_id(cfg->irq_cfg[j].irq_id), cfg);
		}

		/* bind all threads to the partition */
		for (unsigned int j = 0; j < cfg->thread_num; j++) {
			thread_assign_part(&cfg->thread[j], j, cfg);
		}

		/* assign IRQs to partition */
		for (unsigned int j = 0; j < cfg->event_wait_num; j++) {
			event_assign_part(&cfg->event_wait[j], part);
		}
	}
}

/** start a partition and its threads (internal function, called locked) */
static void part_start_internal(void *arg)
{
	const struct part_cfg *cfg;
	struct part *part = arg;

	assert(part != NULL);

	if (part->state == PART_STATE_RUNNING) {
		return;
	}

	assert(part->state == PART_STATE_IDLE);
	part->state = PART_STATE_RUNNING;

	/* clear all signal handlers */
	for (unsigned int sig = 0; sig < NUM_SIGS; sig++) {
		part->sig_handler[sig] = 0;
		part->sig_mask[sig] = 0;
	}

	/* start first thread at the partition entry point */
	cfg = part->part_cfg;
	assert(cfg != NULL);
	thread_start(&cfg->thread[0],
	             cfg->entry, 0, cfg->stack_base + cfg->stack_size, cfg->tls,
	             cfg->max_prio,
	             cfg->first_cpu);
}

/** start a partition and its threads */
void part_start(const struct part_cfg *cfg)
{
	struct part *part;

	assert(cfg != NULL);
	assert(cfg->id != 0);
	part = cfg->part;
	assert(part != NULL);

	part_lock(part);
	part_start_internal(part);
	part_unlock(part);
}

/** stop a partition and its threads (internal function, called locked) */
static void part_stop_internal(void *arg)
{
	const struct part_cfg *cfg;
	struct part *part;

	part = arg;

	assert(part != NULL);
	assert(part->part_cfg->id != 0);

	if (part->state == PART_STATE_IDLE) {
		return;
	}

	assert(part->state == PART_STATE_RUNNING);
	part->state = PART_STATE_IDLE;

	cfg = part->part_cfg;

	/* kill all threads */
	for (unsigned int i = 0; i < cfg->thread_num; i++) {
		thread_kill(&cfg->thread[i]);
	}

	/* kill all interrupts */
	for (unsigned int i = 0; i < cfg->irq_num; i++) {
		irq_shutdown(irq_by_id(cfg->irq_cfg->irq_id));
	}

	/* reset all sync objects */
	for (unsigned int i = 0; i < cfg->sync_num; i++) {
		sync_reset(&cfg->sync[i]);
	}
}

/** stop a partition and its threads */
void part_stop(const struct part_cfg *cfg)
{
	struct part *part;

	assert(cfg != NULL);
	assert(cfg->id != 0);
	part = cfg->part;
	assert(part != NULL);

	part_lock(part);
	part_stop_internal(part);
	part_unlock(part);
}

/** restart partition */
void part_restart(const struct part_cfg *cfg)
{
	struct part *part;

	assert(cfg != NULL);
	assert(cfg->id != 0);
	part = cfg->part;
	assert(part != NULL);

	part_lock(part);
	if (part->state == PART_STATE_RUNNING) {
		part_stop_internal(part);
	}
	assert(part->state == PART_STATE_IDLE);
	part_start_internal(part);
	assert(part->state == PART_STATE_RUNNING);
	part_unlock(part);
}

/** raise a partition error (always to currently running partition) */
void part_error(struct part *part, unsigned int sig)
{
	assert(part != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	// FIXME: debug output to help debugging
	kernel_print_current();
	printf("partition error %d, partition stopped\n", sig);

	part_lock(part);
	part_stop_internal(part);
	part_unlock(part);
}
