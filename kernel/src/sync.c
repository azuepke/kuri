/* SPDX-License-Identifier: MIT */
/*
 * sync.c
 *
 * Mutex and condition variable implementation
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/error.h>
#include <sync.h>
#include <thread.h>
#include <current.h>
#include <user.h>
#include <sched.h>
#include <part.h>
#include <assert.h>

/** get thread object from sync_waitql node */
#define THR_FROM_SYNC_WAITQ(t) list_entry((t), struct thread, sync_waitql)


/** initialize all sync objects at boot time */
void sync_init_all(void)
{
	struct sync *sync;
	unsigned int i;

	for (i = 0; i < sync_num; i++) {
		sync = &sync_dyn[i];

		sync->owner = NULL;
		list_head_init(&sync->sync_waitqh);
		sync->type = SYNC_TYPE_NONE;
	}
}

/** reset sync object */
void sync_reset(struct sync *sync)
{
	assert(sync != NULL);

	sync->owner = NULL;
	assert(list_is_empty(&sync->sync_waitqh));
	sync->type = SYNC_TYPE_NONE;
	sync->stat_lu = 0;	// STATISTICS
	sync->stat_lc = 0;	// STATISTICS
	sync->stat_uu = 0;	// STATISTICS
	sync->stat_uc = 0;	// STATISTICS
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
void sync_mutex_wait_cancel(
	struct thread *thr)
{
	struct part *part;

	assert(thr != NULL);

	part = thr->part_cfg->part;

	/* remove thread from wait queue */
	part_lock(part);
	list_del(&thr->sync_waitql);
	part_unlock(part);
}

/** Requeue a thread from a condition variable to its mutex
 * returns true if the target thread must be woken up
 */
static bool_t mutex_requeue(
	struct thread *thr,
	struct sync *mutex)
{
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_WAIT_COND);
	assert(mutex != NULL);
	assert(mutex->type == SYNC_TYPE_MUTEX);

	if (mutex->owner == NULL) {
		/* fast path */
		mutex->owner = thr;
		/* thread must be woken up */
		return true;
	}

	/* enqueue the current thread on the sync object wait queue in priority order */
	list_add_sorted(&mutex->sync_waitqh, &thr->sync_waitql,
	                THR_FROM_SYNC_WAITQ(__ITER__)->prio < thr->prio);

	thr->state = THREAD_STATE_WAIT_MUTEX;

	/* keep thread in a blocked state */
	return false;
}


/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 * "requeue" is only set during timeout processing
 * returns true if the target thread must / can be woken up
 */
bool_t sync_cond_wait_cancel(
	struct thread *thr,
	bool_t requeue)
{
	struct part *part;
	bool_t wake_thr;

	assert(thr != NULL);

	part = thr->part_cfg->part;

	part_lock(part);

	/* remove thread from wait queue */
	list_del(&thr->sync_waitql);

	/* relock mutex */
	if (requeue) {
		wake_thr = mutex_requeue(thr, thr->cond_mutex);
	} else {
		/* do not wakeup thread */
		wake_thr = false;
	}

	part_unlock(part);

	return wake_thr;
}

/** lock mutex */
err_t sync_mutex_lock(
	struct sync *mutex,
	sys_timeout_t timeout)
{
	struct thread *thr;

	assert(mutex != NULL);

	if (mutex->type != SYNC_TYPE_MUTEX) {
		return ETYPE;
	}

	thr = current_thread();

	if (mutex->owner == thr) {
		return EBUSY;
	}

	if (mutex->owner == NULL) {
		/* fast path */
		mutex->stat_lu++;	// STATISTICS
		mutex->owner = thr;
		//atomic_acquire_barrier();	// FIXME: not needed, we have an outer lock
		return EOK;
	}

	/* enqueue the current thread on the sync object wait queue in priority order */
	mutex->stat_lc++;	// STATISTICS
	thr->prio = thread_prio_get(thr);
	list_add_sorted(&mutex->sync_waitqh, &thr->sync_waitql,
	                THR_FROM_SYNC_WAITQ(__ITER__)->prio < thr->prio);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_MUTEX, timeout);
	sched_unlock(thr->sched);

	/* set error code to ETIMEOUT in case the timeout triggers */
	return ETIMEOUT;
}

/** try to lock mutex */
err_t sync_mutex_trylock(
	struct sync *mutex)
{
	struct thread *thr;

	assert(mutex != NULL);

	if (mutex->type != SYNC_TYPE_MUTEX) {
		return ETYPE;
	}

	thr = current_thread();

	if (mutex->owner != NULL) {
		return EBUSY;
	}

	/* fast path */
	mutex->stat_lu++;	// STATISTICS
	mutex->owner = thr;
	//atomic_acquire_barrier();	// FIXME: not needed, we have an outer lock
	return EOK;
}

/** internal unlock mutex */
static void mutex_unlock(
	struct sync *mutex)
{
	struct thread *thr;
	list_t *node;

	assert(mutex != NULL);
	assert(mutex->type == SYNC_TYPE_MUTEX);
	assert(mutex->owner == current_thread());

	//atomic_release_barrier();	// FIXME: not needed, we have an outer lock

	node = list_first(&mutex->sync_waitqh);
	if (node != NULL) {
		mutex->stat_uc++;	// STATISTICS
		thr = THR_FROM_SYNC_WAITQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_MUTEX);

		/* remove thread from wait queue */
		list_del(&thr->sync_waitql);

		/* wake waiting thread with EOK */
		REG_OUT0(thr->regs) = EOK;
		sched_lock(thr->sched);
		thread_wakeup(thr);
		sched_unlock(thr->sched);
	} else {
		mutex->stat_uu++;	// STATISTICS
		thr = NULL;
	}

	mutex->owner = thr;
}

/** unlock mutex */
err_t sync_mutex_unlock(
	struct sync *mutex)
{
	assert(mutex != NULL);

	if (mutex->type != SYNC_TYPE_MUTEX) {
		return ETYPE;
	}

	if (mutex->owner != current_thread()) {
		return ESTATE;
	}

	mutex_unlock(mutex);
	return EOK;
}

/** retrieve mutex statistics */
err_t sync_mutex_stat(
	struct sync *mutex,
	uint32_t *stat_lu,
	uint32_t *stat_lc,
	uint32_t *stat_uu,
	uint32_t *stat_uc)
{
	err_t err;

	assert(mutex != NULL);

	if (mutex->type != SYNC_TYPE_MUTEX) {
		return ETYPE;
	}

	err = EOK;
	if (stat_lu != NULL) {
		err |= user_copy_out(stat_lu, &mutex->stat_lu, sizeof(mutex->stat_lu));
	}
	if (stat_lc != NULL) {
		err |= user_copy_out(stat_lc, &mutex->stat_lc, sizeof(mutex->stat_lc));
	}
	if (stat_uu != NULL) {
		err |= user_copy_out(stat_uu, &mutex->stat_uu, sizeof(mutex->stat_uu));
	}
	if (stat_uc != NULL) {
		err |= user_copy_out(stat_uc, &mutex->stat_uc, sizeof(mutex->stat_uc));
	}
	return (err != EOK) ? EFAULT : EOK;
}


/** wait on condition variable */
err_t sync_cond_wait(
	struct sync *cond,
	struct sync *mutex,
	sys_timeout_t timeout)
{
	struct thread *thr;

	assert(cond != NULL);
	assert(mutex != NULL);

	if (cond->type != SYNC_TYPE_COND) {
		return ETYPE;
	}
	if (mutex->type != SYNC_TYPE_MUTEX) {
		return ETYPE;
	}

	thr = current_thread();
	if (mutex->owner != thr) {
		return ESTATE;
	}
	thr->cond_mutex = mutex;
	mutex_unlock(mutex);

	/* enqueue the current thread on the sync object wait queue in priority order */
	thr->prio = thread_prio_get(thr);
	list_add_sorted(&cond->sync_waitqh, &thr->sync_waitql,
	                THR_FROM_SYNC_WAITQ(__ITER__)->prio < thr->prio);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_COND, timeout);
	sched_unlock(thr->sched);

	/* NOTE: any requeuing happens in sync_cond_wait_cancel() */

	/* set error code to ETIMEOUT in case the timeout triggers */
	return ETIMEOUT;
}

/** notify condition variable */
err_t sync_cond_wake(
	struct sync *cond,
	bool_t notify_all)
{
	struct thread *thr;
	bool_t wake_thr;
	list_t *node;

	assert(cond != NULL);
	if (cond->type != SYNC_TYPE_COND) {
		return ETYPE;
	}

	while ((node = list_first(&cond->sync_waitqh))) {
		thr = THR_FROM_SYNC_WAITQ(node);
		assert(thr->state == THREAD_STATE_WAIT_COND);

		/* remove thread from wait queue */
		list_del(&thr->sync_waitql);

		/* release any timeout, but let thread continue to wait */
		thread_timeout_release(thr);

		wake_thr = mutex_requeue(thr, thr->cond_mutex);

		if (wake_thr) {
			/* wake waiting thread with EOK */
			REG_OUT0(thr->regs) = EOK;
			sched_lock(thr->sched);
			thread_wakeup(thr);
			sched_unlock(thr->sched);
		}

		if (!notify_all) {
			break;
		}
	}

	return EOK;
}
