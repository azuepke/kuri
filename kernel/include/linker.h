/* SPDX-License-Identifier: MIT */
/*
 * linker.h
 *
 * Linker specific symbols.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 */

#ifndef __LINKER_H__
#define __LINKER_H__

extern char __text_start[];
extern char __text_end[];
extern char __data_start[];
extern char __data_end[];
extern char __bss_start[];
extern char __bss_end[];
extern char __heap_start[];
extern char __heap_end[];

#endif
