/* SPDX-License-Identifier: MIT */
/*
 * stddef.h
 *
 * Standard C definitions.
 *
 * azuepke, 2013-09-17: initial
 * azuepke, 2017-10-02: imported
 */

#ifndef __STDDEF_H__
#define __STDDEF_H__

#ifndef NULL
#define NULL (void *)0
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#endif
