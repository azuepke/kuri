/* SPDX-License-Identifier: MIT */
/*
 * irq.h
 *
 * IRQ handling.
 *
 * azuepke, 2018-01-13: initial
 */

#ifndef __IRQ_H__
#define __IRQ_H__

#include <irq_types.h>
#include <spin.h>

/* forward declaration */
struct thread;

/* API */
/** get IRQ object for a given IRQ ID */
static inline struct irq *irq_by_id(unsigned int irq_id)
{
	return irq_table[irq_id];
}

/** initialize all IRQ objects at boot time */
void irq_init_all(void);

/** assign IRQ to partition at boot time */
void irq_assign_part(struct irq *irq, const struct part_cfg *cfg);

/** handle an IRQ */
void irq_handle(struct irq *irq);

/** configure interrupt mode and enable interrupt */
err_t irq_enable(struct irq *irq, irq_mode_t irq_mode);

/** disable interrupt */
err_t irq_disable(struct irq *irq);

/** current thread waits for interrupt */
err_t irq_wait(struct irq *irq, sys_timeout_t timeout);

/** Callback for cleanup during wakeup (timeout, thread deletion) */
void irq_wait_cancel(struct thread *thr);

/** shutdown interrupt at partition shutdown */
void irq_shutdown(struct irq *irq);

static inline void irq_lock(struct irq *irq)
{
	spin_lock(&irq->lock);
}

static inline void irq_unlock(struct irq *irq)
{
	spin_unlock(&irq->lock);
}

#define assert_irq_locked(irq) assert(spin_locked(&(irq)->lock))

#endif
