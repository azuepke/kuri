/* SPDX-License-Identifier: MIT */
/*
 * sync_types.h
 *
 * Mutex and condition variable data types.
 *
 * azuepke, 2020-02-02: initial
 */

#ifndef __SYNC_TYPES_H__
#define __SYNC_TYPES_H__

#include <marron/types.h>
#include <stdint.h>
#include <list.h>

/* forward declarations */
struct sync;
struct thread;

#define SYNC_TYPE_NONE 0
#define SYNC_TYPE_MUTEX 1
#define SYNC_TYPE_COND 2

/** sync_cond_wait object */
/* NOTE: SMP: protected by task_lock */
struct sync {
	/** Current mutex owner, if any */
	struct thread *owner;

	/** Wait queue head -- list of threads blocked on a sync object */
	list_t sync_waitqh;

	uint32_t type;

	/** Statistics information */
	uint32_t stat_lu;	// STATISTICS: lock uncontended
	uint32_t stat_lc;	// STATISTICS: lock contended
	uint32_t stat_uu;	// STATISTICS: unlock uncontended
	uint32_t stat_uc;	// STATISTICS: unlock contended
};

extern const uint16_t sync_num;
extern struct sync sync_dyn[];

#endif
