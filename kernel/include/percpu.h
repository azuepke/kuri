/* SPDX-License-Identifier: MIT */
/*
 * percpu.h
 *
 * Per-CPU data.
 *
 * azuepke, 2018-03-05: moved current*() getters from kernel.h
 */

#ifndef __PERCPU_H__
#define __PERCPU_H__

#include <percpu_types.h>
#include <arch_percpu.h>
#include <bsp.h>
#include <thread_types.h>

/* "per-CPU" API */
/** update current thread and current partition in per-CPU data */
static inline void percpu_thread_switch(struct thread *thr)
{
	struct percpu *percpu;

	percpu = arch_percpu();
	percpu->current_thread = thr;
	percpu->current_part_cfg = thr->part_cfg;
}

#endif
