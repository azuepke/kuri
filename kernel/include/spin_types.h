/* SPDX-License-Identifier: MIT */
/*
 * spin_types.h
 *
 * Common spin lock operations for kernel use using a Linux-like interface
 *
 * NOTE: The spinlocks are optimized out in UP builds.
 *
 * azuepke, 2014-08-26: initial
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef __SPIN_TYPES_H__
#define __SPIN_TYPES_H__

#include <arch_spin_types.h>

/** spin lock data type */
typedef struct {
#ifdef SMP
	arch_spin_t arch_lock;
#else
	int unused;
#endif
} spin_t;


/** spin lock static initializer */
#ifdef SMP
#define SPIN_INIT { ARCH_SPIN_INIT }
#else
#define SPIN_INIT { }
#endif

#endif
