/* SPDX-License-Identifier: MIT */
/*
 * event.h
 *
 * Event data types.
 *
 * azuepke, 2018-04-10: initial
 */

#ifndef __EVENT_H__
#define __EVENT_H__

#include <event_types.h>
#include <part_types.h>

/* API */
/** initialize all event objects at boot time */
void event_init_all(void);

/** Assign event to partition */
void event_assign_part(struct event_wait *event_wait, struct part *part);

/** Block current thread on event object */
err_t event_wait(struct event_wait *event_wait, sys_timeout_t timeout);

/** Callback for cleanup during wakeup (timeout, thread deletion) */
void event_wait_cancel(struct thread *thr);

/** Wake a thread waiting on event object, if any */
void event_send(const struct event_send_cfg *event_cfg);

#endif
