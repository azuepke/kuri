/* SPDX-License-Identifier: MIT */
/*
 * sig.h
 *
 * Signal and exception handling
 *
 * azuepke, 2018-03-16: initial
 */

#ifndef __SIG_H__
#define __SIG_H__

#include <marron/types.h>
#include <stdint.h>

/* forward declaration */
struct part;
struct regs;
struct thread;

/** register a signal handler */
void sig_register(
	struct part *part,
	unsigned int sig,
	addr_t handler,
	uint32_t sig_mask);

/** immediately raise an exception to the current thread
 * NOTE: the kernel's exception handler and sys_abort() calls this,
 * and exceptions are never set to "pending"
 */
void sig_exception(
	struct thread *thr,
	unsigned int sig);

/** if the thread has pending signals, send them now
 * NOTE: the scheduler calls this function at kernel exit
 */
struct regs *sig_send_if_pending(struct thread *thr);

/** return from a signal handler */
void sig_return(
	struct thread *thr,
	addr_t user_frame,
	uint32_t sig_mask);

/** change the current thread's signal mask */
uint32_t sig_mask(
	struct thread *thr,
	uint32_t clear_mask,
	uint32_t set_mask);

/** send an asynchronous signal to a thread */
void sig_send(struct thread *thr, unsigned int sig);

#endif
