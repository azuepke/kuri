/* SPDX-License-Identifier: MIT */
/*
 * memrq.h
 *
 * Memory requirement data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __MEMRQ_H__
#define __MEMRQ_H__

#include <memrq_types.h>

#endif
