/* SPDX-License-Identifier: MIT */
/*
 * spin.h
 *
 * Common spin lock operations for kernel use using a Linux-like interface
 *
 * NOTE: The spinlocks are optimized out in UP builds.
 *
 * azuepke, 2014-08-26: initial
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef __SPIN_H__
#define __SPIN_H__

#include <spin_types.h>
#include <arch_spin.h>
#include <compiler.h>
#include <current.h>
#include <assert.h>

/** invalid owner CPU (beyond 256 CPUs) */
#define SPIN_INVALID_OWNER_CPU 0x100

/** spin lock initializer */
static inline void spin_init(spin_t *spin __unused)
{
#ifdef SMP
	arch_spin_init(&spin->arch_lock);
#ifndef NDEBUG
	spin->arch_lock.s.owner_cpu = SPIN_INVALID_OWNER_CPU;
#endif
#endif
}


/** lock spin lock */
static inline void spin_lock(spin_t *spin __unused)
{
#ifdef SMP
	arch_spin_lock(&spin->arch_lock);
#ifndef NDEBUG
	spin->arch_lock.s.owner_cpu = current_cpu_id();
#endif
#endif
}

/** try to lock spin lock, returns TRUE on success */
static inline int spin_trylock(spin_t *spin __unused)
{
#ifdef SMP
	int success;

	success = arch_spin_trylock(&spin->arch_lock);

#ifndef NDEBUG
	if (success) {
		spin->arch_lock.s.owner_cpu = current_cpu_id();
	}
#endif

	return success;
#else
	return 1;
#endif
}

/** unlock spin lock */
static inline void spin_unlock(spin_t *spin __unused)
{
#ifdef SMP
#ifndef NDEBUG
	assert(spin->arch_lock.s.owner_cpu == current_cpu_id());
	spin->arch_lock.s.owner_cpu = SPIN_INVALID_OWNER_CPU;
#endif
	arch_spin_unlock(&spin->arch_lock);
#endif
}

/** test if spin lock is locked (for debugging) */
static inline int spin_locked(spin_t *spin __unused)
{
#ifdef SMP
#ifndef NDEBUG
	return (spin->arch_lock.s.owner_cpu == current_cpu_id());
#else
	/* no better default here */
	return 1;
#endif
#else
	/* no better default on SMP */
	return 1;
#endif
}

#endif
