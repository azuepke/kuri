/* SPDX-License-Identifier: MIT */
/*
 * thread_types.h
 *
 * Thread data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __THREAD_TYPES_H__
#define __THREAD_TYPES_H__

#include <marron/types.h>
#include <list.h>
#include <arch_context.h>
#include <arch.h>
#include <compiler.h>

/* thread states */
#define THREAD_STATE_CURRENT		0	/**< thread is currently scheduled (and not on the ready queue) */
#define THREAD_STATE_READY			1	/**< thread is enqueued on the ready queue */
/* blocking states */
#define THREAD_STATE_WAIT_SLEEP		2	/**< thread is sleeping */
#define THREAD_STATE_WAIT_SUSPEND	3	/**< thread is suspended */
#define THREAD_STATE_WAIT_IRQ		4	/**< thread is waiting for IRQ */
#define THREAD_STATE_WAIT_FUTEX		5	/**< thread is waiting on futex */
#define THREAD_STATE_WAIT_LWSYNC	6	/**< thread is waiting on an event */
#define THREAD_STATE_WAIT_EVENT		7	/**< thread is waiting on an event */
#define THREAD_STATE_WAIT_MUTEX		8	/**< thread is waiting on a mutex */
#define THREAD_STATE_WAIT_COND		9	/**< thread is waiting on a condition variable */
#define THREAD_STATE_DEAD			10	/**< thread is dead / uninitialized */


/* forward declarations */
struct part_cfg;
struct thread;
struct regs;
struct sched;
struct futex;
struct event_wait;
struct irq;
struct sync;

/** thread object */
struct thread {
	/** pointer to thread's partition configuration data (const after init) */
	const struct part_cfg *part_cfg;
	/** pointer to register context (const after init) */
	struct regs *regs;
	/** pointer to scheduling data */
	struct sched *sched;
	/** pointer to continuation handler */
	void (*continuation)(struct thread *);

	/** ready queue link */
	/* NOTE: SMP: protected by sched_lock */
	list_t readyql;
	/** timeout queue link */
	/* NOTE: SMP: protected by sched_lock */
	list_t timeoutql;

	/** current timeout value if the thread is waiting with a timeout */
	/* NOTE: SMP: protected by sched_lock */
	sys_time_t timeout;

	/** current scheduling priority */
	int prio;

	/** thread ID in partition (const after init) */
	/* NOTE: SMP: const after init */
	uint8_t id;

	/** thread state */
	uint8_t state;
	/** FPU state */
	uint8_t fpu_state;

	/** suspension wakeup flag */
	/* NOTE: SMP: protected by sched_lock */
	uint8_t suspend_wakeup;

	uint8_t woken_by_timeout;

	/** Set of blocked signals */
	uint32_t sig_mask_blocked;
	/** Set of pending signals */
	uint32_t sig_mask_pending;

	/** light-weight waiting: address of user space state variable */
	/* NOTE: SMP: protected by sched_lock */
	addr_t lwsync_user_state_addr;

	/** IRQ the thread is currently waiting on */
	struct irq *irq;

	/** Futex the thread is currently waiting on */
	/* NOTE: SMP: protected by task_lock */
	addr_t futex_addr;
	/** Futex wait queue node -- node in waiting queue */
	/* NOTE: SMP: protected by task_lock */
	list_t futex_waitql;

	/** Event object the thread is currently waiting on */
	struct event_wait *event_wait;

	/** TLS area of thread in user space */
	struct sys_tls *user_tls;

	/** Mutex the thread is waiting on or must be requeued to */
	/* NOTE: SMP: protected by task_lock */
	struct sync *cond_mutex;
	/** Sync object wait queue node -- node in waiting queue */
	/* NOTE: SMP: protected by task_lock */
	list_t sync_waitql;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint8_t thread_num;
extern struct thread thread_dyn[];
extern struct regs thread_regs[];

#endif
