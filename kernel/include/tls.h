/* SPDX-License-Identifier: MIT */
/*
 * tls.h
 *
 * User space TLS handling
 *
 * azuepke, 2020-02-01: initial
 */

#ifndef __TLS_H__
#define __TLS_H__

#include <compiler.h>
#include <arch_tls.h>

#define SYS_TLS_INIT(_tls, _thr_id, _prio, _cpu_id) {	\
	.tls = &(_tls),	\
	.thr_id = (_thr_id),	\
	.user_prio = (_prio),	\
	.next_prio = (_prio),	\
	.max_prio = (_prio),	\
	.cpu_id = (_cpu_id),	\
	}

/** define an opaque TLS object "name" for an initial thread */
#define SYS_TLS_DEFINE(name)	\
	struct sys_tls name = SYS_TLS_INIT(name, 0, 0xff, 0)

/** fixup TLS of an initial thread at start up time */
#define SYS_TLS_FIXUP() do {	\
		tls_set_1(offsetof(struct sys_tls, user_prio), sys_prio_get_syscall());	\
		tls_set_1(offsetof(struct sys_tls, max_prio), sys_prio_max_syscall());	\
		tls_set_1(offsetof(struct sys_tls, cpu_id), sys_cpu_get_syscall());	\
	} while (0)

#endif
