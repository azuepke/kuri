/* SPDX-License-Identifier: MIT */
/*
 * sync.h
 *
 * Mutex and condition variable data types.
 *
 * azuepke, 2020-02-02: initial
 */

#ifndef __SYNC_H__
#define __SYNC_H__

#include <sync_types.h>

/* API */
/** initialize all sync objects at boot time */
void sync_init_all(void);

/** reset sync object */
void sync_reset(struct sync *sync);

/** Callback for cleanup during wakeup (timeout, thread deletion) */
void sync_mutex_wait_cancel(struct thread *thr);

/** Callback for cleanup during wakeup (timeout, thread deletion) */
bool_t sync_cond_wait_cancel(struct thread *thr, bool_t requeue);

/** lock mutex */
err_t sync_mutex_lock(
	struct sync *mutex,
	sys_timeout_t timeout);

/** try to lock mutex */
err_t sync_mutex_trylock(
	struct sync *mutex);

/** unlock mutex */
err_t sync_mutex_unlock(
	struct sync *mutex);

/** retrieve mutex statistics */
err_t sync_mutex_stat(
	struct sync *mutex,
	uint32_t *stat_lu,
	uint32_t *stat_lc,
	uint32_t *stat_uu,
	uint32_t *stat_uc);

/** wait on condition variable */
err_t sync_cond_wait(
	struct sync *cond,
	struct sync *mutex,
	sys_timeout_t timeout);

/** notify condition variable */
err_t sync_cond_wake(
	struct sync *cond,
	bool_t notify_all);

#endif
