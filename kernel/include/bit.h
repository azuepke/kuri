/* SPDX-License-Identifier: MIT */
/*
 * bit.h
 *
 * Operations on bits.
 *
 * This file provides "find highest bit set" and "find lowest bit set"
 * operations for 32-bit and 64-bit integers.
 *
 * Important note: Unlike POSIX ffs(), the lowest bit is 0.
 * Also, the functions not robust if the value is zero.
 *
 * This code uses GCC atomic builtins and may not be portable.
 *
 * azuepke, 2021-01-21: rework using GCC builtins
 */

#ifndef __BIT_H__
#define __BIT_H__

/** find highest set bit -- 32-bit integer */
static inline unsigned int bit_fhs(unsigned int val)
{
	return ((sizeof(val) * 8) - 1) - __builtin_clz(val);
}

/** find highest set bit -- 64-bit integer */
static inline unsigned int bit_fhs64(unsigned long long val)
{
	return ((sizeof(val) * 8) - 1) - __builtin_clzll(val);
}

/** find lowest set bit -- 32-bit variant */
static inline unsigned int bit_fls(unsigned int val)
{
	return ((sizeof(val) * 8) - 1) - __builtin_clz(val & -val);
}

/** find lowest set bit -- 64-bit integer */
static inline unsigned int bit_fls64(unsigned long long val)
{
	return ((sizeof(val) * 8) - 1) - __builtin_clzll(val & -val);
}

#endif
