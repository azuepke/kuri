/* SPDX-License-Identifier: MIT */
/*
 * memrq_types.h
 *
 * Memory requirement data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __MEMRQ_TYPES_H__
#define __MEMRQ_TYPES_H__

#include <marron/types.h>

/* access permissions */
#define MEMRQ_READ	0x4u	/**< Memory requirement is readable */
#define MEMRQ_WRITE	0x2u	/**< Memory requirement is writable */
#define MEMRQ_EXEC	0x1u	/**< Memory requirement is executable */

/** memrq configuration data */
struct memrq_cfg {
	/** start address of memory requirement */
	addr_t addr;
	/** size of memory requirement */
	size_t size;
	/** access permissions (flags of MEMRQ_xxx) */
	uint8_t access;
	/** cache attributes (architecture specific value) */
	uint8_t cache;
};

/* generated data */
extern const uint8_t memrq_num;
extern const struct memrq_cfg memrq_cfg[];

#endif
