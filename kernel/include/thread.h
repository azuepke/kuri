/* SPDX-License-Identifier: MIT */
/*
 * thread.h
 *
 * Thread data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __THREAD_H__
#define __THREAD_H__

#include <thread_types.h>
#include <assert.h>

/* API */
/** initialize all thread objects at boot time to be blocked in STATE_DEAD */
void thread_init_all(void);

/** bild thread object to a partition at boot time */
void thread_assign_part(
	struct thread *thr,
	unsigned int id,
	const struct part_cfg *this_part_cfg);

/** start a thread */
void thread_start(struct thread *thr,
	ulong_t entry, ulong_t arg, ulong_t stack, ulong_t tls,
	int prio, unsigned int cpu_id);

/** kill a thread in any state */
void thread_kill(struct thread *thr);

/** let current thread wait in wait state with timeout */
void thread_wait(struct thread *thr, unsigned int state, sys_time_t timeout);

/** let current thread wait in wait state with timeout and given wait priority */
void thread_wait_prio(struct thread *thr, unsigned int state, sys_time_t timeout, unsigned int wait_prio);

/** wake a thread and make it READY again */
void thread_wakeup(struct thread *thr);

/** release the timeout of a blocked thread */
void thread_timeout_release(struct thread *thr);

/** do cleanup of special waiting states after timeout or thread deletion */
/* returns true if the target thread still must be woken up afterwards */
bool_t thread_wait_cancel(struct thread *thr);

/** preempt current thread (enforce priority changes) */
void thread_preempt(struct thread *thr);

/** yield current thread */
void thread_yield(struct thread *thr);

/** get bounded user priority of current thread */
int thread_prio_get(struct thread *thr);

/** change priority of current thread */
int thread_prio_change(struct thread *thr, int new_prio);

/** change CPU of current thread */
void thread_cpu_migrate(struct thread *thr, unsigned int new_cpu);

/** suspend current thread */
err_t thread_suspend(struct thread *thr);

/** resume suspended thread */
err_t thread_resume(struct thread *thr);

/** register continuation handler */
static inline void thread_continue_at(
	struct thread *thr,
	void (*continuation)(struct thread *))
{
	assert(thr != NULL);
	assert(thr->continuation == NULL);

	thr->continuation = continuation;
}

#endif
