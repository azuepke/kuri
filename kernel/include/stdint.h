/* SPDX-License-Identifier: MIT */
/*
 * stdint.h
 *
 * Standard integer types.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported
 */

#ifndef __STDINT_H__
#define __STDINT_H__

#if defined __x86_64__
#define __WORDSIZE 64
#endif

#if defined __i386__
#define __WORDSIZE 32
#endif

#if defined __arm__
#define __WORDSIZE 32
#endif

#if defined __aarch64__
#define __WORDSIZE 64
#define __HAS_INT128 1
#endif

#if defined __powerpc__
#define __WORDSIZE 32
#endif

#if defined __riscv_xlen
#define __WORDSIZE __riscv_xlen
#endif

#ifndef __WORDSIZE
#error Adapt this file to your compiler!
#endif

/** unsigned base types */
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
#if __WORDSIZE == 64
typedef unsigned long int uint64_t;
#else
typedef unsigned long long int uint64_t;
#endif
#ifdef __HAS_INT128
typedef __uint128_t uint128_t;
#endif

/** signed base types */
typedef signed char int8_t;
typedef signed short int int16_t;
typedef signed int int32_t;
#if __WORDSIZE == 64
typedef signed long int int64_t;
#else
typedef signed long long int int64_t;
#endif
#ifdef __HAS_INT128
typedef __int128_t int128_t;
#endif

#define INT8_MAX	0x7f
#define INT16_MAX	0x7fff
#define INT32_MAX	0x7fffffff
#if __WORDSIZE == 64
#define INT64_MAX	0x7ffffffffffffffful
#else
#define INT64_MAX	0x7fffffffffffffffull
#endif

#define INT8_MIN	(-INT8_MAX-1)
#define INT16_MIN	(-INT16_MAX-1)
#define INT32_MIN	(-INT32_MAX-1)
#define INT64_MIN	(-INT64_MAX-1)

#define UINT8_MAX	0xffu
#define UINT16_MAX	0xffffu
#define UINT32_MAX	0xffffffffu
#if __WORDSIZE == 64
#define UINT64_MAX	0xfffffffffffffffful
#else
#define UINT64_MAX	0xffffffffffffffffull
#endif

/** pointer types */
#if __WORDSIZE == 64
typedef signed long int intptr_t;
typedef unsigned long int uintptr_t;
#else
typedef signed int intptr_t;
typedef unsigned int uintptr_t;
#endif

/** addresses and sizes */
typedef uintptr_t addr_t;
#define _SSIZE_T_DEFINED_
typedef intptr_t ssize_t;
#define _SIZE_T_DEFINED_
typedef uintptr_t size_t;

#endif
