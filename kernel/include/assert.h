/* SPDX-License-Identifier: MIT */
/*
 * assert.h
 *
 * Assert.
 *
 * azuepke, 2013-05-03: split from kernel.h
 * azuepke, 2017-10-02: imported and adapted
 */

#ifndef __ASSERT_H__
#define __ASSERT_H__

#include <compiler.h>

/** runtime assert */
#ifdef NDEBUG
#define assert(cond) do { } while (0)
#else
/** assert internal function */
void __assert_fail(const char *cond, const char *file, int line, const char *func) __noreturn __cold;

#define assert(cond) \
	do { \
		if (cond) { \
			/* EMPTY */ \
		} else { \
			__assert_fail(__stringify(cond), __FILE__, __LINE__, __FUNCTION__); \
		} \
	} while (0)

#endif

/** static assert, verified at compile time:
 * use the _underscore variant in global scope.
 */
#define _static_assert(cond) \
	typedef int __concatenate(__STATIC_ASSERT_FAILED, __COUNTER__)[(cond) ? 1 : -1]

#define static_assert(cond) \
	do { \
		/* __used is required to calm down gcc 4.8+ */ \
		_static_assert(cond) __really_used; \
	} while (0)

#endif
