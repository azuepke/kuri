/* SPDX-License-Identifier: MIT */
/*
 * current.h
 *
 * Current per-CPU data.
 *
 * azuepke, 2018-03-05: moved current*() getters from kernel.h
 */

#ifndef __CURRENT_H__
#define __CURRENT_H__

#include <percpu_types.h>
#include <arch_percpu.h>
#include <bsp.h>

/** get current CPU via per-CPU area */
static inline unsigned int current_cpu_id(void)
{
#ifdef SMP
	return arch_percpu()->cpu_id;
#else
	return 0;
#endif
}

/** get current thread */
static inline struct thread *current_thread(void)
{
	return arch_percpu()->current_thread;
}

/** get current partition config */
static inline const struct part_cfg *current_part_cfg(void)
{
	return arch_percpu()->current_part_cfg;
}

#endif
