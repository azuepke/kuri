/* SPDX-License-Identifier: MIT */
/*
 * lwsync.h
 *
 * Light-weight synchronization implementation
 *
 * azuepke, 2020-02-02: initial
 */

#ifndef __LWSYNC_H__
#define __LWSYNC_H__

#include <marron/types.h>
#include <stdint.h>

/* forward declaration */
struct thread;

/* API */
err_t lwsync_wait(
	addr_t user_state_addr,
	int compare,
	sys_timeout_t timeout,
	unsigned int wait_prio);

err_t lwsync_wake(
	struct thread *thr,
	addr_t user_state_addr,
	int compare);

#endif
