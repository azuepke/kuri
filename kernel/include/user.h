/* SPDX-License-Identifier: MIT */
/*
 * user.h
 *
 * User space accessors
 *
 * azuepke, 2018-03-22: initial
 * azuepke, 2020-02-01: extended getters and setters
 */

#ifndef __USER_H__
#define __USER_H__

#include <marron/types.h>
#include <stdint.h>
#include <compiler.h>
#include <atomic.h>

/* get basic data types from user space */
#define __user_get_var(type, addr) ({	\
		type *__user_ptr = (addr);	\
		type __val;	\
		__val = access_once(*__user_ptr);	\
		__val;	\
	})

#define user_get_1(addr) __user_get_var(uint8_t, addr)
#define user_get_4(addr) __user_get_var(uint32_t, addr)
#define user_get_8(addr) __user_get_var(uint64_t, addr)
#define user_get_ptr(addr) __user_get_var(void *, addr)

/* put basic data types to user space */
#define __user_put_var(type, addr, val) do {	\
		type *__user_ptr = (addr);	\
		type __val = (val);	\
		access_once(*__user_ptr) = __val;	\
	} while (0)

#define user_put_1(addr, val) __user_put_var(uint8_t, addr, val)
#define user_put_4(addr, val) __user_put_var(uint32_t, addr, val)
#define user_put_8(addr, val) __user_put_var(uint64_t, addr, val)
#define user_put_ptr(addr, val) __user_get_var(void *, addr, val)

#define user_cas(addr, old, new) atomic_cas_relaxed(addr, old, new)

/** check address range in current user space for requested access */
err_t user_check_range(const void *user_ptr, size_t size, unsigned int access);
/** copy data to or from user space */
err_t user_copy(void *to, const void *from, size_t size);
/** copy data from current user space to kernel space */
err_t user_copy_in(void *to_kernel, const void *from_user, size_t size);
/** copy data from kernel space to current user space */
err_t user_copy_out(void *to_user, const void *from_kernel, size_t size);

#endif
