/* SPDX-License-Identifier: MIT */
/*
 * panic.h
 *
 * Kernel panic.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-03-05: moved from kernel.h
 */

#ifndef __PANIC_H__
#define __PANIC_H__

#include <compiler.h>

/* forward declarations */
struct regs;

/** kernel panic! */
void panic(const char *format, ...) __noreturn __printflike(1, 2) __cold;
void panic_regs(struct regs *regs, const char *format, ...) __noreturn __printflike(2, 3) __cold;

#endif
