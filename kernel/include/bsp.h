/* SPDX-License-Identifier: MIT */
/*
 * bsp.h
 *
 * BSP common functions.
 *
 * azuepke, 2017-07-17: initial for mini OS
 * azuepke, 2018-03-09: more documentation
 * azuepke, 2018-03-15: use system error codes
 */

#ifndef __BSP_H__
#define __BSP_H__

#include <marron/types.h>
#include <marron/error.h>
#include <stdint.h>
#include <compiler.h>


/* general services */

/** BSP build ID string
 *
 * The BSP build ID string is printed by the kernel at boot time.
 */
extern const char bsp_buildid[];

/** BSP name string
 *
 * The BSP name string can be retrieved by user space.
 */
extern const char bsp_name[];

/** Initialize BSP
 *
 * This function is responsible to initialize the BSP.
 * The kernel calls this function on all CPUs during startup.
 *
 * \see bsp_cpu_up()
 */
void bsp_init(
	void);

/** Idle the CPU
 *
 * The kernel calls this function in an endless loop from the idle thread
 * on each CPU.
 */
void bsp_idle(
	void);

/** Halt modes */
typedef enum {
	/** Immediate emergency halt, just halt the calling CPU. */
	BOARD_STOP = 0,
	/** Kernel panic. Affects the whole system. */
	BOARD_PANIC = 1,
	/** Regular halt, reset and power-off requests. Affect the whole system. */
	BOARD_HALT = 2,
	BOARD_RESET = 3,
	BOARD_POWEROFF = 4
} halt_mode_t;

/** Halt the CPU or the whole system
 *
 * The kernel calls this function to halt or shutdown the system.
 * The regular halt modes BOARD_HALT, BOARD_RESET, and BOARD_POWEROFF describe
 * that the whole system shall be halted upon user request.
 * Similarly, the kernel uses BOARD_PANIC on a kernel panic to request
 * the system to be halted or restarted.
 * In case of multiple panics, the kernel uses BOARD_STOP to request only
 * the currently calling CPU to be halted.
 *
 * \param [in] mode			Halt mode
 *
 * The actual implementation of halt, reset, or poweroff depends on the BSP.
 * As fallback, only the halt action needs to be implemented.
 */
void bsp_halt(
	halt_mode_t mode) __noreturn;


/* console */

/** Print a character on the system console
 *
 * This callback tries to print a character on the system console
 * if there is sufficient space in the serial output FIFO.
 *
 * \param [in] c			Character to print
 *
 * \retval EOK				Success (character was printed)
 * \retval EBUSY			Console is busy, try again later
 *
 * \todo FIXME: The kernel might call this function from multiple CPUs,
 * so bsp_putc() is responsible for locking.
 */
err_t bsp_putc(
	int c);


/* BSP timer interface
 *
 * The BSP timer interface allows a wide range of different implementations,
 * e.g. periodic timers or oneshot timers.
 * The BSP is responsible to keep the time coherent accross multiple CPUs,
 * e.g. the time must be monotonically increasing on all CPUs,
 * and the unavoidable jitter between the CPUs must not be observable by
 * threads migrating between CPUs.
 */

/** Timer resolution in nanoseconds
 *
 * The BSP reports the minimum resolution of the system timer here.
 * For a periodic timer implementation, this value reflects the timer period.
 * For a oneshot timer implementation, this value is the minimum interval
 * between two timer interrupts.
 *
 * On a timer interrupt, the BSP notifies the kernel via the kernel_timer()
 * callback. For a oneshot timer implementation, this BSP should reprogram
 * the next timer interrupt to infinity or disable the timer before notifying
 * the kernel. The kernel will reprogram the next timer expiry, if necessary.
 * Also, the timer interrupt should never be masked during interrupt handling.
 *
 * \see bsp_timer_get_time()
 * \see bsp_timer_set_expiry()
 */
extern sys_time_t bsp_timer_resolution;

/** Get current system time in nanoseconds
 *
 * This function return the current system time in nanoseconds since startup.
 *
 * \return Current system time in nanoseconds since boot
 *
 * \see bsp_timer_resolution
 * \see bsp_timer_set_expiry()
 */
sys_time_t bsp_timer_get_time(
	void);

/** Set next timer expiry (oneshot)
 *
 * The kernel calls this function to program the next timer interrupt
 * on the given CPU. The expiry time is given in nanoseconds relative to
 * system startup. If the given expiry time is already in the past,
 * the BSP shall raise a timer interrupt as soon as possible.
 *
 * \param [in] expiry		Timer expiry time in nanoseconds since boot
 * \param [in] cpu_id		CPU where the timer shall be set
 *
 * \see bsp_timer_resolution
 * \see bsp_timer_get_time()
 * \see bsp_irq_dispatch()
 * \see kernel_timer()
 */
void bsp_timer_set_expiry(
	sys_time_t expiry,
	unsigned int cpu_id);


/* BSP interrupt management
 *
 * The BSP is responsible for interrupt management and may assign interrupt IDs
 * at will. The kernel only uses interrupt IDs up to the BSP specific limit.
 * Interrupt IDs may be left unassigned, the kernel will probe if an interrupt
 * is available by calling bsp_irq_config() first.
 *
 * Except for the timer interrupt and an interrupt to enforce scheduling on
 * a remote CPU on a multiprocessor system, which are not handled by this API,
 * the kernel does not use interrupts for itself. All requests for interrupt
 * handling come from user space applications. If a thread wants to wait for
 * an interrupt, the kernel checks first if the interrupt is available
 * and configures the interrupt mode by calling bsp_irq_config().
 * If the interrupt is available, and a user space interrupt handler thread
 * is actively waiting for an interrupt, the kernel calls bsp_irq_enable()
 * to unmask the interrupt source and wait for interrupts to happen.
 * If an interrupt is received by the kernel, the architecture layer calls
 * bsp_irq_dispatch() to dispatch the interrupt cause. The BSP then in turn
 * masks and acknowledges the interrupt source and notifies the kernel
 * via kernel_irq() to wake any waiting user space interrupt handler thread.
 * After the thread completed interrupt handling, e.g. the thread completes
 * interrupt handling and calls sys_irq_wait() again, the kernel will unmask
 * the interrupt source by calling bsp_irq_enable() again.
 * If a user space interrupt handler thread stops waiting for the interrupt,
 * the kernel calls bsp_irq_disable() to mask the interrupt source.
 */

/** Number of IRQs supported by the BSP */
extern unsigned int bsp_irq_num;

/** Dispatch interrupt
 *
 * The architecture layer calls this function to dispatch an interrupt
 * on the current CPU. The "vector" argument has an architecture specific
 * meaning and usually refers to an architecture provided interrupt vector.
 *
 * The BSP shall dispatch the interrupt further, i.e. by querying the interrupt
 * controller, and shall notify the kernel about the interrupt cause, e.g.
 * call kernel_timer(), kernel_irq(), or kernel_ipi() on a multiprocessor
 * system. The BSP can also dispatch multiple interrupts at once. Except
 * for the timer interrupt and the inter-processor interrupt for rescheduling,
 * the BSP shall mask the interrupt source before notifying the kernel.
 *
 * \param [in] cause		Architecture specific vector argument
 *
 * \see bsp_irq_register()
 * \see kernel_irq()
 * \see kernel_timer()
 * \see kernel_ipi()
 */
void bsp_irq_dispatch(ulong_t vector);

/** Configure interrupt source
 *
 * The kernel calls this function to configure the mode of an interrupt
 * as requested by the user. The interrupt can configured as level-triggered
 * or edge-triggered interrupt.
 *
 * This call returns EOK if the interrupt source was configured to the
 * requested mode, or TYPE if not.
 * The interrupt mode "IRQ_MODE_AUTO" shall always succeed
 * if the interrupt source is available.
 * The call shall fail with ETYPE if the interrupt is not available
 * on the platform.
 *
 * \param [in] irq_id		Interrupt ID
 * \param [in] mode			Interrupt mode
 *
 * \retval EOK				Success
 * \retval EBUSY			If the interrupt is not available on the platform
 * \retval ETYPE			If the requested interrupt mode is not supported
 *
 * \see bsp_irq_num
 * \see bsp_irq_enable()
 * \see bsp_irq_disable()
 * \see bsp_irq_register()
 */
err_t bsp_irq_config(
	unsigned int irq_id,
	irq_mode_t mode);

/** Enable (unmask) interrupt source and bind interrupt to given CPU
 *
 * The kernel calls this function to unmask the given interrupt, and,
 * if possible, route the interrupt to the given CPU, where a thread
 * is waiting for the interrupt. This CPU is usually the calling CPU.
 *
 * \param [in] irq_id		Interrupt ID
 * \param [in] cpu_id		CPU ID to bind interrupt to
 *
 * \see bsp_irq_num
 * \see bsp_irq_config()
 * \see bsp_irq_disable()
 * \see bsp_irq_register()
 */
void bsp_irq_enable(
	unsigned int irq_id,
	unsigned int cpu_id);

/** Disable (mask) interrupt source on all CPUs
 *
 * The kernel calls this function to mask the given interrupt on all CPUs.
 *
 * \param [in] irq_id		Interrupt ID
 *
 * \see bsp_irq_num
 * \see bsp_irq_config()
 * \see bsp_irq_enable()
 * \see bsp_irq_register()
 */
void bsp_irq_disable(
	unsigned int irq_id);

/** IRQ handler type
 *
 * Function type of IRQ handlers. Note that kernel_irq() is of this type.
 * The BSP always calls the handlers with the current interrupt ID.
 *
 * \param [in] irq_id		Interrupt ID
 *
 * \see bsp_irq_num
 * \see bsp_irq_register()
 */
typedef void (*bsp_irq_handler_t)(unsigned int irq_id);

/** Register an interrupt handler
 *
 * The BSP can use this BSP-internal function to register an interrupt handler,
 * e.g. for the timer interrupt. The kernel will never call this function.
 *
 * \param [in] irq_id		Interrupt ID
 * \param [in] handler		Interrupt handler
 *
 * \see bsp_irq_num
 * \see bsp_irq_dispatch()
 * \see bsp_irq_config()
 * \see bsp_irq_enable()
 * \see bsp_irq_disable()
 */
void bsp_irq_register(
	unsigned int irq_id,
	bsp_irq_handler_t handler);


/* Cache management
 *
 * The BSP implements cache handling for application loading and management
 * of DMA memory buffers.
 *
 * The interface supports PIPT (physically indexed physically tagged) data
 * caches, and either VIPT (virtually indexed physically tagged) or PIPT
 * instruction caches.
 *
 * The operations on the caches are defined as follows:
 * - FLUSH writes a dirty cache line back to memory and invalidates
 *   the cache line afterwards
 * - CLEAN writes a dirty cache line back to memory
 * - INVAL invalidates a cache line without writing it back to memory
 */

/** Cache management
 *
 * Perform a cache management operation on the given virtual memory region
 * denoted by "start" and "size". The "alias" argument is only used for
 * instruction cache handling to handle potential aliases in the instruction
 * caches and may refer to an address in a currently not active address space.
 * If the cache aliases, it is recommended to invalidate the whole instruction
 * cache if "alias" is not equal to "start".
 *
 * \param [in] op			Cache operation
 * \param [in] start		Start of memory region
 * \param [in] size			Size of memory region
 * \param [in] alias		Alias address in another address space
 *
 * \retval EOK				Success
 * \retval EFAULT			Invalid address or page fault
 */
err_t bsp_cache(
	cache_op_t op,
	addr_t start,
	size_t size,
	addr_t alias);


#ifdef SMP
/* CPU management
 *
 * The BSP is responsible for startup and shutdown of the CPUs:
 * At first, the BSP starts CPU #0 only and enters the kernel via the
 * architecture provided entry point, arch_entry(). Next, the kernel
 * calls bsp_init() on this CPU, and then initializes the rest of the system.
 * After initialization, the kernel invokes bsp_cpu_start_secondary() to start
 * the other CPUs. It is recommended to start one CPU after another, until
 * each CPU reaches the bsp_cpu_up() callback before starting scheduling.
 * When bsp_cpu_start_secondary() returns, the kernel calls bsp_cpu_up()
 * on CPU #0.
 */

/** Number of CPUs in the system */
extern unsigned int bsp_cpu_num;

/** Mask of CPUs which are online (already started) in the system */
extern volatile ulong_t bsp_cpu_online_mask;

/** Current CPU ID
 *
 * This function returns the current CPU ID.
 *
 * \return Current CPU ID
 *
 * \see bsp_cpu_num
 * \see bsp_cpu_online_mask
 */
unsigned int bsp_cpu_id(void);

/** Send reschedule IPI
 *
 * The kernel calls this function to enforce rescheduling on other CPUs.
 * On the other CPU, the BSP interrupt dispatcher shall call kernel_ipi().
 *
 * \param [in] cpu_id		CPU ID to send a rescheduling request to
 *
 * \see bsp_cpu_num
 * \see bsp_irq_dispatch()
 * \see kernel_ipi()
 */
void bsp_cpu_reschedule(unsigned int cpu_id);

/** Start secondary CPUs
 *
 * The kernel calls this function from CPU #0 to start other CPUs.
 *
 * \see bsp_cpu_up()
 */
void bsp_cpu_start_secondary(void);

/** Notification of initialization complete
 *
 * The kernel calls this function on all processors
 * shortly before scheduling threads.
 *
 * \param [in] cpu_id		CPU ID which starts scheduling
 *
 * \see bsp_cpu_num
 * \see bsp_init()
 * \see bsp_cpu_start_secondary()
 */
void bsp_cpu_up(unsigned int cpu_id);
#endif

#endif
