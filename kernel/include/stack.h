/* SPDX-License-Identifier: MIT */
/*
 * stack.h
 *
 * User space stack handling
 *
 * azuepke, 2018-04-10: initial
 * azuepke, 2020-02-02: rework for Kuri
 */

#ifndef __STACK_H__
#define __STACK_H__

#include <compiler.h>

/** define an opaque stack object "name" with "size_in_byte" stack inside */
#define SYS_STACK_DEFINE(name, size_in_bytes)	\
	struct {	\
		char stack[size_in_bytes];	\
	} name __aligned(16)	\

/** get the stack stack pointer for a stack object "name" */
#define SYS_STACK_PTR(name) ((void*)(((addr_t)&(name)) + (sizeof(name))))

/** get the base address of a stack object "name" */
#define SYS_STACK_BASE(name) ((void*)&(name))

/** get the size of a stack object "name" */
#define SYS_STACK_SIZE(name) sizeof(name)

#endif
